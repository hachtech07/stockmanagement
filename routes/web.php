<?php

use App\Http\Controllers\Administrators\BillingController;
use App\Http\Controllers\Administrators\UserController;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\DashboardController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Route::get('/', function () {
//     return view('login');
// });

Route::get('/', [App\Http\Controllers\HomeController::class, 'index']);
Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');
Route::get('/logout', [App\Http\Controllers\HomeController::class, 'logout']);
Route::get('forgot', [App\Http\Controllers\AuthController::class, 'forgot'])->name('forgot');
Route::post('forgotPassword', [App\Http\Controllers\AuthController::class, 'forgotpassword'])->name('forgotpassword');
Route::match(['get', 'post'], '/resetpassword/{token}/{id}', [App\Http\Controllers\AuthController::class, 'resetpassword'])->name('resetpassword');


Route::match(['get','post'],'/product/create',[App\Http\Controllers\Admin\ProductController::class,'create'])->name('product.create');
Route::match(['get','post','put'],'/product/update/{id}',[App\Http\Controllers\Admin\ProductController::class,'update'])->name('product.update');







Route::group(['middleware' => ['auth']], function () {

	// Dashboard Routes
	Route::get('/dashboard', [DashboardController::class, 'index'])->name('dashboard')->middleware('status');
	Route::get('/dashboard/report', [DashboardController::class, 'dashboard_report'])->name('dashboard.report')->middleware('status');
	Route::get('/dashboard/index', [DashboardController::class, 'index'])->middleware('status');
	Route::get('dashboard/ajaxuser/{id}', [DashboardController::class, 'get_user_by_branch'])->name('customers')->middleware('status');
	Route::get('dashboard/ajaxcustomer/{id}', [DashboardController::class, 'get_customers_by_branch'])->name('customerdata')->middleware('status');
	Route::get('dashboard/ajaxbarchart/{year}', [DashboardController::class, 'get_due_and_collected'])->name('barchartdata')->middleware('status');
	Route::get('dashboard/ajaxlinechart/{year}', [DashboardController::class, 'get_month_wise_bill_generated'])->name('linechartdata')->middleware('status');
	Route::match(['get', 'post', 'put'], '/profile', [App\Http\Controllers\ProfileController::class, 'update'])->name('updateprofile')->middleware('status');

	Route::match(['get', 'post', 'put'], '/changepass', [App\Http\Controllers\Admin\ProfileController::class, 'change_pass'])->name('changepass');
	Route::get('/user',[App\Http\Controllers\Admin\UserController::class,'index'])->name('users');
	Route::match(['get','post'],'/user/creates',[App\Http\Controllers\Admin\UserController::class,'create'])->name('users.create');
	Route::match(['get','post','put'],'/user/updates/{id}',[App\Http\Controllers\Admin\UserController::class,'update'])->name('users.update');
	Route::match(['get', 'post', 'put'], '/user/change-password/{id}', [App\Http\Controllers\Admin\UserController::class, 'changeUserPassword'])->name('user.update.pass');

// Route::get('/product/subcategory-list/{id}',[App\Http\Controllers\Admin\ProductCategoryController::class,'getSubCategory'])->name('subcategory-list');
Route::get('/product/subcategory-api/{id}',[App\Http\Controllers\Admin\ProductSubCategoryController::class,'getSubCategory'])->name('subcategory-api');

Route::get('/product/subcategory-list/{id}',[App\Http\Controllers\Admin\ProductSubCategoryController::class,'subCategories'])->name('product.subcategory');
Route::get('/product/category',[App\Http\Controllers\Admin\ProductCategoryController::class,'index'])->name('product.category');
Route::match(['get','post'],'/product/category/create',[App\Http\Controllers\Admin\ProductCategoryController::class,'create'])->name('product.category.create');
Route::match(['get','post','put'],'/product/category/updates/{id}',[App\Http\Controllers\Admin\ProductCategoryController::class,'update'])->name('product.category.update');
Route::match(['get','post'],'/product/subcategory/create/{id}',[App\Http\Controllers\Admin\ProductSubCategoryController::class,'createSubCategory'])->name('product.subcategory.create');
// Route::match(['get','post'],'/product/subcategory/create/{id}',[App\Http\Controllers\Admin\ProductCategoryController::class,'createSubCategory'])->name('product.subcategory.create');


Route::match(['get','post'],'/products', [App\Http\Controllers\Admin\ProductController::class, 'index'])->name('product.index');
Route::match(['get','post'],'/sales', [App\Http\Controllers\Admin\SalesController::class, 'index'])->name('sale.index');

// Route::get('/product/subcategory/{id}',[App\Http\Controllers\Admin\ProductCategoryController::class,'subCategories'])->name('product.subcategory'); //throght parent_id
	
	Route::get('/customer-category', [App\Http\Controllers\Administrators\CustomerCategoryController::class, 'index'])->middleware('role_or_permission:super admin|branch admin', 'status', 'permissions:list customer category');
	Route::get('/customer-category/index', [App\Http\Controllers\Administrators\CustomerCategoryController::class, 'index'])->name('customer-category')->middleware('role_or_permission:super admin|branch admin', 'status', 'permissions:create customer category');
	Route::match(['get', 'post'], '/customer-category/create', [App\Http\Controllers\Administrators\CustomerCategoryController::class, 'create'])->name('customer-category.create')->middleware('role_or_permission:super admin|branch admin', 'status', 'permissions:create user', 'permissions:create customer category');
	Route::match(['get', 'post', 'put'], '/customer-category/update/{id}', [App\Http\Controllers\Administrators\CustomerCategoryController::class, 'update'])->name('customer-category.update')->middleware('role_or_permission:super admin|branch admin', 'status', 'permissions:create customer category');

	//excel 
	Route::get('/product/reports/stock-report/export-stock', [App\Http\Controllers\Admin\ProductController::class, 'export_stock'])->name('export.stock');
	Route::get('/sales/reports/stock-report/export-sale', [App\Http\Controllers\Admin\SalesController::class, 'export_sale'])->name('export.sales');

	//import
	Route::get('/customer/download', [App\Http\Controllers\Admin\ProductController::class, 'download'])->name('product.download');
	Route::match(['get', 'post'], '/product/import', [App\Http\Controllers\Admin\ProductController::class, 'import'])->name('product.import');
	

	//sales

Route::get('/product/sale/{id}',[App\Http\Controllers\Admin\SalesController::class,'saleDetail'])->name('sale.view');
Route::post('/product/sale/create',[App\Http\Controllers\Admin\SalesController::class,'sales'])->name('sale.create');



	// Route Role
	Route::get('/role', [App\Http\Controllers\Administrators\RoleController::class, 'index'])->name('role')->middleware('role_or_permission:super admin', 'status', 'permissions:list role');
	Route::get('/role/create', [App\Http\Controllers\Administrators\RoleController::class, 'create'])->name('role.create')->middleware('role_or_permission:super admin', 'status', 'permissions:add permission to role');
	Route::post('/role/store', [App\Http\Controllers\Administrators\RoleController::class, 'store'])->name('role.store')->middleware('role_or_permission:super admin', 'status', 'permissions:add permission to role');
	Route::match(['get', 'post', 'put'], '/role/update/{id}', [App\Http\Controllers\Administrators\RoleController::class, 'update'])->name('role.update')->middleware('role_or_permission:super admin', 'status', 'permissions:update role');
	Route::match(['get', 'post'], '/role/add-permissions/{id}',  [App\Http\Controllers\Administrators\RoleController::class, 'addPermissions'])->name('role.add-permissions')->middleware('role_or_permission:super admin', 'status', 'permissions:add permission to role');
	Route::match(['get', 'post'], '/role/add-role-permissions/{id}',  [App\Http\Controllers\Administrators\RoleController::class, 'addRolePermission'])->name('role.add-role-permissions')->middleware('role_or_permission:super admin', 'status', 'permissions:add permission to role');

});

Auth::routes();
