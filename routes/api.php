<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\ApiController;

Route::post('login', [ApiController::class, 'authenticate']);

Route::group(['middleware' => ['jwt.verify']], function () {
    Route::get('logout', [ApiController::class, 'logout']);
    Route::get('get_user', [ApiController::class, 'get_user'])->middleware('user_api_status','force_logout');
    Route::get('dashboard', [ApiController::class, 'dashboard'])->middleware('user_api_status','force_logout');
    Route::get('search_user', [ApiController::class, 'search_user'])->middleware('user_api_status','force_logout');
    Route::post('billingPage', [ApiController::class, 'billingPage'])->middleware('user_api_status','force_logout');
    Route::post('billingStore', [ApiController::class, 'billingStore'])->middleware('user_api_status','force_logout');
    Route::get('todayBillList', [ApiController::class, 'todayBillList'])->middleware('user_api_status','force_logout');
    Route::get('billDetails', [ApiController::class, 'billDetails'])->middleware('user_api_status','force_logout');
    Route::get('billingInformationForOffline', [ApiController::class, 'billingInformationForOffline'])->middleware('user_api_status','force_logout');
    Route::post('syncOflineBills', [ApiController::class, 'syncOflineBills'])->middleware('user_api_status','force_logout');
    Route::get('customer-statement/{id}', [ApiController::class, 'customer_statement'])->middleware('user_api_status','force_logout');
    Route::get('duplicate-bill-print/{id}', [ApiController::class, 'duplicate_bill_print'])->middleware('user_api_status','force_logout');
});
