$(function () {
  //get the pie chart canvas
  var cBarData = window.cBarData;
  var ctx1 = $("#bar-chart");

  //pie chart data
  var data = {
      labels: ["Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"],
      datasets: [
      {
          label: 'Collected Amount',
          data: cBarData.colData,
          backgroundColor: '#6149cc',
      },
      {
        label: 'Due Amount',
        data: cBarData.dueData,
        backgroundColor: '#ff7da1'
      }
    ]
  };

  //options
  var options = {
      responsive: true,
      maintainAspectRatio: false,
      plugins: {
      legend: {
          display: true,
          position: 'bottom'
      },
      tooltips: {
          titleMarginBottom: 10,
          titleFontColor: '#6e707e',
          titleFontSize: 14,
          backgroundColor: "rgb(255,255,255)",
          bodyFontColor: "#858796",
          borderColor: '#dddfeb',
          borderWidth: 1,
          xPadding: 15,
          yPadding: 15,
          displayColors: false,
          caretPadding: 10
      },
    },
    scales: {
      x: {
        grid: {
          display: false,
        },
      },
      y: {
        grid: {
          display: true,
          color: '#fff',
        },
      },
    },
  };

  //create Bar Chart class object

  window.chart2 = new Chart(ctx1, {
      type: "bar",
      data: data,
      options: options
  });

});