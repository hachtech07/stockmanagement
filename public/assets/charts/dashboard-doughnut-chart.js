$(function() {
    var cData = JSON.parse(window.cDoughnutData);
    var ctx = $('#doughnut-chart');
    //pie chart data
    var data = {
        labels: cData.label,
        datasets: [{
            data: cData.data,
            fill: false,
            backgroundColor: [
                "#6149cc",
                "#73eaf7",
                "#70ce98",
                "#efbe81",
                "#ff7da1",
            ]
        }, ],
    };
    const isMobile = window.innerWidth < 1025;

    //options
    var options = {
        responsive: true,
        maintainAspectRatio: isMobile ? true : false,
        cutout: 90,
        plugins: {
            legend: {
                display: true,
                position: isMobile ? "bottom" : "right",
            },
            scales: {
                xAxes: [{
                    gridLines: {
                        display: false,
                        drawBorder: false
                    },
                    maxBarThickness: 25,
                }],
                yAxes: [{
                    gridLines: {
                        color: "rgb(234, 236, 244)",
                        zeroLineColor: "rgb(234, 236, 244)",
                        drawBorder: false,
                        borderDash: [2],
                        zeroLineBorderDash: [2]
                    }
                }],
            }, // Scales are used to manipulate bar charts Axis and graphs content
        },
        tooltips: {
            titleMarginBottom: 10,
            titleFontColor: "#6e707e",
            titleFontSize: 14,
            backgroundColor: "rgb(255,255,255)",
            bodyFontColor: "#858796",
            borderColor: "#dddfeb",
            borderWidth: 1,
            xPadding: 15,
            yPadding: 15,
            displayColors: false,
            caretPadding: 10,
        },
    };

    //create Pie Chart class object
    var chart1 = new Chart(ctx, {
        type: "doughnut",
        data: data,
        options: options
    });
});