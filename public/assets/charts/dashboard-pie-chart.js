$(function () {
var cData = JSON.parse(window.cPieData);
var ctx = $('#pie-chart');
//pie chart data
var data = {
  labels: cData.label,
  datasets: [
    {
      data: cData.data,
      fill: false,
      backgroundColor: [
        "#003556",
        "#189CEF",
        "#0064A3",
        "#563200",
        "#A35F00",
        "#D06DAB",
        "#A03D50",
      ]
    },
  ],
};

//options
var options = {
  responsive:true,
  maintainAspectRatio: false,
  plugins: {
  legend: {
    display: true,
    position: "right",
  },
  scales: {
    xAxes: [{
        gridLines: {
            display: false,
            drawBorder: false
        },
        maxBarThickness: 25,
    }],
    yAxes: [{
        gridLines: {
            color: "rgb(234, 236, 244)",
            zeroLineColor: "rgb(234, 236, 244)",
            drawBorder: false,
            borderDash: [2],
            zeroLineBorderDash: [2]
        }
    }],
  }, // Scales are used to manipulate bar charts Axis and graphs content
},
  tooltips: {
    titleMarginBottom: 10,
    titleFontColor: "#6e707e",
    titleFontSize: 14,
    backgroundColor: "rgb(255,255,255)",
    bodyFontColor: "#858796",
    borderColor: "#dddfeb",
    borderWidth: 1,
    xPadding: 15,
    yPadding: 15,
    displayColors: false,
    caretPadding: 10,
  },
};

  //create Pie Chart class object
  var chart1 = new Chart(ctx, {
      type: "pie",
      data: data,
      options: options
  });
});
