$(function () {
var cLineData = window.cLineData;
var ctx = $('#line-chart');
//doughnut chart data

var data = {
  labels: ["Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"],
  datasets: [
    {
      color:'#fff',
      label: 'Total Bill',
      labelColor: '#fff',
      data: cLineData.colData,
      borderColor: '#fff',
      pointBackgroundColor: "#fff",
      backgroundColor: "#fff",
      pointBorderColor: "#fff",
      pointHoverBackgroundColor: "#fff",
      pointHoverBorderColor: "#fff",
      pointRadius: 3,
      pointHoverRadius: 4,
      borderWidth: 4,
      tension: 0.4,
    }
  ]
};

//options
var options= {
  responsive: true,
  maintainAspectRatio: false,
  plugins: {
    
    legend: {
      labels: {
        color: '#fff',
    },
      
      position: 'bottom',
      fontColor:'#fff'
    },
  },
  scales: {
    
    x: {
      ticks:{
        color: '#fff'
      },
      grid: {
        fontColor:'#fff',
        display: false,
      },
    },
    y: {
      ticks:{
        color: '#fff'
      },
      grid: {
        display: true,
        // fontColor:'#fff',
        color: 'transparent',
      },
    },
  },
};

  //create Pie Chart class object
  window.chart3 = new Chart(ctx, {
      type: "line",
      data: data,
      options: options
  });
});
