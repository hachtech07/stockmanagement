$("#export-form").on("submit", function (e) {
  e.preventDefault();
  var href = $("#export-form").attr("action");
  var from = $("#from").val();
  var froms = from != null ? from : "";
  var to = $("#to").val();
  var tos = to != null ? to : "";
  var user = $("#user_id").val();
  var users = user != null ? user : "";
  var branch = $("#branch_id").val();
  var branches = branch != null ? branch : "";
  var category = $("#category_id").val();
  var categories = category != null ? category : "";
  var fiscal_year = $("#fiscal_year").val();
  var fiscal_years = fiscal_year != null ? fiscal_year : "";
  var csrf = $('input[name="_token"').val();

  location.replace(
    href +
      "?" +
      "csrf=" +
      csrf +
      "&" +
      "branch_id=" +
      branches +
      "&" +
      "user_id=" +
      users +
      "&" +
      "from=" +
      froms +
      "&" +
      "to=" +
      tos +
      "&" +
      "category_id=" +
      categories +
      "&" +
      "fiscal_year=" +
      fiscal_years
  );
});
