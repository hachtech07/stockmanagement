$("#export-forms").on("submit", function (e) {
  e.preventDefault();
  var href = $("#export-forms").attr("action");
  var searchParams = $("#search_param").val();
  var searchParam = searchParams != null ? searchParams : "";
  var category_id = $("#category_id").val();
  var category = category_id != null ? category_id : "";
  var branch_id = $("#branch_id").val();
  var branch = branch_id != null ? branch_id : "";
  var collector_id = $("#collector_id").val();
  var collector = collector_id != null ? collector_id : "";
  var stat      = $("#status").val();
  var status    = stat != null ? stat : "";
  var csrf = $('input[name="_token"').val();

  location.replace(
    href +
      "?" +
      "csrf=" +
      csrf +
      "&" +
      "search_param=" +
      searchParam +
      "&" +
      "category_id=" +
      category +
      "&" +
      "branch_id=" +
      branch +
      "&" +
      "collector_id=" +
      collector +
      "&" +
      "status=" +
      status
  );
});
