$("body").on(
  "click change",
  ".bill-switch, #from-year, #from-month, #to-year, #to-month, #discount-amount",
  function () {
    let from_year = $("#from-year").val();
    let from_month = $("#from-month").val() - 1;
    let to_year = $("#to-year").val();
    let to_month = $("#to-month").val();
    let months = 0;

    if (from_year == to_year) {
      months = to_month - from_month;
    } else if (to_year > from_year) {
      months = parseInt(to_month) + 12 * (to_year - from_year) - from_month;
    } else {
      return false;
    }

    if (months > 0) {
      $("#number-of-months").html(months);
    } else {
      months = 1;
    }

    let categories = $(".bill-switch:checkbox:checked");
    let gross_amount = 0;
    let discount = $.isNumeric($("#discount-amount").val())
      ? $("#discount-amount").val()
      : 0;
    let due_amount = $(".due-amount").attr("default-price") || 0;
    let advance_amount = $(".advance-amount").attr("default-price") || 0;

    gross_amount +=
      parseInt($(".monthly-payable").attr("default-price")) * months +
      parseInt(due_amount) -
      parseInt(advance_amount);

    $.each(categories, function (index, category) {
      // add extra charge
      if ($(this).hasClass("monthly-charge")) {
        gross_amount += $(this).attr("default-price") * months;
      } else {
        gross_amount += $(this).attr("default-price") * 1;
      }
    });

    $("#gross-amount").parent().addClass("focused");
    $("#gross-amount").val(gross_amount);

    $("#discount-amount").parent().addClass("focused");

    $("#net-amount").parent().addClass("focused");
    $("#net-amount").val(
      $.isNumeric(gross_amount - discount) ? gross_amount - discount : 0
    );
  }
);
