(function ($) {
  $.fn.fetchDataTableContent = function (options) {
    var config = $.extend(
      {
        url: "",
        columns: [],
        tableId: "myTable",
        pageLength: 10,
        data: [],
        footerCallback:"",
        drawCallback:"",
        initComplete:""
      },
      options
    );

    //override Datatable export function to get data from server based on params

    $.fn.DataTable.Api.register("buttons.exportData()", function () {
      if (this.context.length) {
        var src_keyword = $(".dataTables_filter input").val();

        // make columns for sorting
        var columns = [];
        $.each(this.context[0].aoColumns, function (key, value) {
          columns.push({
            data: value.data,
            name: value.name,
            searchable: value.bSearchable,
            orderable: value.bSortable,
          });
        });

        // make option for sorting
        var order = [];
        $.each(this.context[0].aaSorting, function (key, value) {
          order.push({
            column: value[0],
            dir: value[1],
          });
        });

        // make value for search
        var search = {
          value: this.context[0].oPreviousSearch.sSearch,
          regex: this.context[0].oPreviousSearch.bRegex,
        };

        var items = [];
        var status = $("#status").val();
        $.ajax({
          url: config.url,
          data: {
            columns: columns,
            order: order,
            search: search,
            status: status,
            page: "all",
          },
          success: function (result) {
            $.each(result.data, function (key, value) {
              var item = [];
              // item.push(key + 1);

              $.each(config.columns, function (col_index, col_val) {
                if (col_val.name.includes(".")) {
                  var str_arr = col_val.name.split(".");
                  var temp_val = value;
                  for (let i = 0; i < str_arr.length; i++) {
                    temp_val = temp_val[str_arr[i]];
                  }
                  item.push(temp_val);
                } else {
                  var temp = value[col_val.name];
                  item.push(temp);
                }
              });

              items.push(item);
            });
          },
          async: false,
        });

        return {
          body: items,
          header: $("#myTable thead tr th.visible-on-export")
            .map(function () {
              if (this.innerHTML != "Actions") return this.innerHTML;
            })
            .get(),
        };
      }
    });

    // function main() {
    let dt = $(`#${config.tableId}`).DataTable({
      responsive: true,
      processing: true,
      serverSide: true,
      ajax: {
        url: config.url,
        data: config.data,
        dataSrc:config.dataSrc
      },
      columns: config.columns,
      dom: "Br",
      ordering: false,
      buttons: ["csv", "excel", "pdf", "print"],
      'language': {
        'loadingRecords': '&nbsp;',
        'processing': 'Loading... please wait'
    }   ,
      pageLength: config.pageLength,
      footerCallback:config.footerCallback,
      drawCallback:config.drawCallback,
      initComplete:config.initComplete
    });

    return dt;
    // }

    //call function
    // main();
  };
})(jQuery);
