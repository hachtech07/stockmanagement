$("#export-form").on("submit", function(e) {
    e.preventDefault();
    var href = $("#export-form").attr("action");
    var from = $("#from").val();
    var froms = from != null ? from : "";
    var to = $("#to").val();
    var tos = to != null ? to : "";
    var category = $("#category_id").val();
    var categories = category != null ? category : "";
    var subcategory = $("#product_sub_category_id").val();
    var subcategories = subcategory != null ? subcategory : "";
    var batch = $("#batch").val();
    var batchess = batch != null ? batch : "";
    var fiscal_year = $('#fiscal_year').val();
    var fiscal = fiscal_year != null ? fiscal_year : '';
    var csrf = $('input[name="_token"').val();

    location.replace(
        href +
        "?" +
        "csrf=" +
        csrf +
        "&" +
        "product_sub_category_id=" +
        subcategories +
        "&" +
        "category_id=" +
        categories +
        "&" +
        "from=" +
        froms +
        "&" +
        "to=" +
        tos +
        "&" +
        "batch=" +
        batchess +
        "&" +
        "fiscal_year=" +
        fiscal
    );
});