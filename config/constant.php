<?php

use Illuminate\Support\Facades\Config;

return [

    'paginate' => '100',
    'admin_id' => '1',
    'advance'  => '3',
    'zero'     => '0',
    'role_id'  => '1'

];
