@extends('layouts.admin_layout')

@section('title', 'Edit Profile')

@section('content')
	
    <div class="container-fluid my-3">
        <div class="d-flex row">
            <div class="col-md-12">
	            <div class="card my-3 shadow no-b r-0">
	                <div class="card-header white">
	                    <h6>Edit Profile</h6>
	                </div>
	                <div class="card-body">
	                    <form method="POST" action={{ route("updateprofile",$userDetail->id) }}>
	                		@csrf
	                        <div class="col-md-6 mb-3 form-group">
	                            <label for="branch_title">Name</label>
	                            <input type="text" class="form-control @error('name') is-invalid @enderror" id="name" placeholder="Name"  name="name" value="{{ $userDetail -> name }}">
	                            @error('name')
	                            	<div class="invalid-feedback">{{$message}}</div>
	                            @enderror
	                        </div>
	                        <div class="col-md-6 mb-3 form-group">
	                            <label for="address">Email</label>
	                            <input type="text" class="form-control @error('email') is-invalid @enderror" id="email" placeholder="Email" value="{{ $userDetail -> email}}" title="Email Cannot be changed" readonly>
	                            @error('email')
	                            	<div class="invalid-feedback">{{$message}}</div>
	                            @enderror
	                        </div>

                            <div class="col-md-6 mb-3 form-group">
	                            <label for="address">Branch</label>
	                            <input type="text" class="form-control @error('branch_id') is-invalid @enderror" id="email" placeholder="Branch" value="{{ $userDetail -> branch -> branch_title}}" title="Branch Cannot be changed" readonly>
	                            @error('branch_id')
	                            	<div class="invalid-feedback">{{$message}}</div>
	                            @enderror
	                        </div>

                            <div class="col-md-6 mb-3 form-group">
	                            <label for="address">Role</label>
	                            <input type="text" class="form-control @error('user_type') is-invalid @enderror" id="user_type" placeholder="Role" value="{{ $userDetail -> user_type}}" title="Role Cannot be changed" readonly>
	                            @error('user_type')
	                            	<div class="invalid-feedback">{{$message}}</div>
	                            @enderror
	                        </div>

                            <div class="col-md-6 mb-3 form-group">
	                            <label for="address">Status</label>
	                            <input type="text" class="form-control @error('status') is-invalid @enderror" id="status" placeholder="status" value="{{ $userDetail -> status}}" title="Status Cannot be changed" readonly>
	                            @error('status')
	                            	<div class="invalid-feedback">{{$message}}</div>
	                            @enderror
	                        </div>

	                        <div class="col-md-4 mb-3 form-group">
	                        	<button class="btn btn-sm btn-primary" type="submit">Update</button>
	                        </div>
	                    </form>
	                </div>
	            </div>
            </div>
        </div>
    </div>

@endsection

<script>
	const passwordEle = document.getElementById('password');
	const toggleEle = document.getElementById('toggle');

	toggleEle.addEventListener('click', function() {
	    const type = passwordEle.getAttribute('type');
	    
	    passwordEle.setAttribute(
	        'type',
	        // Switch it to a text field if it's a password field
	        // currently, and vice versa
	        type === 'password' ? 'text' : 'password'
	    );
	});
</script>