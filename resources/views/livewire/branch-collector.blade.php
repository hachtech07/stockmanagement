<span class="input-group-addon">Branch</span>
<select class="input-sm form-control" wire:model="branch" name="branch" id="branch_id">
    <option value=''>Choose a branch</option>
    @foreach($branches as $branch)
    <option value={{ $branch->id }}>{{ $branch->branch_title }}</option>
    @endforeach
</select>
@if(count($collectors) > 0)
<span class="input-group-addon">Collector</span>
<select class="input-sm form-control" wire:model="collector" name="collector" id="collector_id">
    <option value=''>Choose a collector</option>
    @foreach($collectors as $collector)
    <option value={{ $collector->id }}>{{ $collector->name }}</option>
    @endforeach
</select>
@endif