<div class="container-fluid my-3">
    <div class="d-flex row">
        <div class="col-md-12">
            <div class="card my-3 shadow no-b r-0">
                <div class="card-header white">
                    <h6>Branches</h6>
                </div>
                <div class="card-body">
                    <div class="table-responsive">
                            <table class="table table-striped table-hover r-0 data-table">
                                <thead>
                                    <tr class="no-b">
                                        <th>SN</th>
										<th>BRANCH</th>
										<th>ADDRESS</th>
										<th>STATUS</th>
										<th>ACTION</th>
                                    </tr>
                                </thead>
                                <tbody>
                                
                                </tbody>
                            </table>
                    </div>	
                </div>
            </div>
        </div>
    </div>
</div>
<script type="text/javascript">
    $(document).ready(function() {
         $('.data-table').DataTable({
            searching: false,
             processing: true,
             serverSide: true,
             ajax: "{{ route('branch.data') }}",
             columns: [
                {data: 'DT_RowIndex', name: 'DT_RowIndex'},
                {data: 'branch_title', name: 'branch_title'},
                {data: 'address', name: 'address'},
                {data: 'status', name: 'status'},
                {data: 'action', name: 'action', orderable: false, searchable: false},
             ],
         });
     });
 </script>