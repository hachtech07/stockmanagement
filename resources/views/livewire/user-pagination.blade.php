<div class="container-fluid my-3">
    <div class="d-flex row">
        <div class="col-md-12">
            <div class="card my-3 shadow no-b r-0">
                <div class="card-header white">
                    <h6>Users</h6>
                </div>
                <div class="card-body pt-0">
                    <div class="table-responsive">
                        <table class="table table-striped table-hover r-0 data-table">
                            <thead>
                                <tr class="no-b">
                                    <th>SN</th>
                                    <th>NAME</th>
                                    <th>EMAIL</th>
                                    <th>BRANCH</th>
                                    <th>ROLE</th>
                                    <th>STATUS</th>
                                    <th>ACTION</th>
                                </tr>
                            </thead>
                            <tbody>

                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<script type="text/javascript">
    $(document).ready(function() {
        $('.data-table').DataTable({
            searching: false,
            processing: true,
            serverSide: true,
            ajax: "{{ route('user.data') }}",
            columns: [{
                    data: 'DT_RowIndex',
                    name: 'DT_RowIndex'
                },
                {
                    data: 'name',
                    name: 'name'
                },
                {
                    data: 'email',
                    name: 'email'
                },
                {
                    data: 'branch_title',
                    name: 'branch_title'
                },
                {
                    data: 'user_type',
                    name: 'user_type'
                },
                {
                    data: 'status',
                    name: 'status'
                },
                {
                    data: 'action',
                    name: 'action',
                    orderable: false,
                    searchable: false
                },

            ],
        });
    });
</script>