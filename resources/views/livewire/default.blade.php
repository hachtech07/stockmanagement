<!DOCTYPE html>
<html lang="zxx">
    <x-head />
    <body class="light loaded">
        <div id="loader" class="loader">
            <div class="plane-container">
                <div class="preloader-wrapper small active">
                    <div class="spinner-layer spinner-blue">
                        <div class="circle-clipper left">
                            <div class="circle"></div>
                        </div><div class="gap-patch">
                        <div class="circle"></div>
                    </div><div class="circle-clipper right">
                        <div class="circle"></div>
                    </div>
                    </div>

                    <div class="spinner-layer spinner-red">
                        <div class="circle-clipper left">
                            <div class="circle"></div>
                        </div><div class="gap-patch">
                        <div class="circle"></div>
                    </div><div class="circle-clipper right">
                        <div class="circle"></div>
                    </div>
                    </div>

                    <div class="spinner-layer spinner-yellow">
                        <div class="circle-clipper left">
                            <div class="circle"></div>
                        </div><div class="gap-patch">
                        <div class="circle"></div>
                    </div><div class="circle-clipper right">
                        <div class="circle"></div>
                    </div>
                    </div>

                    <div class="spinner-layer spinner-green">
                        <div class="circle-clipper left">
                            <div class="circle"></div>
                        </div><div class="gap-patch">
                        <div class="circle"></div>
                    </div><div class="circle-clipper right">
                        <div class="circle"></div>
                    </div>
                    </div>
                </div>
            </div>
        </div>
        <div id="app">
            <script>
                /*
                 *  Add sidebar classes (sidebar-mini sidebar-collapse sidebar-expanded-on-hover) in body tag
                 *  you can remove this script tag and add classes directly to body
                 *  this is only for demo
                 */
                document.body.className += ' sidebar-mini' + ' sidebar-collapse' + ' sidebar-expanded-on-hover' + ' sidebar-top-offset';
            </script>

           
        </div>
        <div class="page has-sidebar-left height-full">
            <x-toast />
		    <div class="container-fluid my-3">
                <x-sidebar />

                <div class="sticky">
                    
                    <x-navbar />
    
                </div>
		        <div class="row my-3">
		        	<div class="col-md-12">
                        @switch ($type)
                            @case('branch')
                                @livewire('branch-pagination')
                                @break
                            @case('customer')
                                @livewire('customer-pagination')
                                @break
                            @case('user')
                                @livewire('user-pagination')
                                @break
                            @default
                                @break
                        @endswitch
			      	</div>
			    </div>
			</div>
	  
			@livewireScripts

        </div>
        <!-- <x-right-sidebar /> -->

        <!-- Add the sidebar's background. This div must be placed
             immediately after the control sidebar -->
        <div class="control-sidebar-bg shadow white fixed"></div>

        <!--/#app -->
        <script src="{{ url('assets/js/app.js') }}"></script>
        <!--
        --- Footer Part - Use Jquery anywhere at page.
        --- http://writing.colin-gourlay.com/safely-using-ready-before-including-jquery/
        -->
        <script>(function($,d){$.each(readyQ,function(i,f){$(f)});$.each(bindReadyQ,function(i,f){$(d).bind("ready",f)})})(jQuery,document)</script>
        <!-- <x-toast /> -->
    </body>
</html>