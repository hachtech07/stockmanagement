<div class="container-fluid my-3">
    <div class="d-flex row">
        <div class="col-md-12">
            <div class="card my-3 shadow no-b r-0">
                <div class="card-header white">
                    <h6>Customers</h6>
                </div>
                <div class="card-body">
                    <div class="table-responsive">
                        <table class="table table-striped table-hover r-0 data-table">
                            <thead>
                                <tr class="no-b">
                                    <th>SN</th>
                                    <th>BRANCH CODE</th>
                                    <th>CUSTOMER NO.</th>
                                    <th>NAME.</th>
                                    <th>PHONE NO.</th>
                                    <th>MUNICIPALITY</th>
                                    <th>WARD</th>
                                    <th>HOUSE NO</th>
                                    <th>BRANCH</th>
                                    <th>CATEGORY</th>
                                    <th>DUE</th>
                                    <th>ADVANCE</th>
                                    <th>BILL UPTO</th>
                                    <!-- <th>CARD NO.</th> -->
                                    <th>STATUS</th>
                                    <th>Action</th>
                                </tr>
                            </thead>
                            <tbody>

                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<script type="text/javascript">
    $(document).ready(function() {
        $('.data-table').DataTable({
            searching: false,
            processing: true,
            serverSide: true,
            ajax: "{{ route('customer.data') }}",
            columns: [{
                    data: 'DT_RowIndex',
                    name: 'DT_RowIndex'
                },
                {
                    data: 'branch_code',
                    name: 'branch_code'
                },
                {
                    data: 'customer_number',
                    name: 'customer_number'
                },
                {
                    data: 'name',
                    name: 'name'
                },
                {
                    data: 'phone_number',
                    name: 'phone_number'
                },
                {
                    data: 'municipality',
                    name: 'municipality'
                },
                {
                    data: 'ward',
                    name: 'ward'
                },
                {
                    data: 'house_number',
                    name: 'house_number'
                },
                {
                    data: 'branch_title',
                    name: 'branch_title'
                },
                {
                    data: 'category_title',
                    name: 'category_title'
                },
                {
                    data: 'due_amount',
                    name: 'due_amount'
                },
                {
                    data: 'advance_amount',
                    name: 'advance_amount'
                },
                {
                    data: 'last_bill_date',
                    name: 'last_bill_date'
                },
                // {data: 'card_number', name: 'card_number'},
                {
                    data: 'status',
                    name: 'status'
                },
                {
                    data: 'action',
                    name: 'action',
                    orderable: false,
                    searchable: false
                },
            ],
        });
    });
</script>