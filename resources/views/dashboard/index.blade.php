@extends('layouts.admin_layout')

@section('title', 'Dashboard')

@section('content')
<div class="container-fluid my-3">
  <div class="dashboard-data">
    <div class="d-flex row">
      <div class="col-md-12 col-lg-12 p-sm-0 p-md-auto">
        <div class="row h-100">
          <x-dashboard-card :title="'Total Bill Amount Sold'" :backgroundColor="'#b8ffb9'" :icon="'icon-money'" iconBackgroundColor="'#035904'" :total="5" />
          <x-dashboard-card :title="'Total Bill Generated'" :backgroundColor="'#d7ceff'" :icon="'icon-receipt'" iconBackgroundColor="'#0f014d'" :total="6" />
          <x-dashboard-card :title="'Total Bill Amount Collected Today'" :backgroundColor="'#b8fdff'" :icon="'icon-history'" iconBackgroundColor="'#035904'" :total="100" />
          <x-dashboard-card :title="'Total Sold amount Today'" :backgroundColor="'#b8e6ff'" :icon="'icon-bullhorn'" iconBackgroundColor="'#035904'" :total="100" />
          <x-dashboard-card :title="'Total product'" :backgroundColor="'#ffb8b8'" :icon="'icon-person'" iconBackgroundColor="'#450000'" :total="800" />
          <x-dashboard-card :title="'Total sales'" :backgroundColor="'#ffceb8'" :icon="'icon-group'" iconBackgroundColor="'#632103'" :total="900" />
          <x-dashboard-card :title="'Total Bill Returned Amount'" :backgroundColor="'#b8ffb9'" :icon="'icon-money'" iconBackgroundColor="'#632103'" :total="1000" />
          <x-dashboard-card :title="'Total Bill Returned Amount Today'" :backgroundColor="'#d7ceff'" :icon="'icon-receipt'" iconBackgroundColor="'#0f014d'" :total="2500" />
        </div>
      </div>
     
    </div>
  </div>
</div>
{{-- <script src="{{ asset('assets/charts/dashboard-doughnut-chart.js') }}"></script> --}}
@endsection