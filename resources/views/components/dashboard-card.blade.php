<div class="col-md-6">
    <div class="card dash-card no-b r-0  mt-0">
        <div class="dash-card-icon" style="background: {{$backgroundColor}}">
            <i class="icon icon {{$icon}} s-24" style="color: {{$iconBackgroundColor}}"></i>
        </div>

        <div class="card-header bg-transparent">
            <h5 class=""><strong>{{$title}}</strong></h5>
        </div>
        <div class="card-body">
            <h1><strong>{{$total}}</strong></h1>
        </div>
    </div>
</div>