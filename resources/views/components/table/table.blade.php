

<tbody>
    @if(count($discount_report)>0)
    @foreach($discount_report as $discount_reports)
    <tr>

    <x-table.td>{{$loop->iteration}}</x-table.td>
    <x-table.td>{{$discount_reports->customer_name}}</x-table.td>
    <x-table.td>{{$discount_reports->collector_name}}</x-table.td>
    <x-table.td>{{$discount_reports->branch}}</x-table.td>
    <x-table.td>
        <?php
                  $from = $DateConveterTrait->english_to_nepali(date('Y', strtotime($discount_reports->created_at)), date('m', strtotime($discount_reports->created_at)), date('d', strtotime($discount_reports->created_at)));
                  $new_date = $from['year'] . "-" . $from['month'] . "-" . $from['date'];
                  echo $new_date;
                  ?>
    </x-table.td>
    <x-table.td>{{$discount_reports->gross_amount}}</x-table.td>
    <x-table.td>{{$discount_reports->discount_amount}}</x-table.td>
    <x-table.td>{{$discount_reports->net_amount}}</x-table.td>

    </tr>
    @endforeach

    @else
    <td>
      <h3> No data found</h3>
    </td>
    @endif
</tbody>