<?php if ($message = Session::get('success')){?>
<div class="toast"
    data-title="Success"
    data-message="{{$message}}"
    data-type="success">
</div>
<?php }?>

<?php if ($message = Session::get('error')) { ?>
	<div class="toast"
    data-title="Error"
    data-message="{{$message}}"
    data-type="error">
</div>
<?php }?>


<?php if ($message = Session::get('warning')) { ?>
<div class="toast"
    data-title="Warning"
    data-message="{{$message}}"
    data-type="warning">
</div>
<?php } ?>


<?php if ($message = Session::get('info')) { ?>
<div class="toast"
    data-title="Info"
    data-message="{{$message}}"
    data-type="info">
</div>
<?php } ?>


<?php if ($errors->any()) {?>
	@foreach ($errors->all() as $error)
  		<div class="toast"
		    data-title="Error"
		    data-message="{{$error}}"
		    data-type="error">
		</div>
  	@endforeach
<?php }?>