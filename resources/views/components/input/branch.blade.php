<div>
    <span class="input-group-addon">Branch</span>
    <select class="input-sm form-control" name="branch_id" id="branch_id">
      <option value="">Select branch</option>
      @foreach($branches as $branch)
      <option value="{{$branch->id}}">{{$branch->branch_title}}</option>
      @endforeach
    </select>
  </div>