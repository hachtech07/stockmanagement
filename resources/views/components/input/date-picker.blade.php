<div class="input-daterange" id="datepicker">
    <span class="input-group-addon">{{$title}}</span>
    <input type="text" placeholder="Select Date..." class="input-sm form-control nepali-datepicker" {{$attributes}} readonly />
  </div>