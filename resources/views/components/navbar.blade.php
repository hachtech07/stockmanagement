<div class="navbar fixed-top navbar-expand d-flex justify-content-between bd-navbar bg-secondary b-b">
    <div class="relative">
        <div class="d-flex">
            <div>
                <a href="#" data-toggle="push-menu" class="paper-nav-toggle pp-nav-toggle">
                    <i></i>
                </a>
            </div>
            <ul style="align-content: center" class="nav responsive-tab nav-material nav-material-white mt-1 ml-3 text-white" id="v-pills-tab">
                <b>STOCK MANAGEMENT</b>
            </ul>
        </div>
    </div>
    <!--Top Menu Start -->
    <div class="navbar-custom-menu d-flex align-items-center">
        <ul class="nav navbar-nav">
            <!-- Messages-->
            <li class="dropdown custom-dropdown messages-menu d-none">
                <a href="#" class="nav-link pl-lg-3 pr-lg-3" data-toggle="dropdown">
                    <i class="icon-message "></i>
                    <span class="badge badge-success badge-mini rounded-circle">4</span>
                </a>
                <ul class="dropdown-menu dropdown-menu-right">
                    <li>
                        <!-- inner menu: contains the actual data -->
                        <ul class="menu pl-2 pr-2">
                            <!-- start message -->
                            <li>
                                <a href="#">
                                    <div class="avatar float-left">
                                        <img src="{{ url('assets/img/dummy/u4.png')}}" alt="">
                                        <span class="avatar-badge busy"></span>
                                    </div>
                                    <h4>
                                        Support Team
                                        <small><i class="icon icon-clock-o"></i> 5 mins</small>
                                    </h4>
                                    <p>Why not buy a new awesome theme?</p>
                                </a>
                            </li>
                            <!-- end message -->
                            <!-- start message -->
                            <li>
                                <a href="#">
                                    <div class="avatar float-left">
                                        <img src="{{ url('assets/img/dummy/u1.png') }}" alt="">
                                        <span class="avatar-badge online"></span>
                                    </div>
                                    <h4>
                                        Support Team
                                        <small><i class="icon icon-clock-o"></i> 5 mins</small>
                                    </h4>
                                    <p>Why not buy a new awesome theme?</p>
                                </a>
                            </li>
                            <!-- end message -->
                            <!-- start message -->
                            <li>
                                <a href="#">
                                    <div class="avatar float-left">
                                        <img src="{{ url('assets/img/dummy/u2.png') }}" alt="">
                                        <span class="avatar-badge idle"></span>
                                    </div>
                                    <h4>
                                        Support Team
                                        <small><i class="icon icon-clock-o"></i> 5 mins</small>
                                    </h4>
                                    <p>Why not buy a new awesome theme?</p>
                                </a>
                            </li>
                            <!-- end message -->
                            <!-- start message -->
                            <li>
                                <a href="#">
                                    <div class="avatar float-left">
                                        <img src="{{ url('assets/img/dummy/u3.png') }}" alt="">
                                        <span class="avatar-badge busy"></span>
                                    </div>
                                    <h4>
                                        Support Team
                                        <small><i class="icon icon-clock-o"></i> 5 mins</small>
                                    </h4>
                                    <p>Why not buy a new awesome theme?</p>
                                </a>
                            </li>
                            <!-- end message -->
                        </ul>
                    </li>
                    <li class="footer s-12 p-2 text-center"><a href="#">See All Messages</a></li>
                </ul>
            </li>
            <!-- Notifications -->
            <li class="dropdown custom-dropdown notifications-menu d-none">
                <a href="#" class="nav-link pl-lg-3 pr-lg-3" data-toggle="dropdown" aria-expanded="false">
                    <i class="icon-notifications "></i>
                    <span class="badge badge-danger badge-mini rounded-circle">4</span>
                </a>
                <ul class="dropdown-menu dropdown-menu-right">
                    <li class="header">You have 10 notifications</li>
                    <li>
                        <!-- inner menu: contains the actual data -->
                        <ul class="menu">
                            <li>
                                <a href="#">
                                    <i class="icon icon-data_usage text-success"></i> 5 new members joined today
                                </a>
                            </li>
                            <li>
                                <a href="#">
                                    <i class="icon icon-data_usage text-danger"></i> 5 new members joined today
                                </a>
                            </li>
                            <li>
                                <a href="#">
                                    <i class="icon icon-data_usage text-yellow"></i> 5 new members joined today
                                </a>
                            </li>
                        </ul>
                    </li>
                    <li class="footer p-2 text-center"><a href="#">View all</a></li>
                </ul>
            </li>

            <li class="nav-item dropdown custom-dropdown notifications-menu">
                <a href="#" class="nav-link  pl-lg-3 pr-lg-3 dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
                    <i class="icon-user "></i>
                </a>
                <ul class="dropdown-menu dropdown-menu-right">
                    <li class="header text-center"><strong>{{ Auth::user()->name }}</strong></li>
                    <li>
                        <!-- inner menu: contains the actual data -->
                        <ul class="menu">
                            <li>
                                <a href="{{ route('updateprofile') }}">
                                    <i class="icon icon-refresh text-info"></i> Profile
                                </a>
                            </li>
                            <li>
                                <a href="{{ route('changepass') }}">
                                    <i class="icon icon-lock text-info"></i> Change Password
                                </a>
                            </li>
                            <li>
                                <a href="{{ route('logout') }}">
                                    <i class="icon icon-power text-danger"></i> Log Out
                                </a>
                            </li>
                        </ul>
                    </li>
                </ul>
            </li>

        </ul>
    </div>
</div>