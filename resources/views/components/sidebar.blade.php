<aside class="main-sidebar fixed offcanvas" data-toggle='offcanvas'>
    <section class="sidebar">
        <ul class="sidebar-menu mt-4">
            <li class="treeview">
                <a href="{{ route('dashboard') }}">
                    <i class="icon icon-bar-chart s-24"></i> <span>Dashboard</span>
                </a>
            </li>
            <li class="treeview">
                <a href="#">
                    <i class="icon icon icon-group s-24"></i>
                    <span>Users</span>
                </a>
                <ul class="treeview-menu">
                    <li><a href="{{ route('users') }}"><i class="icon icon-list-ol"></i>List Users</a></li>
                    <li><a href="{{ route('users.create') }}"><i class="icon icon-add"></i>Add User</a></li>
                </ul>
            </li>
            <li class="treeview">
                <a href="{{ route('role')}}">
                    <i><svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-box-seam" viewBox="0 0 16 16">
                        <path d="M8.186 1.113a.5.5 0 0 0-.372 0L1.846 3.5l2.404.961L10.404 2l-2.218-.887zm3.564 1.426L5.596 5 8 5.961 14.154 3.5l-2.404-.961zm3.25 1.7-6.5 2.6v7.922l6.5-2.6V4.24zM7.5 14.762V6.838L1 4.239v7.923l6.5 2.6zM7.443.184a1.5 1.5 0 0 1 1.114 0l7.129 2.852A.5.5 0 0 1 16 3.5v8.662a1 1 0 0 1-.629.928l-7.185 2.874a.5.5 0 0 1-.372 0L.63 13.09a1 1 0 0 1-.63-.928V3.5a.5.5 0 0 1 .314-.464L7.443.184z"/>
                      </svg></i>
                    <span>Product</span>
                </a>
                <ul class="treeview-menu">
                    <li><a href="{{ route('product.create') }}"><i class="icon icon-list-ol"></i>Add Product</a></li>
                    <li><a href="{{ route('product.import') }}"><i class="icon icon-upload"></i>Import Product</a></li>
                    <li><a href="{{ route('product.index') }}"><i class="icon icon-add"></i>List Product</a></li>
                    {{-- <li><a href="{{ route('customer.import') }}"><i class="icon icon-upload"></i>List Product</a></li> --}}
                </ul>
            </li>
            <li class="treeview">
                <a href="{{ route('role')}}">
                    <i><svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-box-seam" viewBox="0 0 16 16">
                        <path d="M8.186 1.113a.5.5 0 0 0-.372 0L1.846 3.5l2.404.961L10.404 2l-2.218-.887zm3.564 1.426L5.596 5 8 5.961 14.154 3.5l-2.404-.961zm3.25 1.7-6.5 2.6v7.922l6.5-2.6V4.24zM7.5 14.762V6.838L1 4.239v7.923l6.5 2.6zM7.443.184a1.5 1.5 0 0 1 1.114 0l7.129 2.852A.5.5 0 0 1 16 3.5v8.662a1 1 0 0 1-.629.928l-7.185 2.874a.5.5 0 0 1-.372 0L.63 13.09a1 1 0 0 1-.63-.928V3.5a.5.5 0 0 1 .314-.464L7.443.184z"/>
                      </svg></i>
                    <span>Sales</span>
                </a>
                <ul class="treeview-menu">
                    <li><a href=""><i class="icon icon-list-ol"></i>Add Product</a></li>
                    <li><a href="{{ route('sale.index') }}"><i class="icon icon-add"></i>List Sales</a></li>
                    {{-- <li><a href="{{ route('customer.import') }}"><i class="icon icon-upload"></i>List Product</a></li> --}}
                </ul>
            </li>
            <li class="treeview">
                <a href="#">
                    <i class="icon icon icon-sitemap s-24"></i>
                    <span>Product Category</span>
                    <span class="badge r-3 badge-primary pull-right"></span>
                </a>
                <ul class="treeview-menu">
                    <li><a href="{{ route('product.category') }}"><i class="icon icon-list-ol"></i>List Product Categories</a></li>
                    <li><a href="{{ route('product.category.create') }}"><i class="icon icon-add"></i>Add Product Category</a></li>
                </ul>
            </li>
            @if(Auth::user()->user_type == 'super  duper admin')
            <li class="treeview">
                <a href="{{ route('role')}}">
                    <i class="icon icon icon-sitemap s-24"></i>
                    <span>Role Permissions</span>
                </a>
            </li>
            @endif
        </ul>
    </section>
</aside>