<div class="toast"
    data-title="Success"
    data-message="{{$message}}"
    data-type="success">
</div>
<?php if ($message = Session::get('success')){?>
<div class="alert alert-success alert-dismissible" role="alert">
    <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span>
    </button>
    <strong>Success!</strong> <?= $message ?>.
</div>
<?php }?>


<?php if ($message = Session::get('error')) { ?>
<div class="alert alert-danger alert-dismissible" role="alert">
    <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span>
    </button>
    <strong>Error!</strong> <?= $message ?>.
</div>
<?php }?>


<?php if ($message = Session::get('warning')) { ?>
<div class="alert alert-danger alert-dismissible" role="alert">
    <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span>
    </button>
    <strong>Warning!</strong> <?= $message ?>.
</div>
<?php } ?>


<?php if ($message = Session::get('info')) { ?>
<div class="alert alert-info alert-dismissible" role="alert">
    <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span>
    </button>
    <strong>Info!</strong> <?= $message ?>.
</div>
<?php } ?>


<?php if ($errors->any()) {?>
<div class="alert alert-danger alert-dismissible" role="alert">
    <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span>
    </button>
    <strong>Danger!</strong> <?= $message ?>.
</div>
<?php }?>