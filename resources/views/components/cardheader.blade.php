<div class="card-header white">
	<strong>{{ $title }}</strong>
	<a href="{{ $link }}">
		<button class="btn btn-dark btn-xs float-right">
			<i class="icon icon-{{$icon}}"></i>{{ $buttonLabel }}
		</button>
	</a>
</div>