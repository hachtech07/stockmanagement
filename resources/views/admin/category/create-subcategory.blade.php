@extends('layouts.admin_layout')

@section('title', 'Product Category')

@section('content')
	

    <div class="container-fluid my-3">
        <div class="d-flex row">
            <div class="col-md-12">
	            <div class="card my-3 shadow no-b r-0">
					<?php $link = route('product.category') ?>
                	<x-cardheader title="Add Product Category" :link="$link" :icon="'list-ul'" :buttonLabel="'Product Category List'" />
	                <div class="card-body pt-0">
						<form method="POST" action={{ route("product.subcategory.create",$productCategory->id) }}>
								<div class="row">
														@csrf
								<div class="col-md-6 mb-3 form-group">
									<label for="name">Sub Category Title <span class="text-danger">*</span></label>
									<input type="text" class="form-control @error('subcategory_name') is-invalid @enderror" id="subcategory_name" placeholder="Catergory Title"  name="subcategory_name" value="{{ old('subcategory_name') }}" required>
									@error('subcategory_name')
										<div class="invalid-feedback">{{$message}}</div>
									@enderror
								</div>
									<input type="hidden" class="form-control  id="category_id"  name="category_id" value="{{$productCategory->id}}" required>

								<div class="col-md-6 mb-3 form-group">
									<label for="status">Status</label>
									   <select class="form-control @error('subcategory_status') is-invalid @enderror" name="subcategory_status" required>
										   <option value="active">Active</option>
										   <option value="inactive">Inactive</option>
									   </select>
									@error('subcategory_status')
										<div class="invalid-feedback">{{$message}}</div>
									@enderror
								</div>
								<div class="col-md-4 mb-3 form-group">
									<button class="btn btn-sm btn-primary px-5" type="submit">Save</button>
								</div>
							</div>
							</form>
	                </div>
	            </div>
            </div>
        </div>
    </div>


@endsection