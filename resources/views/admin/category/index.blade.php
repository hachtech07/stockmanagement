@extends('layouts.admin_layout')

@section('title', 'Product Category List')

@section('content')
<?php

$controller_name = request()->segment(count(request()->segments()));
?>

	<?php //$links = ['List Country' => url('/country/list') , 'Add Country' => url('/country/create')]?>

    <div class="container-fluid my-3">
        <div class="row my-3">
        	<div class="col-md-12">
                <div class="card r-0">
                    <?php $link = "" ?>
                <x-cardheader title="Product Category" :link="$link" :icon="'list-ul'" :buttonLabel="'Create  Category'" />
                    <dv class="">
                        <div class="table-responsive">
                            <table class="table table-striped table-hover r-0">
                                <thead>
                                <tr class="no-b">
                                    <th>SN</th>
                                    <th>CATEGORY TITLE</th>
                                    <th>STATUS</th>
                                    <th>Action</th>
                                </tr>
                                </thead>
                                <tbody>
                                    @if(count($productCategories)>0)
                                    @foreach($productCategories as $productCategory)
                                        <tr>
                                            <td>{{ $loop->iteration }}</td>
                                            <td>{{ $productCategory->name }}</td>
                                            <td><span class="icon icon-circle s-12  mr-2 <?= ($productCategory->status == 'active') ? 'text-success':'text-warning'?>"></span>{{ $productCategory->status }}</td>
                                            <td>
                                                <a href="{{route('product.category.update', $productCategory->id)}}" class="btn btn-primary btn-xs">Edit</a>
                                                <a href="{{route('product.subcategory.create',$productCategory->id)}}" class="btn btn-primary btn-xs">Add Subcategory</a>
                                                <a href="{{route('product.subcategory',$productCategory->id)}}" class="btn btn-primary btn-xs">View Subcategory</a>
                                                
                                                
                                            </td>
                                        </tr>
                                    @endforeach
                                    @else
                                    <td><h3>No product category found<h3></td>
                                    @endif
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>


@endsection
