@extends('layouts.admin_layout')

@section('title', 'List Subca')

@section('content')
<?php

$controller_name = request()->segment(count(request()->segments()));
?>

    <div class="container-fluid my-3">
        <div class="row my-3">
        	<div class="col-md-12">
                <div class="card r-0">
                    <div class="card-header white">
                        <strong> SubCategories of : </strong>
                        <a href="{{ route('product.category') }}">
                            <button class="btn btn-dark btn-xs float-right">
                                <i class="icon icon-plus"></i>List Category
                            </button>
                        </a>
                    </div>
                    <div class="card-body b-b pt-0 bg-light">
                        {{-- <form action="{{route('role.add-permissions', $user_id)}}" method="post"> --}}
                            {{-- @csrf --}}
                            {{-- <input type="hidden" name="user_id" value="{{$user_id}}"> --}}
                            <ul class="list-unstyled">
                                <li class="pt-3 pb-3 bg-light sticky">
                                    <strong>Total SubCategory</strong>
                                    <span class="badge r-3 badge-success float-right">{{count($subcategories)}}</span>
                                </li>
                                @if (count($subcategories) > 0)
                                @foreach($subcategories as $subcategory)
                                    <li class="my-1">
                                        <div class="card b-b p-3 mt-2">
                                            <div class="">
                                                <div class="float-right">
                                                    
                                                </div>
                                                <div class="image mr-3  float-left">
                                                    {{ $subcategory->subcategory_name }}
                                                </div>
                                            </div>
                                        </div>
                                    </li>
                                @endforeach
                                @else
                                <h5>No subcatogory exist</h5>
                                @endif
                            </ul>
                            {{-- <button type="submit" class="btn btn-primary btn-sm">Add Permissions</button> --}}
                        {{-- </form> --}}
                    </div>
                </div>
            </div>
        </div>
    </div>


@endsection
