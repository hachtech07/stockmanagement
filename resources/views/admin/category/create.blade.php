@extends('layouts.admin_layout')

@section('title', 'Product Category')

@section('content')
	

    <div class="container-fluid my-3">
        <div class="d-flex row">
            <div class="col-md-12">
	            <div class="card my-3 shadow no-b r-0">
					<?php $link = route('product.category') ?>
                	<x-cardheader title="Add Product Category" :link="$link" :icon="'list-ul'" :buttonLabel="'Product Category List'" />
	                <div class="card-body pt-0">
						<form method="POST" action={{ route("product.category.create") }}>
								<div class="row">
														@csrf
								<div class="col-md-6 mb-3 form-group">
									<label for="name">Category Title <span class="text-danger">*</span></label>
									<input type="text" class="form-control @error('name') is-invalid @enderror" id="name" placeholder="Catergory Title"  name="name" value="{{ old('name') }}" required>
									@error('name')
										<div class="invalid-feedback">{{$message}}</div>
									@enderror
								</div>
								<div class="col-md-6 mb-3 form-group">
									<label for="status">Status</label>
									   <select class="form-control @error('status') is-invalid @enderror" name="status" required>
										   <option value="active">Active</option>
										   <option value="inactive">Inactive</option>
									   </select>
									@error('status')
										<div class="invalid-feedback">{{$message}}</div>
									@enderror
								</div>
								<div class="col-md-4 mb-3 form-group">
									<button class="btn btn-sm btn-primary px-5" type="submit">Save</button>
								</div>
							</div>
							</form>
	                </div>
	            </div>
            </div>
        </div>
    </div>


@endsection