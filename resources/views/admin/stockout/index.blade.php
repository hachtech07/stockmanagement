@extends('layouts.admin_layout')

@section('title', 'Sales Report')
@inject('DateConveterTrait', 'App\Http\Controllers\Admin\ProductController')
@section('content')
<div class="container-fluid my-3">
  <div class="d-flex row">
    <div class="col-md-12">
      <div class="card my-3 shadow no-b r-0">
        <div class="card-header white d-flex justify-content-between">
          <h6>Sales List</h6>
          <div class="form-group">
            <form method="GET" action={{route('export.sales')}} id="export-form">
              @csrf
              <input type="submit" class="btn btn-sm btn-primary" value="Export to excel">
            </form>
          </div>
        </div>
        <div class="card-body pt-0 pb-3">
          <div class="row">
            <div class="col-xs-6 form-inline">
              <form class="input-daterange input-group" id="date-search" method="POST" action={{route('sale.index')}}>
                @csrf
                <x-input.date-picker :title="'From'" name="from" id="from" value="{{ $froms != null ? $froms:old('from')}}" />
                <x-input.date-picker :title="'To'" name="to" id="to" value="{{ $tos != null ? $tos:old('to')}}" />
                
                <div class="col-sm-2">
                  <span class="input-group-addon">Product</span>
                  <div class="form-group">
                    <div class="form-line">
                      <input id="product_name" name="product_name" type="text" value="{{ $product_name != null ? $product_name:old('product_name')}}" class="form-control ">
                                        </div>
                  </div>
                </div>
                <div>
                  <span class="input-group-addon">Category</span>
                  <select class="input-sm form-control" name="category_id" id="category_id">
                    <option value="">Select category</option>
                    @foreach($categories as $category)
                    <option value="{{$category->id}}" {{ ($categorys == $category->id) ? 'selected' : ''}}>{{$category->name}}</option>
                    @endforeach
                  </select>
                </div>
                <div>
                  <span class="input-group-addon">SubCategory</span>
                  <select class="input-sm form-control" name="product_sub_category_id" id="product_sub_category_id">
                    <option value="">please select category</option>
                    @foreach ($selectedSubCategories as $selectedSubCategory)
                    <option value="{{$selectedSubCategory->id}}" {{($sub_category == $selectedSubCategory->id) ? 'selected':''}}>{{$selectedSubCategory->subcategory_name}}</option>
                    @endforeach
                  </select>
                </div>
                <div>
                  {{-- <span class="input-group-addon">Batch No</span> --}}
                  <select class="input-sm form-control batch" name="batch" id="batch">
                    <option value="">Select Batch</option>
                    @foreach ($batchs as $batch)
                    <option value="{{$batch->batch_no}}" {{($batch_nos == $batch->batch_no) ? 'selected':''}}>{{$batch->batch_no}}</option>
                    @endforeach
                  </select>
                </div>
                <div>
                  {{-- <span class="input-group-addon">Fiscal Year</span> --}}
                  <select class="input-sm form-control fiscal_year" name="fiscal_year" id="fiscal_year">
                    <option value="">Select Fiscal Year</option>
                    @foreach ($fiscal_years as $fiscal)
                    <option value="{{$fiscal->id}}" {{($fiscal_year == $fiscal->id) ? 'selected':''}}>{{$fiscal->fiscal_years}}</option>
                    @endforeach
                  </select>
                </div>
                <div>
                  <input type="submit" value="Search" id="dateSearch" class="btn btn-sm btn-primary">
                </div>
                <div class="mx-auto" style="width: 100px;">
                  <a href="{{route("sale.index") }}" class="btn btn-sm btn-danger" role="button">Reset</a>
                </div>
              </form>
            </div>
          </div>
         
        </div>
        <br>
        <div class="table-responsive">
          <table id="myTable" class="table table-striped table-hover r-0 data-table">
            <thead>
              <tr class="no-b">
                <th class="visible-on-export">SN</th>
                <th class="visble-on-export">Product Name.</th>
                <th class="visible-on-export">Product Category</th>
                <th class="visible-on-export">Product Sub Category</th>
                <th class="visible-on-export">Product Batch NO.</th>
                <th class="visible-on-export">Product Color</th>
                <th class="visible-on-export">Model</th>
                <th class="visible-on-export">Quantity</th>
                <th class="visible-on-export">MRP Per Unit</th>
                <th class="visible-on-export">Total</th>
                <th class="visible-on-export">Fiscal years</th>
                <th class="visible-on-export">Sold by</th>
                <th class="visible-on-export">Sold At</th>

              </tr>
            </thead>
            <tbody>
              @if(count($sales)>0)
              @foreach($sales as $sale)
              <tr>
                <td>{{$loop->iteration}}</td>
                <td>{{$sale->product_name}}</td>
                <td>{{$sale->category_name}}</td>
                <td>{{$sale->sub_category}}</td>
                <td>{{$sale->batch_no }}</td>
                <td>{{$sale->color}}</td>
                <td>{{$sale->model}}</td>
                <td>{{$sale->quantity}}</td>
                <td>{{$sale->mrp}}</td>
                <td>{{$sale->mrp * $sale->quantity}}</td>
                {{-- <td>{{$sale->entry_date}}</td> --}}
                <td>{{$sale->fiscal_years}}</td>
                <td>{{$sale->first_name}}</td>
                <td>
                <?php
                $from = $DateConveterTrait->english_to_nepali(date('Y', strtotime($sale->created_at)), date('m', strtotime($sale->created_at)), date('d', strtotime($sale->created_at)));
                $new_date = $from['year'] . "-" . $from['month'] . "-" . $from['date'];
                echo $new_date;
                ?>
                </td>
                
              </tr>
              @endforeach
              @endif
            </tbody>
            <tfoot>
              <tr>
                <th></th>
                <th></th>
                <th></th>
                <th></th>
                <th></th>
                <th></th>
                <th>Total</th>
                <th id="gross_amount"></th>
                <th id="discount_amount"></th>
                <th id="vat_amount"></th>
                <th></th>
                <th></th>
                <th></th>
                <th></th>
                <th id="tender_amount"></th>
                <th id="due_amount"></th>
                <th id="advance_amount"></th>

              </tr>
            </tfoot>
          </table>
          {!! $sales->appends(Request::except(['page','_token']))->links() !!}
        </div>
      </div>
    </div>
  </div>
</div>
</div>
<script type="text/javascript">
  $(document).ready(function() {
    let current = NepaliFunctions.GetCurrentBsDate();
    const disable = `${current.year}-${current.month}-${current.day}`;
    $('#to').nepaliDatePicker({
      disableAfter: disable,
    });
    $('#from').nepaliDatePicker({
      onChange: function() {
        $('#to').nepaliDatePicker({
          disableBefore: $('#from').val(),
          disableAfter: disable,
        });
      }
    });
  })
</script>
<script type="text/javascript">
	$(document).ready(function() {
		$('select[name="category_id"]').on('change', function() {
			var categoryId = $(this).val();
			if (categoryId) {
				$.ajax({
					url: "{{url('/product/subcategory-api')}}/" + categoryId,
					type: "GET",
					dataType: "json",
					success: function(data) {
						$('select[name="product_sub_category_id"]').empty();
						$('select[name="product_sub_category_id"]').append('<option value="">Select sub category</option>');
						$.each(data, function(key, value) {
							$('select[name="product_sub_category_id"]').append('<option value="' + value.id + '">' + value.subcategory_name + '</option>');
						});
					}
				});
			} else {
				$('select[name="product_sub_category_id"]').empty();
				$('select[name="product_sub_category_id"]').append('<option value="">' + 'Select sub category' + '</option>');
			}
		});
	});
</script>
<script>
  function sumColumn(num) {
    let rows = document.querySelectorAll('table tr td:nth-child(' + num + ')');
    let amount = 0;
    for (let i = 0; i < rows.length; i++) {
      amount += parseFloat(rows[i].textContent.replace(",", ""));
    }
    return amount;
  }
  let grossAmount = sumColumn(8);
  let discountAmount = sumColumn(9);
  let vatAmount = sumColumn(10)
  let netAmount = sumColumn(11);
  let tenderAmount = sumColumn(14);
  let dueAmount = sumColumn(15);
  let advanceAmount = sumColumn(16);

  document.getElementById("gross_amount").textContent = `${isNaN(grossAmount) ? 0:grossAmount.toFixed(2).toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",")}`;
  document.getElementById("discount_amount").textContent = `${isNaN(discountAmount) ? 0:discountAmount.toFixed(2).toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",")}`;
  document.getElementById("vat_amount").textContent = `${isNaN(vatAmount) ? 0:vatAmount.toFixed(2).toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",")}`;
  document.getElementById("net_amount").textContent = `${isNaN(netAmount) ? 0:netAmount.toFixed(2).toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",")}`;
  document.getElementById("tender_amount").textContent = `${isNaN(tenderAmount) ? 0:tenderAmount.toFixed(2).toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",")}`;
  document.getElementById("advance_amount").textContent = `${isNaN(advanceAmount) ? 0:advanceAmount.toFixed(2).toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",")}`;
  document.getElementById("due_amount").textContent = `${isNaN(dueAmount) ? 0:dueAmount.toFixed(2).toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",")}`;
</script>
@endsection