@extends('layouts.admin_layout')

@section('title', 'Sale Product')

@section('content')
<div class="container-fluid my-3">
	<div class="d-flex row">
		<div class="col-md-12">
			<div class="card my-3 shadow no-b r-0">
				<?php $link = ""?>
				<x-cardheader title="Sale Product" :link="$link" :icon="'list-ul'" :buttonLabel="'Customer List'" />

				<div class="card-body pt-0">
					<form method="POST" class="from-prevent-multiple-submits" action={{ route("sale.create") }}>
						<div class="row">
							@csrf
							<div class="col-md-6 mb-3 form-group">
								<label for="product_name">Product name <span class="text-danger">*</span></label>
								<input type="text" class="form-control @error('product_name') is-invalid @enderror" id="product_name" placeholder="Product Number" name="product_name" value="{{$product->product_name}}" readonly>
								@error('product_name')
								<div class="invalid-feedback">{{$message}}</div>
								@enderror
							</div>
								<input type="hidden" class="form-control" id="product_id" name="product_id" value="{{ $product->id }}">
							<div class="col-md-6 mb-3 form-group">
								<label for="product_category">Product Category <span class="text-danger">*</span></label>
                                <input type="text" class="form-control @error('product_category') is-invalid @enderror" id="product_category" placeholder="Product Number" name="product_category" value="{{ $product->product_category}}" disabled>
								@error('product_category')
								<div class="invalid-feedback">{{$message}}</div>
								@enderror
							</div>
							<div class="col-md-6 mb-3 form-group">
								<label for="product_sub_category">Product  SubCategory <span class="text-danger">*</span></label>
                                <input type="text" class="form-control @error('product_sub_category') is-invalid @enderror" id="product_sub_category" placeholder="Product Number" name="product_sub_category" value="{{ $product->sub_category_name}}" disabled>
								@error('product_sub_category')
								<div class="invalid-feedback">{{$message}}</div>
								@enderror
							</div>
							<div class="col-md-6 mb-3 form-group">
								<label for="batch_no">Product Batch <span class="text-danger">*</span></label>
								<input type="text" class="form-control @error('batch_no') is-invalid @enderror" id="batch_no" placeholder="Bstch no." name="batch_no" value="{{ $product->batch_no }}" readonly>
								@error('batch_no')
								<div class="invalid-feedback">{{$message}}</div>
								@enderror
							</div>
							<div class="col-md-6 mb-3 form-group">
								<label for="color">Product color <span class="text-danger">*</span></label>
								<input type="text" class="form-control @error('color') is-invalid @enderror" id="color" placeholder="product color" name="color" value="{{ $product->color }}" readonly>
								@error('color')
								<div class="invalid-feedback">{{$message}}</div>
								@enderror
							</div>
							<div class="col-md-6 mb-3 form-group">
								<label for="model">Product Model <span class="text-danger">*</span></label>
								<input type="text" class="form-control @error('model') is-invalid @enderror" id="model" placeholder="product model" name="model" value="{{ $product->model }}" readonly>
								@error('model')
								<div class="invalid-feedback">{{$message}}</div>
								@enderror
							</div>
							<div class="col-md-6 mb-3 form-group">
								<label for="unit">Quantity <span class="text-danger">*</span></label>
								<input type="text" class="form-control @error('quantity') is-invalid @enderror" id="quantity" placeholder="product unit" name="quantity" value="{{ $product->unit }}" required>
								@error('quantity')
								<div class="invalid-feedback">{{$message}}</div>
								@enderror
							</div>
							<div class="col-md-6 mb-3 form-group">
								<label for="mrp">MRP/unit <span class="text-danger">*</span></label>
								<input type="text" class="form-control @error('mrp') is-invalid @enderror" id="mrp" placeholder="mrp" name="mrp" value="{{ $product->mrp }}" required>
								@error('mrp')
								<div class="invalid-feedback">{{$message}}</div>
								@enderror
							</div>
                            <div class="col-md-6 mb-3 form-group">
								<label for="mrp">Total</label>
								<input type="text" class="form-control  id="mrp" name="mrp"  disabled>
								
							</div>
							<div class="col-md-12 mb-3 form-group">
								<button class="btn btn-sm btn-primary px-5 from-prevent-multiple-submit" type="submit" name="saveExit">Save</button>
							</div>
						</div>
					</form>
				</div>
			</div>
		</div>
	</div>
</div>
<script type="text/javascript">
	$(document).ready(function() {
		$('#nepali-datepicker').nepaliDatePicker();
	});
</script>
<script type="text/javascript">
	$(document).ready(function() {
		$('select[name="branch_id"]').on('change', function() {
			var branchId = $(this).val();
			if (branchId) {
				$.ajax({
					url: "{{url('/dashboard/ajaxuser')}}/" + branchId,
					type: "GET",
					dataType: "json",
					success: function(data) {
						$('select[name="collector_id"]').empty();
						$('select[name="collector_id"]').append('<option value="">Select collector</option>');
						$.each(data, function(key, value) {
							$('select[name="collector_id"]').append('<option value="' + value.id + '">' + value.name + '</option>');
						});
					}
				});
			} else {
				$('select[name="collector_id"]').empty();
				$('select[name="collector_id"]').append('<option value="">' + 'Select user' + '</option>');
			}
		});
	});
</script>
<script type="text/javascript">
	$(document).ready(function() {
		$('select[name="branch_id"]').on('change', function() {
			var branchId = $(this).val();

			if (branchId) {
				$.ajax({
					url: "{{url('/customer/last-customer-id')}}/" + branchId,
					type: "GET",
					dataType: "json",
					success: function(data) {
						$('#customer_number').val(data);
					}
				});
			} else {
				$('#customer_number').val('')
			}
		});
	})
</script>
<script>
	$(document).ready(function(ev) {
		$(".from-prevent-multiple-submit").on('click', function(ev) {
			var $form = $(this).closest('form');

			if ($form.valid()) {

				$(this).prop('disabled', true);

				// (optional: if multiple submit button, specify which one has been cliqued to the MVC controler)
				$form.append('<input type="hidden" name="save" value="save" /> ');

				$form.submit();
			}
		});
	});
</script>
@endsection