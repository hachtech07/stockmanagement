@extends('layouts.admin_layout')

@section('title', 'Add Product')

@section('content')
<div class="container-fluid my-3">
	<div class="d-flex row">
		<div class="col-md-12">
			<div class="card my-3 shadow no-b r-0">
				<?php $link = ""?>
				<x-cardheader title="Add Product" :link="$link" :icon="'list-ul'" :buttonLabel="'Customer List'" />

				<div class="card-body pt-0">
					<form method="POST" class="from-prevent-multiple-submits" action={{ route("product.create") }}>
						<div class="row">
							@csrf
							<div class="col-md-6 mb-3 form-group">
								<label for="product_name">Product name <span class="text-danger">*</span></label>
								<input type="text" class="form-control @error('product_name') is-invalid @enderror" id="customer_number" placeholder="Product Number" name="product_name" value="{{ old('product_name') }}" required>
								@error('product_name')
								<div class="invalid-feedback">{{$message}}</div>
								@enderror
							</div>
							<div class="col-md-6 mb-3 form-group">
								<label for="product_category_id">Product Category <span class="text-danger">*</span></label>
								<select class="form-control @error('product_category_id') is-invalid @enderror" name="product_category_id" id="product_category_id" required>
									<option value="">Select Product Category</option>
									@foreach($categories as $category)
									<option value="{{ $category -> id }}" {{ (old('product_category_id') == $category ->id) ? 'selected' : ''}}>{{ $category -> name }}</option>
									@endforeach
								</select>
								@error('product_category_id')
								<div class="invalid-feedback">{{$message}}</div>
								@enderror
							</div>
							<div class="col-md-6 mb-3 form-group">
								<label for="product_sub_category_id">Product SubCategory <span class="text-danger">*</span></label>
								<select class="form-control @error('product_sub_category_id') is-invalid @enderror" name="product_sub_category_id" id="product_sub_category_id" required>
									<option value="">Please select Category</option>
								</select>
								@error('product_sub_category_id')
								<div class="invalid-feedback">{{$message}}</div>
								@enderror
							</div>
							<div class="col-md-6 mb-3 form-group">
								<label for="batch_no">Product Batch <span class="text-danger">*</span></label>
								<input type="text" class="form-control @error('batch_no') is-invalid @enderror" id="batch_no" placeholder="Bstch no." name="batch_no" value="{{ old('batch_no') }}" required>
								@error('batch_no')
								<div class="invalid-feedback">{{$message}}</div>
								@enderror
							</div>
							<div class="col-md-6 mb-3 form-group">
								<label for="color">Product color <span class="text-danger">*</span></label>
								<input type="text" class="form-control @error('color') is-invalid @enderror" id="color" placeholder="product color" name="color" value="{{ old('color') }}" required>
								@error('color')
								<div class="invalid-feedback">{{$message}}</div>
								@enderror
							</div>
							<div class="col-md-6 mb-3 form-group">
								<label for="model">Model <span class="text-danger"></span></label>
								<input type="text" class="form-control @error('model') is-invalid @enderror" id="model" placeholder="product model" name="model" value="{{ old('model') }}">
								@error('model')
								<div class="invalid-feedback">{{$message}}</div>
								@enderror
							</div>
							<div class="col-md-6 mb-3 form-group">
								<label for="unit">Quantity <span class="text-danger">*</span></label>
								<input type="text" class="form-control @error('unit') is-invalid @enderror" id="unit" placeholder="product unit" name="unit" value="{{ old('unit') }}" required>
								@error('unit')
								<div class="invalid-feedback">{{$message}}</div>
								@enderror
							</div>
							<div class="col-md-6 mb-3 form-group">
								<label for="cp">Cost Price (CP)/unit <span class="text-danger">*</span></label>
								<input type="text" class="form-control @error('cp') is-invalid @enderror" id="address" placeholder="Cost price" name="cp" value="{{ old('cp') }}" required>
								@error('cp')
								<div class="invalid-feedback">{{$message}}</div>
								@enderror
							</div>
							<div class="col-md-6 mb-3 form-group">
								<label for="mrp">MRP/unit <span class="text-danger">*</span></label>
								<input type="text" class="form-control @error('mrp') is-invalid @enderror" id="mrp" placeholder="mrp" name="mrp" value="{{ old('mrp') }}" required>
								@error('mrp')
								<div class="invalid-feedback">{{$message}}</div>
								@enderror
							</div>
							
							<div class="col-md-6 mb-3 form-group">
								<label for="stock_alert">Stock alert <span class="text-danger">*</span></label>
								<input type="number" class="form-control @error('stock_alert') is-invalid @enderror" id="stock_alert" placeholder="Stock alert" name="stock_alert" value="{{ old('stock_alert') }}" required>
								@error('stock_alert')
								<div class="invalid-feedback">{{$message}}</div>
								@enderror
							</div>
							<div class="col-md-12 mb-3 form-group">
								<button class="btn btn-sm btn-primary px-5 from-prevent-multiple-submit" type="submit" name="saveExit">Save</button>
							</div>
						</div>
					</form>
				</div>
			</div>
		</div>
	</div>
</div>
<script type="text/javascript">
	$(document).ready(function() {
		$('#nepali-datepicker').nepaliDatePicker();
	});
</script>
<script type="text/javascript">
	$(document).ready(function() {
		$('select[name="product_category_id"]').on('change', function() {
			var categoryId = $(this).val();
			if (categoryId) {
				$.ajax({
					url: "{{url('/product/subcategory-api')}}/" + categoryId,
					type: "GET",
					dataType: "json",
					success: function(data) {
						$('select[name="product_sub_category_id"]').empty();
						$('select[name="product_sub_category_id"]').append('<option value="">Select sub category</option>');
						$.each(data, function(key, value) {
							$('select[name="product_sub_category_id"]').append('<option value="' + value.id + '">' + value.subcategory_name + '</option>');
						});
					}
				});
			} else {
				$('select[name="product_sub_category_id"]').empty();
				$('select[name="product_sub_category_id"]').append('<option value="">' + 'Select sub category' + '</option>');
			}
		});
	});
</script>
<script>
	$(document).ready(function(ev) {
		$(".from-prevent-multiple-submit").on('click', function(ev) {
			var $form = $(this).closest('form');

			if ($form.valid()) {

				$(this).prop('disabled', true);

				// (optional: if multiple submit button, specify which one has been cliqued to the MVC controler)
				$form.append('<input type="hidden" name="save" value="save" /> ');

				$form.submit();
			}
		});
	});
</script>
@endsection