@extends('layouts.admin_layout')

@section('title', 'Import Product')

@section('content')
<?php

$controller_name = request()->segment(count(request()->segments()));
?>

    <div class="container-fluid my-3">
        <div class="row my-3">
            <div class="col-md-12">
                <div class="card r-0">
                    <div class="card-header white">
                        <strong>Import Product</strong>
                        <a href="{{ route('product.download')}}">
                            <button class="btn btn-dark btn-xs float-right">
                                <i class="icon icon-download"></i>Download Sample File
                            </button>
                        </a>
                    </div>
                    <div class="card-body">
                        <div class="row">
                            <div class="col-md-12">
                                <form method="POST" action={{ route("product.import") }} enctype="multipart/form-data">
                            @csrf
                            <div class="form-group">
                                <input id="file" name="file" type="file" class="form-control @error('file') is-invalid @enderror" required>
                                @error('file')
                                    <div class="invalid-feedback">{{$message}}</div>
                                @enderror
                            </div>
                            <button type="submit" class="btn btn-primary mt-2">Submit</button>    
                        </form>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="card r-0 mt-2">
                    <div class="card-header white">
                        <strong class="text-danger">NOTE</strong>
                    </div>
                    <div class="card-body">
                        <div class="row">
                            <div class="col-md-12">
                                <div class="alert alert-danger">
                                    <div class="my-3">
                                        * All fields from sample file are required.
                                    </div>
                                    <div class="my-3">
                                        * product_category_id must be in category list
                                    </div>
                                    <div class="my-3">
                                        * product_sub_category_id must be in subcategory category list
                                    </div>
                                    <div class="my-3">
                                        * stock_alert must be less than unit
                                    </div>
                                    <div class="my-3">
                                        * cp implies Cost price
                                    </div>
                                    <div class="my-3">
                                        * mrp implies Market retailing price
                                    </div>
                                    <div class="my-3">
                                        * value of cp must be in per unit
                                    </div>
                                    <div class="my-3">
                                        * value of mrp must be in per unit
                                    </div>
                                    <div class="my-3">
                                        * created_by is 1 as default
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection
