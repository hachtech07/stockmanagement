@extends('layouts.admin_layout')

@section('title', 'Add Customer')

@section('content')
<div class="container-fluid my-3">
	<div class="d-flex row">
		<div class="col-md-12">
			<div class="card my-3 shadow no-b r-0">
				<?php $link = route('customer') ?>
				<x-cardheader title="Add Customer" :link="$link" :icon="'list-ul'" :buttonLabel="'Customer List'" />

				<div class="card-body pt-0">
					<form method="POST" class="from-prevent-multiple-submits" action={{ route("product.update") }}>
						<div class="row">
							@csrf
							<div class="col-md-6 mb-3 form-group">
								<label for="product_name">Product name <span class="text-danger">*</span></label>
								<input type="text" class="form-control @error('product_name') is-invalid @enderror" id="customer_number" placeholder="Product Number" name="product_name" value="{{ old('product_name') }}" required>
								@error('product_name')
								<div class="invalid-feedback">{{$message}}</div>
								@enderror
							</div>
							<div class="col-md-6 mb-3 form-group">
								<label for="product_category_id">Product Category <span class="text-danger">*</span></label>
								<select class="form-control @error('product_category_id') is-invalid @enderror" name="product_category_id" required>
                                    <option value="">Select category</option>
									<option value="active">Active</option>
									<option value="inactive">Inactive</option>
								</select>
								@error('branch_id')
								<div class="invalid-feedback">{{$message}}</div>
								@enderror
							</div>
							<div class="col-md-6 mb-3 form-group">
								<label for="color">Product color <span class="text-danger">*</span></label>
								<input type="text" class="form-control @error('color') is-invalid @enderror" id="color" placeholder="product color" name="color" value="{{ old('color') }}" required>
								@error('color')
								<div class="invalid-feedback">{{$message}}</div>
								@enderror
							</div>
							<div class="col-md-6 mb-3 form-group">
								<label for="model">Model <span class="text-danger"></span></label>
								<input type="text" class="form-control @error('model') is-invalid @enderror" id="model" placeholder="product model" name="model" value="{{ old('model') }}">
								@error('model')
								<div class="invalid-feedback">{{$message}}</div>
								@enderror
							</div>
							<div class="col-md-6 mb-3 form-group">
								<label for="mrp">MRP <span class="text-danger">*</span></label>
								<input type="text" class="form-control @error('mrp') is-invalid @enderror" id="mrp" placeholder="mrp" name="mrp" value="{{ old('mrp') }}" required>
								@error('mrp')
								<div class="invalid-feedback">{{$message}}</div>
								@enderror
							</div>
							<div class="col-md-6 mb-3 form-group">
								<label for="cp">Cost Price (CP) <span class="text-danger">*</span></label>
								<input type="text" class="form-control @error('cp') is-invalid @enderror" id="address" placeholder="Cost price" name="cp" value="{{ old('cp') }}" required>
								@error('cp')
								<div class="invalid-feedback">{{$message}}</div>
								@enderror
							</div>
							<div class="col-md-6 mb-3 form-group">
								<label for="stock_alert">Stock alert <span class="text-danger">*</span></label>
								<input type="number" class="form-control @error('stock_alert') is-invalid @enderror" id="stock_alert" placeholder="Stock alert" name="stock_alert" value="{{ old('stock_alert') }}" required>
								@error('stock_alert')
								<div class="invalid-feedback">{{$message}}</div>
								@enderror
							</div>
							<div class="col-md-6 mb-3 form-group">
								<label for="entry_date">Entry Date <span class="text-danger">*</span></label>
								<input type="text" class="form-control @error('entry_date') is-invalid @enderror" id="nepali-datepicker" placeholder="Bill Upto" name="entry_date" value="{{ old('entry_date') }}" autocomplete="off" readonly>
								@error('entry_date')
								<div class="invalid-feedback">{{$message}}</div>
								@enderror
							</div>
							<div class="col-md-12 mb-3 form-group">
								<button class="btn btn-sm btn-primary px-5 from-prevent-multiple-submit" type="submit" name="saveExit">Save</button>
							</div>
						</div>
					</form>
				</div>
			</div>
		</div>
	</div>
</div>
<script type="text/javascript">
	$(document).ready(function() {
		$('#nepali-datepicker').nepaliDatePicker();
	});
</script>
<script type="text/javascript">
	$(document).ready(function() {
		$('select[name="branch_id"]').on('change', function() {
			var branchId = $(this).val();
			if (branchId) {
				$.ajax({
					url: "{{url('/dashboard/ajaxuser')}}/" + branchId,
					type: "GET",
					dataType: "json",
					success: function(data) {
						$('select[name="collector_id"]').empty();
						$('select[name="collector_id"]').append('<option value="">Select collector</option>');
						$.each(data, function(key, value) {
							$('select[name="collector_id"]').append('<option value="' + value.id + '">' + value.name + '</option>');
						});
					}
				});
			} else {
				$('select[name="collector_id"]').empty();
				$('select[name="collector_id"]').append('<option value="">' + 'Select user' + '</option>');
			}
		});
	});
</script>
<script type="text/javascript">
	$(document).ready(function() {
		$('select[name="branch_id"]').on('change', function() {
			var branchId = $(this).val();

			if (branchId) {
				$.ajax({
					url: "{{url('/customer/last-customer-id')}}/" + branchId,
					type: "GET",
					dataType: "json",
					success: function(data) {
						$('#customer_number').val(data);
					}
				});
			} else {
				$('#customer_number').val('')
			}
		});
	})
</script>
<script>
	$(document).ready(function(ev) {
		$(".from-prevent-multiple-submit").on('click', function(ev) {
			var $form = $(this).closest('form');

			if ($form.valid()) {

				$(this).prop('disabled', true);

				// (optional: if multiple submit button, specify which one has been cliqued to the MVC controler)
				$form.append('<input type="hidden" name="save" value="save" /> ');

				$form.submit();
			}
		});
	});
</script>
@endsection