@extends('layouts.admin_layout')

@section('title', 'Update Stock')

@section('content')
<div class="container-fluid my-3">
	<div class="d-flex row">
		<div class="col-md-12">
			<div class="card my-3 shadow no-b r-0">
				<?php $link = ""?>
				<x-cardheader title="Update Stock" :link="$link" :icon="'list-ul'" :buttonLabel="'Customer List'" />

				<div class="card-body pt-0">
					<form method="POST" class="from-prevent-multiple-submits" action="{{ route("product.update",$product->id) }}"">
						<div class="row">
							@csrf
                            @method('PUT')
							{{-- <div class="col-md-6 mb-3 form-group">
								<label for="product_category_id">Product Category <span class="text-danger">*</span></label>
								<select class="form-control @error('product_category_id') is-invalid @enderror" name="product_category_id" required>
									<option value="">Select Product Category</option>
									@foreach($categories as $category)
									<option value="{{ $category -> id }}" {{ (old('product_category_id') == $category ->id) ? 'selected' : ''}}>{{ $category -> name }}</option>
									@endforeach
								</select>
								@error('branch_id')
								<div class="invalid-feedback">{{$message}}</div>
								@enderror
							</div> --}}
							<div class="col-md-6 mb-3 form-group">
								<label for="unit">Quantity <span class="text-danger">*</span></label>
								<input type="text" class="form-control @error('unit') is-invalid @enderror" id="unit" placeholder="product unit" name="unit" value="{{ $product -> unit }}" required>
								@error('unit')
								<div class="invalid-feedback">{{$message}}</div>
								@enderror
							</div>
							{{-- <div class="col-md-6 mb-3 form-group">
								<label for="cp">Cost Price (CP)/unit <span class="text-danger">*</span></label>
								<input type="text" class="form-control @error('cp') is-invalid @enderror" id="address" placeholder="Cost price" name="cp" value="{{ old('cp') }}" required>
								@error('cp')
								<div class="invalid-feedback">{{$message}}</div>
								@enderror
							</div>
							<div class="col-md-6 mb-3 form-group">
								<label for="mrp">MRP/unit <span class="text-danger">*</span></label>
								<input type="text" class="form-control @error('mrp') is-invalid @enderror" id="mrp" placeholder="mrp" name="mrp" value="{{ old('mrp') }}" required>
								@error('mrp')
								<div class="invalid-feedback">{{$message}}</div>
								@enderror
							</div> --}}
							
							<div class="col-md-6 mb-3 form-group">
								<label for="stock_alert">Stock alert <span class="text-danger">*</span></label>
								<input type="number" class="form-control @error('stock_alert') is-invalid @enderror" id="stock_alert" placeholder="Stock alert" name="stock_alert" value="{{ $product -> stock_alert }}" required>
								@error('stock_alert')
								<div class="invalid-feedback">{{$message}}</div>
								@enderror
							</div>
							<div class="col-md-12 mb-3 form-group">
								<button class="btn btn-sm btn-primary px-5 from-prevent-multiple-submit" type="submit" name="saveExit">Save</button>
							</div>
						</div>
					</form>
				</div>
			</div>
		</div>
	</div>
</div>
<script type="text/javascript">
	$(document).ready(function() {
		$('#nepali-datepicker').nepaliDatePicker();
	});
</script>
<script type="text/javascript">
	$(document).ready(function() {
		$('select[name="branch_id"]').on('change', function() {
			var branchId = $(this).val();
			if (branchId) {
				$.ajax({
					url: "{{url('/dashboard/ajaxuser')}}/" + branchId,
					type: "GET",
					dataType: "json",
					success: function(data) {
						$('select[name="collector_id"]').empty();
						$('select[name="collector_id"]').append('<option value="">Select collector</option>');
						$.each(data, function(key, value) {
							$('select[name="collector_id"]').append('<option value="' + value.id + '">' + value.name + '</option>');
						});
					}
				});
			} else {
				$('select[name="collector_id"]').empty();
				$('select[name="collector_id"]').append('<option value="">' + 'Select user' + '</option>');
			}
		});
	});
</script>
<script type="text/javascript">
	$(document).ready(function() {
		$('select[name="branch_id"]').on('change', function() {
			var branchId = $(this).val();

			if (branchId) {
				$.ajax({
					url: "{{url('/customer/last-customer-id')}}/" + branchId,
					type: "GET",
					dataType: "json",
					success: function(data) {
						$('#customer_number').val(data);
					}
				});
			} else {
				$('#customer_number').val('')
			}
		});
	})
</script>
<script>
	$(document).ready(function(ev) {
		$(".from-prevent-multiple-submit").on('click', function(ev) {
			var $form = $(this).closest('form');

			if ($form.valid()) {

				$(this).prop('disabled', true);

				// (optional: if multiple submit button, specify which one has been cliqued to the MVC controler)
				$form.append('<input type="hidden" name="save" value="save" /> ');

				$form.submit();
			}
		});
	});
</script>
@endsection