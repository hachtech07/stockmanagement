@extends('layouts.admin_layout')

@section('title', 'Update Password')

@section('content')
	
    <div class="container-fluid my-3">
        <div class="d-flex row">
            <div class="col-md-12">
	            <div class="card my-3 shadow no-b r-0">
	                <div class="card-header white">
	                    <h6>Update Password</h6>
	                </div>
	                <div class="card-body">
	                    <form method="POST" action={{ route("user.update.pass",$user->id) }}>
	                		@csrf
	                        <div class="col-md-6 mb-3 form-group">
	                            <label for="address">New Password <span class="text-danger">*</span></label>
	                            <input type="password" class="form-control @error('password') is-invalid @enderror" id="password" placeholder="Password" name="password" spellcheck="false" autocorrect="off"  autocapitalize="off" required>
	                            @error('password')
	                            	<div class="invalid-feedback">{{$message}}</div>
	                            @enderror
	                        </div>
	                        <div class="col-md-6 mb-3 form-group">
	                            <label for="address">Confirm Password <span class="text-danger">*</span></label>
	                            <input type="password" class="form-control @error('confirm_password') is-invalid @enderror" id="confirm password" placeholder="Confirm Password" name="confirm_password" spellcheck="false" autocorrect="off"  autocapitalize="off" required>
	                            @error('confirm_password')
	                            	<div class="invalid-feedback">{{$message}}</div>
	                            @enderror
	                        </div>

	                        <div class="col-md-4 mb-3 form-group">
	                        	<button class="btn btn-sm btn-primary" type="submit">Update</button>
	                        </div>
	                    </form>
	                </div>
	            </div>
            </div>
        </div>
    </div>

@endsection

<script>
	const passwordEle = document.getElementById('password');
	const toggleEle = document.getElementById('toggle');

	toggleEle.addEventListener('click', function() {
	    const type = passwordEle.getAttribute('type');
	    
	    passwordEle.setAttribute(
	        'type',
	        // Switch it to a text field if it's a password field
	        // currently, and vice versa
	        type === 'password' ? 'text' : 'password'
	    );
	});
</script>