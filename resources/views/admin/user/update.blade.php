@extends('layouts.admin_layout')

@section('title', 'Update User')

@section('content')

<div class="container-fluid my-3">
	<div class="d-flex row">
		<div class="col-md-12">
			<div class="card my-3 shadow no-b r-0">
				<?php $link = route('users')?>
                <x-cardheader title="Update User" :link="$link" :icon="'list-ul'" :buttonLabel="'User List'" />
				<div class="card-body pt-0">
					<form method="POST" action={{ route("users.update",$user->id) }}>
						<div class="row">
							@csrf
                            @method('PUT')
							<div class="col-md-6 mb-3 form-group">
								<label for="first_name">First Name <span class="text-danger">*</span></label>
								<input type="text" class="form-control @error('first_name') is-invalid @enderror" id="first_name" placeholder="First name" name="first_name" value="{{ $user -> first_name }}" required>
								@error('first_name')
								<div class="invalid-feedback">{{$message}}</div>
								@enderror
							</div>
                            <div class="col-md-6 mb-3 form-group">
								<label for="last_name">Last Name <span class="text-danger">*</span></label>
								<input type="text" class="form-control @error('last_name') is-invalid @enderror" id="last_name" placeholder="First name" name="last_name" value="{{ $user -> last_name }}" required>
								@error('last_name')
								<div class="invalid-feedback">{{$message}}</div>
								@enderror
							</div>
							<div class="col-md-6 mb-3 form-group">
								<label for="username">Username <span class="text-danger">*</span></label>
								<input type="text" class="form-control @error('username') is-invalid @enderror" id="username" placeholder="Username" name="username" value="{{ $user -> username }}" required>
								@error('username')
								<div class="invalid-feedback">{{$message}}</div>
								@enderror
							</div>
							<div class="col-md-6 mb-3 form-group">
								<label for="status">Status <span class="text-danger">*</span></label>
								<select class="form-control  @error('status') is-invalid @enderror" name="status" required>
									<option value="active" <?= ($user->status == "active") ? 'selected' : '' ?>>Active</option>
									<option value="inactive" <?= ($user->status == "inactive") ? 'selected' : '' ?>>Inactive</option>
								</select>
								@error('status')
								<div class="invalid-feedback">{{$message}}</div>
								@enderror
							</div>
							<div class="col-md-6 mb-3 form-group">
								<button class="btn btn-sm btn-primary px-5" type="submit">Save</button>
							</div>
						</div>
					</form>
				</div>
			</div>
		</div>
	</div>
</div>

@endsection

<script>
	const passwordEle = document.getElementById('password');
	const toggleEle = document.getElementById('toggle');

	toggleEle.addEventListener('click', function() {
		const type = passwordEle.getAttribute('type');

		passwordEle.setAttribute(
			'type',
			// Switch it to a text field if it's a password field
			// currently, and vice versa
			type === 'password' ? 'text' : 'password'
		);
	});
</script>