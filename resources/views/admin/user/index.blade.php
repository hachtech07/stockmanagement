@extends('layouts.admin_layout')

@section('title', 'Users List')

@section('content')
<?php

$controller_name = request()->segment(count(request()->segments()));
?>

<div class="container-fluid my-3">
    <div class="row my-3">
        <div class="col-md-12">
            <div class="card r-0">
                <?php $link = route('users.create') ?>
                <x-cardheader title="User List" :link="$link" :icon="'list-ul'" :buttonLabel="'Create User'" />
                <div class="table-responsive">
                    <table class="table table-striped table-hover r-0 data-table">
                        <thead>
                            <tr class="no-b">
                                <th>SN</th>
                                <th>FIRST NAME</th>
                                <th>LAST NAME</th>
                                <th>USERNAME</th>
                                <th>STATUS</th>
                                <th>ACTION</th>
                            </tr>
                        </thead>
                        <tbody>

                            @foreach($users as $user)
                            <tr>
                                <td>{{$loop->iteration}}</td>
                                <td>{{$user->first_name}}</td>
                                <td>{{$user->last_name}}</td>
                                <td>{{$user->username}}</td>
                                <td><span class="icon icon-circle s-12  mr-2 <?= ($user->status == \App\Models\Branch::ACTIVE) ? 'text-success' : 'text-warning' ?>"></span>{{$user->status}}</std>
                                <td>
                                    <a href="{{ route('users.update', $user->id)}}" class="btn btn-success btn-sm">Edit</a>
                                    <a href="{{ route('user.update.pass', $user->id)}}" class="btn btn-success btn-sm">Change Password</a>
                                </td>
                            </tr>
                            @endforeach

                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection