<!DOCTYPE html>
<html lang="zxx">
<x-head />

<body class="light loaded">
    <div id="loader" class="loader">
        <div class="plane-container">
            <div class="preloader-wrapper small active">
                <div class="spinner-layer spinner-blue">
                    <div class="circle-clipper left">
                        <div class="circle"></div>
                    </div>
                    <div class="gap-patch">
                        <div class="circle"></div>
                    </div>
                    <div class="circle-clipper right">
                        <div class="circle"></div>
                    </div>
                </div>

                <div class="spinner-layer spinner-red">
                    <div class="circle-clipper left">
                        <div class="circle"></div>
                    </div>
                    <div class="gap-patch">
                        <div class="circle"></div>
                    </div>
                    <div class="circle-clipper right">
                        <div class="circle"></div>
                    </div>
                </div>

                <div class="spinner-layer spinner-yellow">
                    <div class="circle-clipper left">
                        <div class="circle"></div>
                    </div>
                    <div class="gap-patch">
                        <div class="circle"></div>
                    </div>
                    <div class="circle-clipper right">
                        <div class="circle"></div>
                    </div>
                </div>

                <div class="spinner-layer spinner-green">
                    <div class="circle-clipper left">
                        <div class="circle"></div>
                    </div>
                    <div class="gap-patch">
                        <div class="circle"></div>
                    </div>
                    <div class="circle-clipper right">
                        <div class="circle"></div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div id="app">
        <script>
            /*
             *  Add sidebar classes (sidebar-mini sidebar-collapse sidebar-expanded-on-hover) in body tag
             *  you can remove this script tag and add classes directly to body
             *  this is only for demo
             */
            document.body.className += ' sidebar-mini' + ' sidebar-collapse' + ' sidebar-expanded-on-hover' + ' sidebar-top-offset';
        </script>

    </div>
    <div class="page has-sidebar-left height-full">
        <x-toast />
        <div class="container-fluid my-3">
            <br>
            <br>
            <x-sidebar />

            <div class="sticky">

                <x-navbar />

            </div>

            @yield('content')
        </div>

    </div>

    <!-- <x-right-sidebar /> -->

    <!-- Add the sidebar's background. This div must be placed
             immediately after the control sidebar -->
    <div class="control-sidebar-bg shadow white fixed"></div>

    <!--/#app -->
    <script src="{{ url('assets/js/app.js') }}"></script>
    <script>
        $('.popover-dismiss').popover({
            trigger: 'focus'
        });
    </script>

    <!--
        --- Footer Part - Use Jquery anywhere at page.
        --- http://writing.colin-gourlay.com/safely-using-ready-before-including-jquery/
        -->
    <script>
        (function($, d) {
            $.each(readyQ, function(i, f) {
                $(f)
            });
            $.each(bindReadyQ, function(i, f) {
                $(d).bind("ready", f)
            })
        })(jQuery, document)
    </script>

    <!-- include js file if exists based on controller -->

    <?php $routeName = app('request')->route()->getAction(); ?>
    <?php if (isset($routeName['as'])) : ?>
        <?php $routeExplode  = explode('.', $routeName['as']) ?>
        <?php $fileName = $routeExplode[0]; ?>
        <?php if (file_exists(public_path() . '/assets/js/' . $fileName . '.js')) : ?>
            <script src="{{ asset('/assets/js/'.$fileName.'.js')}}"></script>
        <?php endif; ?>
    <?php endif; ?>

    <!-- js file include end-->
    <script src="{{ asset('/assets/js/due.js')}}"></script>
    <!-- <script type="text/javascript" src="{{ asset('assets/datepicker/nepali.datepicker.v3.6.min.js') }}"></script> -->
    <script src="https://cdn.datatables.net/1.11.3/js/jquery.dataTables.min.js"></script>
    <script src="{{ asset('assets/js/chart.min.js') }}"></script>
    <link href="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/css/select2.min.css" rel="stylesheet" />
    <script src="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/js/select2.min.js"></script>
    <script src="https://cdn.datatables.net/buttons/2.0.1/js/dataTables.buttons.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.53/pdfmake.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.53/vfs_fonts.js"></script>
    <script src="https://cdn.datatables.net/buttons/2.0.1/js/buttons.html5.min.js"></script>
    <script src="{{asset('assets/js/datatable-serverside-render.js')}}"></script>
</body>

</html>