@extends('layouts.login_layout')

@section('content')

<main class="login-bg">
    <div id="primary" class="p-t-b-100 height-full ">
        <div class="">
            <div class="row">
                <div class="col-lg-4 mx-md-auto">
                    <div class="text-center">
                        <!-- <img src="assets/img/dummy/u5.png" alt=""> -->
                        <h3 class="mt-2 mb-4 font-weight-bolder text-white">{{ __('Login') }}</h3>
                        <!-- <p class="p-t-b-20">Hey Soldier welcome back signin now there is lot of new stuff waiting for you</p> -->
                    </div>
                    <form method="POST" action="{{ route('login') }}" class="login-form">
                        @csrf

                        <div class="row">
                            <label for="username" class=" col-md-8 col-form-label">{{ __('Username') }}</label>

                            <div class=" col-md-12 form-group has-icon">
                                <i class="icon-envelope-o"></i>
                                <input id="username" type="username" class="form-control form-control-lg @error('username') is-invalid @enderror" name="username" value="{{ old('username') }}" required autocomplete="username" autofocus>

                                @error('username')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>

                        <div class="row">
                            <label for="password" class=" col-md-12 col-form-label">{{ __('Password') }}</label>

                            <div class=" col-md-12 form-group has-icon">
                                <i class="icon-user-secret"></i>
                                <input id="password" type="password" class="form-control form-control-lg @error('password') is-invalid @enderror" name="password" required autocomplete="current-password">

                                @error('password')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-md-12  form-group">
                                <div class="form-check d-flex justify-content-between">
                                    
                                    <label class="form-check-label" for="remember">
                                        <input class="form-check-input" type="checkbox" name="remember" id="remember" {{ old('remember') ? 'checked' : '' }}>
                                        {{ __('Remember Me') }}
                                    </label>
                                        <a class="btn btn-link small pr-0" href="{{ route('forgot') }}">
                                            {{ __('Forgot Your Password?') }}
                                        </a>
                                </div>
                            </div>
                        </div>

                        <div class="form-group row mb-0">
                            <div class="col-md-12 ">
                                <button type="submit" class="btn btn-primary btn-block">
                                    {{ __('Login') }}
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
    <!-- #primary -->
</main>

@endsection
