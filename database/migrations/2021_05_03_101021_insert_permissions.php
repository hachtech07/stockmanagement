<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class InsertPermissions extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        \DB::table('permissions')->insert(['id' => '1', 'name' => 'create branch', 'guard_name' => 'web', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')]);
        \DB::table('permissions')->insert(['id' => '2', 'name' => 'view branch', 'guard_name' => 'web', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')]);
        \DB::table('permissions')->insert(['id' => '3', 'name' => 'edit branch', 'guard_name' => 'web', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')]);
        \DB::table('permissions')->insert(['id' => '4', 'name' => 'list branch', 'guard_name' => 'web', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')]);
        \DB::table('permissions')->insert(['id' => '5', 'name' => 'create customer', 'guard_name' => 'web', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')]);
        \DB::table('permissions')->insert(['id' => '6', 'name' => 'view customer', 'guard_name' => 'web', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')]);
        \DB::table('permissions')->insert(['id' => '7', 'name' => 'edit customer', 'guard_name' => 'web', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')]);
        \DB::table('permissions')->insert(['id' => '8', 'name' => 'list customer', 'guard_name' => 'web', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')]);
        \DB::table('permissions')->insert(['id' => '9', 'name' => 'create customer category', 'guard_name' => 'web', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')]);
        \DB::table('permissions')->insert(['id' => '10', 'name' => 'view customer category', 'guard_name' => 'web', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')]);
        \DB::table('permissions')->insert(['id' => '11', 'name' => 'edit customer category', 'guard_name' => 'web', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')]);
        \DB::table('permissions')->insert(['id' => '12', 'name' => 'list customer category', 'guard_name' => 'web', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')]);
        \DB::table('permissions')->insert(['id' => '13', 'name' => 'create user', 'guard_name' => 'web', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')]);
        \DB::table('permissions')->insert(['id' => '14', 'name' => 'edit user', 'guard_name' => 'web', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')]);
        \DB::table('permissions')->insert(['id' => '15', 'name' => 'list user', 'guard_name' => 'web', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')]);
        \DB::table('permissions')->insert(['id' => '16', 'name' => 'list role', 'guard_name' => 'web', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')]);
        \DB::table('permissions')->insert(['id' => '17', 'name' => 'create role', 'guard_name' => 'web', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')]);
        \DB::table('permissions')->insert(['id' => '18', 'name' => 'update role', 'guard_name' => 'web', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')]);
        \DB::table('permissions')->insert(['id' => '19', 'name' => 'add permission to role', 'guard_name' => 'web', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')]);
        \DB::table('permissions')->insert(['id' => '20', 'name' => 'list billing category', 'guard_name' => 'web', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')]);
        \DB::table('permissions')->insert(['id' => '21', 'name' => 'create billing category', 'guard_name' => 'web', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')]);
        \DB::table('permissions')->insert(['id' => '22', 'name' => 'update billing category', 'guard_name' => 'web', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')]);
        \DB::table('permissions')->insert(['id' => '23', 'name' => 'list billing', 'guard_name' => 'web', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')]);
        \DB::table('permissions')->insert(['id' => '24', 'name' => 'create billing', 'guard_name' => 'web', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')]);

        \DB::table('model_has_permissions')->insert(['permission_id' => '1', 'model_type' => 'App\Models\User', 'model_id' => '1']);
        \DB::table('model_has_permissions')->insert(['permission_id' => '2', 'model_type' => 'App\Models\User', 'model_id' => '1']);
        \DB::table('model_has_permissions')->insert(['permission_id' => '3', 'model_type' => 'App\Models\User', 'model_id' => '1']);
        \DB::table('model_has_permissions')->insert(['permission_id' => '4', 'model_type' => 'App\Models\User', 'model_id' => '1']);
        \DB::table('model_has_permissions')->insert(['permission_id' => '5', 'model_type' => 'App\Models\User', 'model_id' => '1']);
        \DB::table('model_has_permissions')->insert(['permission_id' => '6', 'model_type' => 'App\Models\User', 'model_id' => '1']);
        \DB::table('model_has_permissions')->insert(['permission_id' => '7', 'model_type' => 'App\Models\User', 'model_id' => '1']);
        \DB::table('model_has_permissions')->insert(['permission_id' => '8', 'model_type' => 'App\Models\User', 'model_id' => '1']);
        \DB::table('model_has_permissions')->insert(['permission_id' => '9', 'model_type' => 'App\Models\User', 'model_id' => '1']);
        \DB::table('model_has_permissions')->insert(['permission_id' => '10', 'model_type' => 'App\Models\User', 'model_id' => '1']);
        \DB::table('model_has_permissions')->insert(['permission_id' => '11', 'model_type' => 'App\Models\User', 'model_id' => '1']);
        \DB::table('model_has_permissions')->insert(['permission_id' => '12', 'model_type' => 'App\Models\User', 'model_id' => '1']);
        \DB::table('model_has_permissions')->insert(['permission_id' => '13', 'model_type' => 'App\Models\User', 'model_id' => '1']);
        \DB::table('model_has_permissions')->insert(['permission_id' => '14', 'model_type' => 'App\Models\User', 'model_id' => '1']);
        \DB::table('model_has_permissions')->insert(['permission_id' => '15', 'model_type' => 'App\Models\User', 'model_id' => '1']);
        \DB::table('model_has_permissions')->insert(['permission_id' => '16', 'model_type' => 'App\Models\User', 'model_id' => '1']);
        \DB::table('model_has_permissions')->insert(['permission_id' => '17', 'model_type' => 'App\Models\User', 'model_id' => '1']);
        \DB::table('model_has_permissions')->insert(['permission_id' => '18', 'model_type' => 'App\Models\User', 'model_id' => '1']);
        \DB::table('model_has_permissions')->insert(['permission_id' => '19', 'model_type' => 'App\Models\User', 'model_id' => '1']);
        \DB::table('model_has_permissions')->insert(['permission_id' => '20', 'model_type' => 'App\Models\User', 'model_id' => '1']);
        \DB::table('model_has_permissions')->insert(['permission_id' => '21', 'model_type' => 'App\Models\User', 'model_id' => '1']);
        \DB::table('model_has_permissions')->insert(['permission_id' => '22', 'model_type' => 'App\Models\User', 'model_id' => '1']);
        \DB::table('model_has_permissions')->insert(['permission_id' => '23', 'model_type' => 'App\Models\User', 'model_id' => '1']);
        \DB::table('model_has_permissions')->insert(['permission_id' => '24', 'model_type' => 'App\Models\User', 'model_id' => '1']);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
