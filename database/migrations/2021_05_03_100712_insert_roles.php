<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class InsertRoles extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        \DB::table('roles')->insert(['id' => '1', 'name' => 'super admin', 'guard_name' => 'web', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')]);
        \DB::table('roles')->insert(['id' => '2', 'name' => 'branch admin', 'guard_name' => 'web', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')]);
        \DB::table('roles')->insert(['id' => '3', 'name' => 'collector', 'guard_name' => 'web', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')]);

        \DB::table('model_has_roles')->insert(['role_id' => '1', 'model_type' => 'App\Models\User', 'model_id' => '1']);
    }

    /**
     * Reverse the migrations.
     *
     * @return vpopmail_del_domain(domain)
     */
    public function down()
    {
        //
    }
}
