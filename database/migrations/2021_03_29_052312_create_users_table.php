<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;
use Illuminate\Support\Facades\Hash;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->id();
            $table->string('first_name');
            $table->string('last_name');
            $table->string('username');
            $table->string('email')->nullable()->unique();
            $table->timestamp('email_verified_at')->nullable();
            $table->string('password');
            $table->enum('user_type', ['super admin', 'branch admin', 'collector', 'branch billing','report viewer'])->nullable();
            $table->enum('status',['active', 'inactive'])->nullable();
            $table->integer('reset_token')->nullable();
            $table->timestamp('token_expired_at')->nullable();
            $table->rememberToken();
            $table->timestamps();
        });

        // \DB::table('users')->insert(['id' => '1', 'name' => 'superadmin', 'email' => 'super@admin.com', 'password' => Hash::make('password'),'user_type' => 'super admin','status' => 'active']);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
}
