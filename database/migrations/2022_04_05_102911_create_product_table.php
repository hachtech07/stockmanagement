<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;
use Illuminate\Support\Facades\Hash;

class CreateProductTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('products', function (Blueprint $table) {
            $table->id();
            $table->string('product_name');
            $table->foreignId('product_category_id')->references('id')->on('product_categories')->onDelete('cascade');
            $table->string('color');
            $table->string('model')->nullable();
            $table->float('mrp',8,2);
            $table->float('cp',8,2);
            $table->integer('stock_alert');
            $table->string('entry_date');
            $table->foreignId('fiscal_year_id')->references('id')->on('fiscal_year')->onDelete('cascade');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('products');
    }
}
