<?php

namespace App\Console\Commands;

use App\Models\Billing;
use App\Models\Customer;
use App\Traits\FiscalYear;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\Http;

class BillSync extends Command
{
    use FiscalYear;
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'sync:bill';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'This command is used to sync local data with external source';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
            $bill = Billing::where('ird_status','unsynced')->get();
            if($bill->count() ===0){
                return 1;
            }
            $convertedDate = $this->current_date_to_nepali() . ' ' . date('H:i:s');

            foreach($bill as $row){
                if($row->id && ($row->net_amount > 0 ) && ($row->created_date_time !=null)){
                $invoice = date_format($row->created_at, 'Y-m-d');
                $convert = $this->convert_date_to_nepali($invoice);
                $formattedInvoice = explode('-',$convert);
                $customer = Customer::find($row->customer_id);

                $response = Http::post('http://43.245.85.152:9050/api/bill', [
                    'username'          => env('IRD_USERNAME'),
                    'password'          => env('IRD_PASSWORD'),
                    'seller_pan'        => env('COMPANY_PAN_VAT'),
                    'buyer_pan'         => $customer->vat_pan_number != Null ? $customer->vat_pan_number : 0,
                    'buyer_name'        => $customer->name,
                    'fiscal_year'       => $this->formattedFiscalDate($convertedDate),
                    'invoice_number'    => $row->id,
                    'invoice_date'      => $formattedInvoice[0].'.'.$formattedInvoice[1].'.'.$formattedInvoice[2],
                    'total_sales'       => $row->net_amount,
                    'taxable_sales_vat' => $row->gross_amount - $row->discount_amount != null? $row->discount_amount :0,
                    'vat'               => 0.13 * $row->gross_amount - $row->discount_amount != null? $row->discount_amount :0,
                    'excisable_amount'  => 0,
                    'excise'            => 0,
                    'taxable_sales_hst' => 0,
                    'hst'               => 0,
                    'amount_for_esf'    => 0,
                    'esf'               => 0,
                    'export_sales'      => 0,
                    'tax_exempted_sales'=> 0,
                    'isrealtime'        => false,
                    'datetimeclient'    => date("Y-m-d h:i:s")
                ]);
                \DB::table('ird_response_log')->insert([
                    'billing_id'    => $row->id,
                    'response'      => $response,
                    'created_at'    => date("Y-m-d h:i:s")
                ]);

                $bills = Billing::find($row->id);
                if($response == '200'){
                    $bills->ird_status = 'synced';
                    $bills->save();
                }
            
            return true;
                }
            }
    }
}
