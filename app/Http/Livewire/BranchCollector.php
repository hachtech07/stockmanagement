<?php

namespace App\Http\Livewire;

use App\Models\User;
use App\Models\Branch;
use Livewire\Component;

class BranchCollector extends Component
{
    public $branch;
    public $collectors = [];
    public $collector;

    public function mount($branch, $collector)
    {
        $this->branch = $branch;
        $this->collector = $collector;
    }


    public function render()
    {
        if (!empty($this->branch)) {
            echo $this->branch;
            exit;
            $this->collectors = User::where('branch_id', $this->branch)->where('user_type', 'collector')->get();
        }
        return view('livewire.branch-collector')
            ->withBranches(Branch::select('id', 'branch_title')->get());
    }
}
