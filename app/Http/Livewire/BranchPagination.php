<?php
      
namespace App\Http\Livewire;
  
use Livewire\Component;
use Livewire\WithPagination;
use App\Models\Branch;
  
class BranchPagination extends Component
{
    use WithPagination;
    protected $paginationTheme = 'bootstrap';

    public $search = '';

    public function updatingSearch()
    {
        $this->resetPage();
    }
  
    /**
     * Write code on Method
     *
     * @return response()
     */
    public function render()
    {
        return view('livewire.branch-pagination', [
            'branches' => Branch::where('branch_title', 'like', '%'.$this->search.'%')->where('id', '>', 1)->paginate(10),
        ]);
    }
}