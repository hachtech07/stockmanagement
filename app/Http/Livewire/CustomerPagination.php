<?php
      
namespace App\Http\Livewire;
  
use Livewire\Component;
use Livewire\WithPagination;
use App\Models\Customer;
  
class CustomerPagination extends Component
{
    use WithPagination;
    protected $paginationTheme = 'bootstrap';

    public $search = '';

    public function updatingSearch()
    {
        $this->resetPage();
    }
  
    /**
     * Write code on Method
     *
     * @return response()
     */
    public function render()
    {
        if(\Auth::id() == 1){
            $customers = \App\Models\Customer::where(function($query){
                $query -> where('name', 'LIKE', '%'.$this->search.'%')
                       -> orWhere('customer_number', 'LIKE', '%'.$this->search.'%')
                       -> orWhere('phone_number', 'LIKE', '%'.$this->search.'%')    
                       -> orWhere('card_number', 'LIKE', '%'.$this->search.'%');   
            })->paginate(1);
        }else {
            $branch_id = \App\Models\User::getUserBranch();
            // $customers = Customer::where('customer_number', 'like', '%'.$this->search.'%')->where('branch_id', $branch_id)->paginate(10);
            $customers = \App\Models\Customer::where('branch_id', $branch_id) -> where(function($query){
                $query -> where('name', 'LIKE', '%'.$this->search.'%')
                       -> orWhere('customer_number', 'LIKE', '%'.$this->search.'%')
                       -> orWhere('phone_number', 'LIKE', '%'.$this->search.'%')
                       -> orWhere('card_number', 'LIKE', '%'.$this->search.'%');   
            })->paginate(1);
        }

        $branch_id = \App\Models\User::getUserBranch();
        return view('livewire.customer-pagination', [
            'customers' => $customers,
        ]);
    }
}