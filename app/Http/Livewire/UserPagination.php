<?php
      
namespace App\Http\Livewire;
  
use Livewire\Component;
use Livewire\WithPagination;
use App\Models\User;
  
class UserPagination extends Component
{
    use WithPagination;
    protected $paginationTheme = 'bootstrap';

    public $search = '';

    public function updatingSearch()
    {
        $this->resetPage();
    }
  
    /**
     * Write code on Method
     *
     * @return response()
     */
    public function render()
    {
        if(\Auth::id() == 1) {
            $users = User::where('name', 'like', '%'.$this->search.'%')->where('id', '>', 1)->where('id', '!=', \Auth::id())->paginate(10);
        }else {
            $userDetail = User::find(\Auth::id());
            $users = User::where('name', 'like', '%'.$this->search.'%')->where('id', '>', 1)->where('branch_id', $userDetail->branch_id)->where('id', '!=', \Auth::id())->paginate(10);
        }
        return view('livewire.user-pagination', [
            'users' => $users,
        ]);
    }
}