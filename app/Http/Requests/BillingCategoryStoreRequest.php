<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class BillingCategoryStoreRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        switch ($this -> method()) {
            case 'GET':
                return [];
                break;
            default:
                return [
                    'title'             => 'required|unique:billing_categories,title,'.$this->id,
                    'default_price'     => 'required|integer|min:1',
                    'monthly_charge'    => 'required|in:yes,no',
                    'status'            => 'required|in:active,inactive',
                ];
                break;
        }
        
    }

     /**
     * Custom message for validation
     *
     * @return array
     */
    public function messages()
    {
        return [
            'monthly_charge' => 'Monthly chargeable field is required.',
        ];
    }
}
