<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class ChangeUserPassword extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        switch ($this -> method()) {
            case 'GET':
                return [];
                break;
            default:
                return [
                    'password'   => 'required',
                    'confirm_password'   => 'required|same:password'
                ];
                break;
                
    }
}

    /**
     * Custom message for validation
     *
     * @return array
     */
    public function messages()
    {
        return [
            'password' => 'Password field is required.',
            'confirm_password'   => 'Confirm password field is required'
        ];
    }
}
