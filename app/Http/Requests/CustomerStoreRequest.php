<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class CustomerStoreRequest extends  FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        switch ($this->method()) {
            case 'GET':
                return [];
                break;
            default:
                return [
                    'customer_number'   => 'required|numeric',
                    'name'              => 'required',
                    'address'           => 'required',
                    'phone_number'      => 'required|numeric|digits:10',
                    'municipality'      => 'required',
                    'ward'              => 'required|integer',
                    'house_number'      => 'required',
                    'branch_id'         => 'required|integer',
                    'customer_category_id' => 'required|integer',
                    'monthly_payable'   => 'required|numeric',
                    'last_bill_date'    => 'required|date_format:Y-m-d',
                    'status'            => 'required',
                    'vat_pan_number'    => 'nullable|numeric|digits:9|unique:customers,vat_pan_number,' . $this->id
                ];
                break;
        }
    }

    /**
     * Custom message for validation
     *
     * @return array
     */
    public function messages()
    {
        return [
            'customer_category_id'  => 'Customer category field is required.',
            'branch_id'             => 'Branch field is required.',
        ];
    }
}
