<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class ProductRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        switch ($this -> method()) {
            case 'GET':
                return [];
                break;
            case 'PUT':
                return [
                    'unit'               => 'required',
                    'stock_alert'        => 'required',
                 ];
                 break;
            default:
                return [
                    'product_name' => 'required',
                    'product_category_id' => 'required',
                    'product_sub_category_id'=>'required',
                    'color'               =>'required',
                    'model'               =>'required',
                    'mrp'                 =>'required|numeric',
                    'cp'                  =>'required|numeric',
                    'stock_alert'         =>'required|numeric',
                    'entry_date'          =>'nullable',
                    'fiscal_year_id'      =>'nullable',
                    'created_by'          =>'nullable',
                    'unit'                =>'required',
                    'batch_no'  => 'required|unique:products,batch_no,'.$this->id,
                ];
                break;
        }
        
    }

     /**
     * Custom message for validation
     *
     * @return array
     */
    public function messages()
    {
        return [];
    }
}
