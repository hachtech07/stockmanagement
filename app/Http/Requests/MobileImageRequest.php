<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class MobileImageRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        switch ($this -> method()) {
            case 'GET':
                return [];
                break;
            default:
                return [
                    'image_name' => 'required|string',
                    'image'      => 'required|image|mimes:jpeg,png,jpg,gif|max:2048',
                    'status'     => 'required',
                ];
                break;
        }
        
    }

     /**
     * Custom message for validation
     *
     * @return array
     */
    public function messages()
    {
        return [];
    }
}
