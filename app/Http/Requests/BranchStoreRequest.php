<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class BranchStoreRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        switch ($this -> method()) {
            case 'GET':
                return [];
                break;
            default:
                return [
                    'branch_code'  => 'required|unique:branches,branch_code,'.$this->id,
                    'branch_title'  => 'required|unique:branches,branch_title,'.$this->id,
                    'address'       => 'required',
                    'status'        => 'required',
                ];
                break;
        }
        
    }

     /**
     * Custom message for validation
     *
     * @return array
     */
    public function messages()
    {
        return[
            'branch_code.required'  =>  'Branch Code is required',
            'branch_title.required' => 'Branch Title is required!',
            'address.required'      => 'Address is required!',
            'status.required'       => 'Status is required!',
        ];
    }
}
