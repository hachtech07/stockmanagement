<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class UserStoreRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        switch ($this->method()) {
            case 'GET':
                return [];
                break;
            case 'PUT':
                return [
                    'first_name' => 'required',
                    'last_name'   => 'required',
                    'username'    => 'required|unique:users,username,'.$this->id,
                    'status'      => 'required'
                ];
            default:
                return [
                    'first_name' => 'required',
                    'last_name'   => 'required',
                    'username'    => 'required|unique:users,username,'.$this->id,
                    'password'   => 'required',
                    'confirm_password'   => 'required|same:password',
                    'status'             => 'required'
                ];
                break;
        }
    }

    /**
     * Custom message for validation
     *
     * @return array
     */
    public function messages()
    {
        return [
            'username' => 'username field is required.'
        ];
    }
}
