<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class ProductSubCategoryRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        switch ($this -> method()) {
            case 'GET':
                return [];
                break;
            case 'PUT':
                return [
                    'name'  => 'required|unique:product_categories,name,'.$this->id,
                     'status'        => 'required',
                 ];
                 break;
            default:
                return [
                   'category_id'  => 'required|unique:product_categories,name,'.$this->id,
                //    'parent_id'=>'nullable',
                   'subcategory_name'=>'required',
                    'subcategory_status'        => 'required',
                ];
                break;
        }
        
    }

     /**
     * Custom message for validation
     *
     * @return array
     */
    public function messages()
    {
        return [];
    }
}
