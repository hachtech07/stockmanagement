<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class SalesRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        switch ($this -> method()) {
            case 'GET':
                return [];
                break;
            case 'PUT':
                return [];
                 break;
            default:
                return [
                    'product_name'        =>'required',
                    'product_id'          =>'nullable',
                    'product_category'    =>'nullable',
                    'product_sub_category'=>'nullable',
                    'model'               =>'nullable',
                    'color'               =>'nullable',
                    'mrp'                 =>'required|numeric',
                    'fiscal_year_id'      =>'nullable',
                    'quantity'            =>'required',
                    'batch_no'            =>'nullable',
                    'sold_by'             =>'nullable'
                ];
                break;
        }
        
    }

     /**
     * Custom message for validation
     *
     * @return array
     */
    public function messages()
    {
        return [];
    }
}
