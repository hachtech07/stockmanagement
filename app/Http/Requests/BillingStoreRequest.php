<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class BillingStoreRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        switch ($this -> method()) {
            case 'GET':
                return [];
                break;
            default:
                return [
                    'customer'          => 'required',
                    'from_year'         => 'required|integer|min:2077|max:9999',
                    'from_month'        => 'required|integer|min:1|max:12',
                    'to_year'           => 'required|integer|min:2077|max:9999',
                    'to_month'          => 'required|integer|min:1|max:12',
                    'discount_amount'   => 'numeric',
                    'gross_amount'      => 'required|numeric',
                    'net_amount'        => 'required|numeric',
                    'tender_amount'     => 'required|numeric'
                ];
                break;
        }
        
    }

     /**
     * Custom message for validation
     *
     * @return array
     */
    public function messages()
    {
        return [];
    }
}
