<?php

namespace App\Http\Middleware;
use Illuminate\Support\Facades\Auth;
use Closure;

class UserStatusMiddleware{
    public function __construct()
    {
    }

    public function handle($request ,  Closure $next){
        if(Auth::check()){
            $user = Auth::user();
            if($user->status == 'inactive'){
                auth()->logout();
                return back()->with('error', 'User account is disabled!');
            }
        }
        return $next($request);
    }
}