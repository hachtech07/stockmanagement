<?php

namespace App\Http\Middleware;
use Illuminate\Support\Facades\Auth;
use Closure;
use Spatie\Permission\Models\Role;

class RolePermisionMiddleware{
    public function __construct()
    {
    }

    public function handle($request,Closure $next,$permission){
        if(auth()->user()->user_type =='branch admin'){
            $role = Role::findByName('branch admin');
        if($role->hasPermissionTo($permission)){
            return $next($request);
        }
    }else{
        return $next($request);
    }
        abort(403);
        
    }
}