<?php

namespace App\Http\Middleware;

use Closure;
use JWTAuth;
use Exception;
use Tymon\JWTAuth\Http\Middleware\BaseMiddleware;
use Symfony\Component\HttpFoundation\Response;
use App\Traits\RespondTrait;

class JwtMiddleware extends BaseMiddleware
{
    use RespondTrait;
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        try {
            $user = JWTAuth::parseToken()->authenticate();
        } catch (Exception $e) {
            if ($e instanceof \Tymon\JWTAuth\Exceptions\TokenInvalidException){
                return $this->errorResponse('Token is Invalid',Response::HTTP_UNAUTHORIZED);
            }else if ($e instanceof \Tymon\JWTAuth\Exceptions\TokenExpiredException){
                return $this->errorResponse('Token is Expired',Response::HTTP_UNAUTHORIZED);
                // return response()->json(['status' => 'Token is Expired']);
            }else{
                return $this->errorResponse('Authorization Token not found',Response::HTTP_UNAUTHORIZED);
                // return response()->json(['status' => 'Authorization Token not found']);
            }
        }
        return $next($request);
    }
}