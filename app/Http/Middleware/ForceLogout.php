<?php

namespace App\Http\Middleware;
use JWTAuth;
use App\Traits\RespondTrait;
use Illuminate\Support\Facades\Auth;
use Closure;
use Symfony\Component\HttpFoundation\Response;

class ForceLogout{
    use RespondTrait;
    public function __construct()
    {
    }

    public function handle($request ,  Closure $next){
        if(Auth::check()){
            $user = Auth::user();
            if($user->password_reset_at > $user->logged_in_at){
                $mess = explode('.',$user->password_reset_at)[0];
                JWTAuth::invalidate($request->token);
                return $this->errorResponse([
                    'message' => "User password has been changed at $mess please contact to adminstrator"
                ], Response::HTTP_UNAUTHORIZED);
            }
        }
        return $next($request);
    }
}