<?php

namespace App\Http\Middleware;
use JWTAuth;
use App\Traits\RespondTrait;
use Illuminate\Support\Facades\Auth;
use Closure;
use Symfony\Component\HttpFoundation\Response;

class UserStatusMiddlewareApi{
    use RespondTrait;
    public function __construct()
    {
    }

    public function handle($request ,  Closure $next){
        if(Auth::check()){
            $user = Auth::user();
            if($user->status == 'inactive'){
                JWTAuth::invalidate($request->token);
                return $this->errorResponse([
                    'message' => 'User account has been disabled'
                ], Response::HTTP_UNAUTHORIZED);
            }
        }
        return $next($request);
    }
}