<?php

namespace App\Http\Controllers;

use App\Exceptions\CustomException;
use App\Http\Controllers\Controller;
use App\Http\Requests\ChangePasswordRequest;
use App\Interfaces\ProfileInterface;
use App\Interfaces\UserRepositoryInterface;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\DB;
use App\Models\User;
use Illuminate\Support\Facades\Validator;

class ProfileController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    private $profieInterface;
    private $userInterface;
    public function __construct(ProfileInterface $profieInterface, UserRepositoryInterface $userInterface)
    {
        $this->profieInterface = $profieInterface;
        $this->userInterface   = $userInterface;
    }

    public function update(Request $request)
    {
        $user =  $this->userInterface->authenticatedUser();
        $userDetail = $this->userInterface->findById($user->id);

        if ($request->isMethod('post')) {
            Validator::make($request->all(), [
                'name'      => 'required'
            ])->validate();

            $user = $this->profieInterface->update($request->all(), $user->id);

            if ($user) {
                return back()->with('success', 'Profile updated successfully!');
            }
            return back()->with('error', 'Could not update user at the moment!');
        }

        return view('profile.update_profile', ['userDetail' => $userDetail]);
    }

    public function change_pass(ChangePasswordRequest $request)
    {
        $user =  $this->userInterface->authenticatedUser();
        $userDetail = $this->userInterface->findById($user->id);

        if ($request->isMethod('post')) {

            DB::beginTransaction();
            try {

                $changePassword = $this->profieInterface->changeUserPassword($request->all(), $user->id);
                if ($changePassword) {
                    DB::commit();
                    return back()->with('success', 'Password changed successfully!');
                }
            } catch (CustomException $exception) {
                return back()->with('error', $exception->getMessage());
            } catch (\Exception $e) {
                DB::rollback();
                return back()->with('error', "cannot change the password");
            }
        }

        return view('profile.change_password', ['userDetail' => $userDetail]);
    }
}
