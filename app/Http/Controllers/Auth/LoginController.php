<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Models\User;
use App\Providers\RouteServiceProvider;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Http\Request;
use Illuminate\Validation\ValidationException;

class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    // protected $redirectTo = RouteServiceProvider::HOME;
    protected $redirectTo = '/dashboard';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest')->except('logout');
    }
    public function username()
{
    return 'username';
}

    protected function validateLogin(Request $request)
{
    // Get the user details from database and check if user is exist and active.
    $user = User::where('username',$request->email)->first();
    if( $user && !$user->status == 'inactive'){
        throw ValidationException::withMessages([('User has been desactivated.')]);
    }

    if($user && $user->user_type == 'collector'){
        throw ValidationException::withMessages([('Unauthorized entry')]);
    }

    // Then, validate input.
    return $request->validate([
        $this->username() => 'required|string',
        'password' => 'required|string',
    ]);
}
}
