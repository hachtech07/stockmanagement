<?php

namespace App\Http\Controllers;

use App\Exceptions\CustomException;
use Bsdate;
use JWTAuth;
use Exception;
use App\Models\User;
use App\Models\Billing;
use App\Models\Customer;
use App\Traits\RespondTrait;
use Illuminate\Http\Request;
use App\Models\BillingDetail;
use App\Models\BillingCategory;
use App\Traits\FiscalYear;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Http;
use Illuminate\Support\Facades\Validator;
use App\Http\Requests\BillingStoreRequest;
use Tymon\JWTAuth\Exceptions\JWTException;
use Spatie\Activitylog\Models\Activity;
use Symfony\Component\HttpFoundation\Response;
use Tymon\JWTAuth\Claims\DatetimeTrait;

class ApiController extends Controller
{
    use RespondTrait, FiscalYear, DatetimeTrait;
    public function authenticate(Request $request)
    {
        //$credentials = $request->only('email', 'password');
        //valid credential
        $credentials = [
            'email' => $request->get('email'),
            'password' => $request->get('password'),
            'user_type' => 'collector',
            'status'    => 'active'
        ];
        $validator = Validator::make($credentials, [
            'email' => 'required|email',
            'password' => 'required|string|min:6|max:50'
        ]);

        //Send failed response if request is not valid
        if ($validator->fails()) {
            // return response()->json(['error' => $validator->messages()], 200);
            return $this->errorResponse($validator->errors(), 422);
        }

        //Request is validated
        //Crean token
        try {
            if (!$token = JWTAuth::attempt($credentials)) {
                return $this->errorResponse([
                    'message' => 'Login credentials are invalid.',
                ], 401);
            }
        } catch (JWTException $e) {
            return $this->errorResponse([
                'message' => 'Could not create token.',
            ], 500);
        }
        $user = User::where('email', $credentials['email'])->first();
        $user->logged_in_at    = date("Y-m-d h:i:s", time());
        $user->save();
        activity('Api logging')
            ->performedOn($user)
            ->causedBy($user->id)
            ->withProperties(['email' => $credentials['email'], 'user_type' => $credentials['user_type']])
            ->log($user->name . ' ' . 'loggedIn');
        //Token created, return with success response and jwt token
        return $this->successResponse([
            'token' => $token
        ]);
    }

    public function logout(Request $request)
    {

        //Request is validated, do logout
        try {
            JWTAuth::invalidate($request->token);

            activity('Api logging')
                ->causedBy(auth()->user()->id)
                ->log(auth()->user()->name . ' ' . 'loggedout');
            return $this->successResponse(
                [],
                'User has been logged out'
            );
        } catch (JWTException $exception) {
            return $this->errorResponse([
                'message' => 'Sorry, user cannot be logged out'
            ], Response::HTTP_INTERNAL_SERVER_ERROR);
        }
    }

    public function get_user(Request $request)
    {
        $user = JWTAuth::authenticate($request->token);
        return $this->successResponse([$user]);
    }
    public function dashboard(Request $request)
    {
        $totalBill   = Billing::getTodaysNoOfBilling();
        $totalAmount = Billing::getTodaysTotalAmountOfBilling();
        $data        = ['totalBillGenerated' => $totalBill, 'totalCollectedAmount' => $totalAmount];
        return $this->successResponse([$data]);
    }
    public function search_user(Request $request)
    {
        $myKey = $request->get('search_key');
        $data  = Customer::searchCustomer(auth()->user()->id, $myKey);
        return $this->successResponse([$data]);
    }
    public function billingInformationForOffline(Request $request)
    {
        try {
            $yearmonth = config('yearmonth');
            $customersDetails = Customer::select('id', 'address', 'customer_number', 'name', 'phone_number', 'house_number', 'last_bill_date', DB::raw('YEAR(last_bill_date) as billingYearFrom'), DB::raw('MONTH(last_bill_date) as billingMonthFrom'), 'advance_amount', 'due_amount', 'monthly_payable')
                ->where('branch_id', "=", auth()->user()->branch_id)->where('collector_id', auth()->user()->id)->get();

            $billingCategories = \App\Models\BillingCategory::getActiveNonMonthlyCharges();

            $data = [
                'yearmonth'         => $yearmonth,
                'billingCategories' => $billingCategories,
                'customerDetails'   => $customersDetails
            ];
            return $this->successResponse([$data]);
        } catch (JWTException $exception) {
            return $this->errorResponse([
                'message' => 'Sorry, Unable To get Bill Information'
            ], Response::HTTP_INTERNAL_SERVER_ERROR);
        }
    }
    public function billingPage(Request $request)
    {
        try {
            $split_date = '';
            $date = '';
            $custumer = '';
            $arr = [];
            $yearmonth = config('yearmonth');
            $customerId = $request->get('customer_id');
            if ($customerId == null) {
                return response()->json(['status' => 'Error', 'message' => 'CustomerId required', 'data' => null]);
            }

            $customersDetails = Customer::select('id', 'customer_number', 'name', 'phone_number', 'house_number', 'last_bill_date', 'last_bill_date as billingYearFrom', 'last_bill_date as billingYearTo', 'last_bill_date as billingMonthFrom', 'last_bill_date as billingMonthTo', 'advance_amount', 'due_amount', 'monthly_payable')
                ->where('id', '=', $customerId)->where('collector_id', auth()->user()->id)
                ->get();
            if (count($customersDetails) > 0) {
                $split_date = json_decode($customersDetails, true)[0]['last_bill_date'];
                $date = explode('-', $split_date);
                $custumer = json_decode($customersDetails, true)[0];
                $arr = [
                    'id' => $custumer['id'],
                    'customer_number' => $custumer['customer_number'],
                    'name' => $custumer['name'],
                    'phone_number' => $custumer['phone_number'],
                    'house_number' => $custumer['house_number'],
                    'last_bill_date' => $custumer['last_bill_date'],
                    'billingYearFrom' => $date[0],
                    'billingYearTo'    => $date[0],
                    'billingMonthFrom' => $date[1],
                    'billingMonthTo'   => $date[1],
                    'advance_amount'   => $custumer['advance_amount'],
                    'due_amount'       => $custumer['due_amount'],
                    'monthly_payable'  => $custumer['monthly_payable']
                ];
            }
            $billingCategories = \App\Models\BillingCategory::getActiveNonMonthlyCharges();

            $data = [
                'yearmonth'         => $yearmonth,
                'billingCategories' => $billingCategories,
                // 'customerDetail'   => $customersDetails,
                'customerDetails' => count($customersDetails) > 0 ?  array($arr)
                    : []
            ];

            // $customersDetails = Customer::select('id', 'customer_number', 'name', 'phone_number', 'house_number', 'last_bill_date', DB::raw('YEAR(last_bill_date) as billingYearFrom'), DB::raw('YEAR(last_bill_date) as billingYearTo'), DB::raw('MONTH(last_bill_date) as billingMonthFrom'), DB::raw('MONTH(last_bill_date) as billingMonthTo'), 'advance_amount', 'due_amount', 'monthly_payable')
            // ->where('id', '=', $customerId)
            // ->get();


            // $billingCategories = \App\Models\BillingCategory::getActiveNonMonthlyCharges();

            // $data = [
            //     'yearmonth'         => $yearmonth,
            //     'billingCategories' => $billingCategories,
            //     'customerDetails'   => $customersDetails
            // ];
            return $this->successResponse([$data]);
        } catch (JWTException $exception) {
            return $this->errorResponse([
                'message' => 'Sorry, Unable To get Bill Information'
            ], Response::HTTP_INTERNAL_SERVER_ERROR);
        }
    }
    public function billingPagebackup(Request $request)
    {
        try {
            $yearmonth = config('yearmonth');
            $customerId = $request->get('customer_id');
            if ($customerId == null) {
                return back()->with('error', 'Select customer to add billing.');
            }

            $customerBilling    = Billing::getLastBilling($customerId);

            $payable            = $customerBilling->customer->monthly_payable + $customerBilling->due_amount - $customerBilling->advance_amount;

            $defaultPayableMonth = 1;

            //for pre select in year and months
            if ($customerBilling->to_month < 12) {
                $billingYearFrom    = $customerBilling->to_year;
                $billingMonthFrom   = $customerBilling->to_month + 1;
                $billingYearTo      = (($customerBilling->to_month + 1) > 12) ? $customerBilling->to_year + 1 : $customerBilling->to_year;
                $billingMonthTo     = (($customerBilling->to_month + 1) > 12) ?  1 : $customerBilling->to_month + 1;
            } else {
                $billingYearFrom    = $customerBilling->to_year + 1;
                $billingMonthFrom   = 1;
                $billingYearTo      = $customerBilling->to_year + 1;
                $billingMonthTo     = 2;
            }

            // when customer have paid advance and paid advance is greater than monthly payable
            if ($customerBilling->advance_amount > $customerBilling->customer->monthly_payable) {
                $billingMonthTo += 1;

                $nextPayableMonth = 1;
                while ($customerBilling->advance_amount > $nextPayableMonth * $customerBilling->customer->monthly_payable) {
                    $nextPayableMonth++;
                }

                $defaultPayableMonth = ($customerBilling->advance_amount == $nextPayableMonth * $customerBilling->customer->monthly_payable) ? $nextPayableMonth + 1 : $nextPayableMonth;
                $billingMonthTo  = $nextPayableMonth + $billingMonthFrom - 1;

                if ($billingMonthTo > 12) {
                    $remainder = $billingMonthTo % 12;
                    $quotient  = ($billingMonthTo - $remainder) / 12;

                    $billingMonthTo = $remainder + 1;
                    $billingYearTo  = $billingYearTo + $quotient;
                    $payable         = ($customerBilling->customer->monthly_payable * ($nextPayableMonth + 1)) + $customerBilling->due_amount - $customerBilling->advance_amount;
                } else {
                    $payable         = ($customerBilling->customer->monthly_payable * $nextPayableMonth) + $customerBilling->due_amount - $customerBilling->advance_amount;
                }
            }

            $billingCategories = \App\Models\BillingCategory::getActiveNonMonthlyCharges();

            $data = [
                'yearmonth'         => $yearmonth,
                'billingCategories' => $billingCategories,
                'customerBilling'   => $customerBilling,
                'billingYearFrom'   => $billingYearFrom,
                'billingMonthFrom'  => $billingMonthFrom,
                'billingYearTo'     => $billingYearTo,
                'billingMonthTo'    => $billingMonthTo,
                'payable'           => $payable,
                'defaultPayableMonth' => $defaultPayableMonth,
            ];
            return $this->successResponse([$data]);
        } catch (JWTException $exception) {
            return $this->errorResponse([
                'message' => 'Sorry, Unable To get Bill Information'
            ], Response::HTTP_INTERNAL_SERVER_ERROR);
        }
    }

    public function billingStore(Request $request)
    {
        \DB::beginTransaction();
        try {

            $convertedDate = $this->current_date_to_nepali() . ' ' . date('H:i:s');
            $fiscalYear = $this->fiscalYear($convertedDate);
            $customer = Customer::find($request->get('customer_id'));

            if (!$customer) {
                throw new Exception('No customer not found', 404);
            }

            $billingTable = new Billing();
            $billingTable->customer_id = $request->input('customer_id');
            $billingTable->user_id = auth()->user()->id;
            $billingTable->date_np = $request->input('to_year') . "-" . $request->input('to_month') . "-30";
            $billingTable->from_year = $request->input('from_year');
            $billingTable->from_month = $request->input('from_month');
            $billingTable->to_year = $request->input('to_year');
            $billingTable->to_month = $request->input('to_month');
            $billingTable->gross_amount = $request->input('gross_amount');
            $billingTable->discount_amount = $request->input('discount_amount');
            $billingTable->net_amount = $request->input('net_amount');
            $billingTable->tender_amount = $request->input('tender_amount');
            $billingTable->advance_amount = (-1 * $request->input('due_amount'));
            // $billingTable->advance_amount = $customer->advance_amount + ($request->input('tender_amount') - $request->input('net_amount'));
            $billingTable->due_amount = $request->input('due_amount');
            $billingTable->vat_amount = $request->input('vat_amount');
            $billingTable->created_date_time = $request->input('created_date_time');
            $billingTable->discount_percent = $request->input('discount_percent');
            $billingTable->total_amount = $request->input('total_amount');
            if($customer->advance_amount > 0){
                $billingTable->previous_advance_amount = $customer->advance_amount ;
            }
            if($customer->due_amount > 0){
                $billingTable->previous_due_amount = $customer->due_amount ;
            }

            if ($customer->status == 'inactive') {
                return $this->errorResponse([
                    'message' => 'Customer is inactive cannot be billed'
                ], Response::HTTP_INTERNAL_SERVER_ERROR);
            }

            if ($customer->collector_id != auth()->user()->id) {
                return $this->errorResponse([
                    'message' => 'You cannot bill this customer'
                ], Response::HTTP_INTERNAL_SERVER_ERROR);
            }
            $billingTable->fiscal_year_id = $fiscalYear->id;

            $billingSaved = $billingTable->save();


            $data = $request->get('billingCategory');
            // $customer_details  = Customer::select('*')->where('id', '=', $request->input('customer_id'))->first();
            $previous_advance  = $customer->advance_amount;
            $extra_amount_paid = $customer->advance_amount + $request->input('due_amount');
            $customer->advance_amount = (-1 * $request->input('due_amount'));
            $customer->due_amount = $request->input('due_amount');

            $customer->last_bill_date = $request->input('to_year') . "-" . $request->input('to_month') . "-28";
            $customer->save();
            // monthly payable 
            $billingDetails = new BillingDetail();
            $billingDetails->billing_id = $billingTable->id;
            $billingDetails->billing_category_id = \App\Models\BillingCategory::MONTHLY_PAYABLE_CATEGORY_ID;
            $billingDetails->total_month_paid = $request->input('total_months');
            $billingDetails->billing_amount = $customer->monthly_payable * $request->input('total_months');
            $billingDetails->sub_total = $customer->monthly_payable * $request->input('total_months');

            $billingDetails->save();

            // detect if customer has paid due
            if ($previous_advance < 0) {
                $billingDetails = new BillingDetail();
                $billingDetails->billing_id = $billingTable->id;
                $billingDetails->billing_category_id = \App\Models\BillingCategory::DUE_AMOUNT_CATEGORY_ID;
                $billingDetails->total_month_paid = 1;
                $billingDetails->billing_amount = -1 * $previous_advance;
                $billingDetails->sub_total = -1 * $previous_advance;
                $billingDetails->save();
            }
            if ($previous_advance > 0) {
                $billingDetails = new BillingDetail();
                $billingDetails->billing_id = $billingTable->id;
                $billingDetails->billing_category_id = \App\Models\BillingCategory::ADVANCE_AMOUNT_CATEGORY_ID;
                $billingDetails->total_month_paid = 1;
                $billingDetails->billing_amount = 1 * $previous_advance;
                $billingDetails->sub_total = 1 * $previous_advance;
                $billingDetails->save();
            }

            if (!empty($data)) {
                foreach ($data as $da) {

                    $billingDetails = new BillingDetail();
                    $billingDetails->billing_id = $billingTable->id;
                    $billingDetails->billing_category_id = $da['billing_category_id'];
                    $billingDetails->billing_amount = $da['billing_amount'];
                    $billingDetails->total_month_paid = 1;
                    $billingDetails->sub_total = $da['billing_amount'];
                    $billingDetails->save();
                }
            }

            $taxsableSalesVat = $request->get('gross_amount') - ($request->get('discount_amount') != Null  ? $request->get('discount_amount') : 0);
            // $customer = Customer::where('id', $request->get('customer_id'))->first();

            $invoice = date_format($billingTable->created_at, 'Y-m-d');
             $convert = $this->convert_date_to_nepali($invoice);
             $formattedInvoice = explode('-', $convert);
            if ($billingTable->id) {
                $payload = [
                'username' => env('IRD_USERNAME'),
                'password' => env('IRD_PASSWORD'),
                'seller_pan' => env('COMPANY_PAN_VAT'),
                'buyer_pan' => $customer->vat_pan_number != Null ? $customer->vat_pan_number : 0,
                'buyer_name' => $customer->name,
                'fiscal_year' => $this->formattedFiscalDate($convertedDate),
                'invoice_number' => $billingTable->id,
                'invoice_date' => $formattedInvoice[0] . '.' . $formattedInvoice[1] . '.' . $formattedInvoice[2],
                'total_sales' => $request->get('net_amount'),
                'taxable_sales_vat' => $taxsableSalesVat,
                'vat' => 0.13 * $taxsableSalesVat,
                'excisable_amount' => 0.00,
                'excise' => 0.00,
                'taxable_sales_hst' => 0.00,
                'hst' => 0.00,
                'amount_for_esf' => 0.00,
                'esf' => 0.00,
                'export_sales' => 0.00,
                'tax_exempted_sales' => 0.00,
                'isrealtime' => true,
                'datetimeclient' => date("Y-m-d H:i:s")
                ];
                $response = Http::post('http://43.245.85.152:9050/api/bill', $payload);
                \DB::table('ird_response_log')->insert([
                    'billing_id' => $billingTable->id,
                    'response' => $response,
                    'created_at' => date('Y-m-d H:i:s')
                ]);
            }

            \DB::commit();

            $bill = Billing::find($billingTable->id);
            if ($response == '200') {
                $bill->ird_status = 'synced';
                $bill->save();
            } else {
                $bill->ird_status = 'unsynced';
                $bill->save();
            }
            \DB::commit();
            $bill_information = $this->getAllbillingDetailsById($billingTable->id);
            $billing_details = $this->getBillingDetails($billingTable->id);
            $data = [
                'bill_information' => $bill_information,
                'billing_details' => $billing_details,
                'issued_date' => Bsdate::eng_to_nep(Date('Y'), Date('m'), Date('d'))
            ];
            return $this->successResponse([$data]);
        } catch (JWTException $exception) {
            return $this->errorResponse([
                'message' => 'Sorry, Unable to store bill data'
            ], Response::HTTP_INTERNAL_SERVER_ERROR);
        } catch (Exception $exception) {
            \DB::rollback();
            return $this->errorResponse(['message' => $exception->getMessage()], $exception->getCode());
        }
    }




    public function billingStoreback(BillingStoreRequest $request)
    {
        \DB::beginTransaction();
        try {

            $hasPaidDue     = false;
            $hasPaidAdvance = false;

            $totalAmount    = 0;
            $grossAmount    = $request->get('gross_amount');
            $discountAmount = $request->get('discount_amount') || 0;
            $netAmount      = $request->get('net_amount');
            $tenderAmount   = $request->get('tender_amount');
            $tenderAmount   = $request->get('due_amount');
            $fromYear       = $request->get('from_year');
            $fromMonth      = $request->get('from_month');
            $toYear         = $request->get('to_year');
            $toMonth        = $request->get('to_month');
            $months         = \App\Models\BillingCategory::getNumberOfMonthPaid($fromYear, $fromMonth, $toYear, $toMonth);

            $customerId     = $request->get('customer');
            $customerDetail = \App\Models\Customer::find($customerId); // get monthly payable amount
            $lastCustomerBilling    = Billing::getLastBilling($customerId); // get due amount or advance amount

            // get category id from array

            $totalAmount = ($customerDetail->monthly_payable * $months) + $lastCustomerBilling->due_amount - $lastCustomerBilling->advance_amount;
            if ($request->get('billingCategory')) {
                $categoryIds = array_keys($request->get('billingCategory'));
                $billingCategories = \App\Models\BillingCategory::getCategoriesDetail($categoryIds);

                // add extra charges
                foreach ($billingCategories as $categoryDetail) {
                    if ($categoryDetail->monthly_charge === \App\Models\BillingCategory::MONTHLY_CHARGE) {
                        $totalAmount += $categoryDetail->default_price * $months;
                    } else {
                        $totalAmount += $categoryDetail->default_price * 1;
                    }
                }
            }


            if ($totalAmount != $grossAmount) {
                \DB::rollback();
                return back()->with('error', 'Could not add billing. Calculation mis-match' . $months . '-' . $totalAmount . '-' . $grossAmount);
            }

            $billingModel = new Billing;
            $billingModel->user_id        = \Auth::id();
            $billingModel->customer_id    = $request->get('customer');
            $billingModel->date_np        = date('Y-m-d');
            $billingModel->from_year      = $fromYear;
            $billingModel->from_month     = $fromMonth;
            $billingModel->to_year        = $toYear;
            $billingModel->to_month       = $toMonth;
            $billingModel->gross_amount   = $totalAmount;
            $billingModel->discount_amount = $discountAmount;
            $billingModel->net_amount     = $totalAmount - $discountAmount;
            $billingModel->tender_amount  = $tenderAmount;

            if (($totalAmount - $discountAmount) > $tenderAmount) {
                $billingModel->due_amount     = ($totalAmount - $discountAmount) - $tenderAmount;
                $billingModel->advance_amount = 0.00;
            } else if (($totalAmount - $discountAmount) < $tenderAmount) {
                $billingModel->due_amount     = 0.00;
                $billingModel->advance_amount = $tenderAmount - ($totalAmount - $discountAmount);
            } else {
                $billingModel->due_amount     = 0.00;
                $billingModel->advance_amount = 0.00;
            }

            $billingModel->save();


            // monthly payable 
            $billingData[] = [
                'billing_id'            => $billingModel->id,
                'billing_category_id'   => \App\Models\BillingCategory::MONTHLY_PAYABLE_CATEGORY_ID,
                'total_month_paid'      => $months,
                'billing_amount'        => $customerDetail->monthly_payable,
                'sub_total'             => $months * $customerDetail->monthly_payable,
            ];

            // detect if customer has paid due
            if (($lastCustomerBilling->due_amount > 0) && ($tenderAmount >= $netAmount)) {
                $billingData[] = [
                    'billing_id'            => $billingModel->id,
                    'billing_category_id'   => \App\Models\BillingCategory::DUE_AMOUNT_CATEGORY_ID,
                    'total_month_paid'      => 1,
                    'billing_amount'        => $lastCustomerBilling->due_amount,
                    'sub_total'             => $lastCustomerBilling->due_amount,
                ];
            }

            // detect if customer has paid advance
            if ($tenderAmount > $netAmount) {
                $billingData[] = [
                    'billing_id'            => $billingModel->id,
                    'billing_category_id'   => \App\Models\BillingCategory::ADVANCE_AMOUNT_CATEGORY_ID,
                    'total_month_paid'      => 1,
                    'billing_amount'        => $tenderAmount - $totalAmount - $discountAmount,
                    'sub_total'             => $tenderAmount - $totalAmount - $discountAmount,
                ];
            }

            if ($request->get('billingCategory')) {
                foreach ($billingCategories as $key => $billingCategory) {
                    $mon = ($billingCategory->monthly_charge == \App\Models\BillingCategory::MONTHLY_CHARGE) ? $months : 1;

                    $billingData[] = [
                        'billing_id'           => $billingModel->id,
                        'billing_category_id'  => $billingCategory->id,
                        'total_month_paid'     => $mon,
                        'billing_amount'       => $billingCategory->default_price,
                        'sub_total'            => $billingCategory->default_price * $mon,
                    ];
                }
            }

            \App\Models\BillingDetail::insert($billingData);

            \DB::commit();
            $data = [];
            return $this->successResponse([$data]);
        } catch (JWTException $exception) {
            return $this->errorResponse([
                'message' => 'Sorry, Unable to store bill data'
            ], Response::HTTP_INTERNAL_SERVER_ERROR);
        }
    }

    public function todayBillList(Request $request)
    {
        try {
            $data  = Billing::getTodaysBillList(auth()->user()->id);
            return $this->successResponse([$data]);
        } catch (JWTException $exception) {
            return $this->errorResponse([
                'message' => 'Sorry, Unable to list bill'
            ], Response::HTTP_INTERNAL_SERVER_ERROR);
        }
    }
    public function billDetails(Request $request)
    {
        try {
            $data  = Billing::getBillDetails($request->get('id'));
            return $this->successResponse([$data]);
        } catch (JWTException $exception) {
            return $this->errorResponse([
                'message' => 'Sorry, Unable to view details of the bill'
            ], Response::HTTP_INTERNAL_SERVER_ERROR);
        }
    }
    public function getBillingDetails($billId)
    {
        $a = Billing::LEFTJOIN('users', 'users.id', '=', 'billing.user_id')
            ->LEFTJOIN('customers', 'customers.id', '=', 'billing.customer_id')
            ->WHERE('billing.id', '=', $billId)
            ->first([
                'billing.id',
                'billing.created_at',
                'billing.created_date_time',
                'billing.date_np',
                'billing.from_year',
                'billing.from_month',
                'billing.to_year',
                'billing.to_month',
                'billing.gross_amount',
                'billing.discount_amount',
                'billing.vat_amount',
                'billing.net_amount',
                'billing.tender_amount',
                'billing.advance_amount',
                'customers.name',
                'customers.address',
                'customers.customer_number',
            'users.name as billing_by',
            'billing.total_amount',
            ]);
        return $a;
    }
    public function getAllbillingDetailsById($billId)
    {
        $a = BillingDetail::LEFTJOIN('billing_categories', 'billing_categories.id', '=', 'billing_details.billing_category_id')
            ->WHERE('billing_details.billing_id', '=', $billId)
            ->get(
                [
                    'billing_categories.title',
                    'billing_details.total_month_paid',
                    'billing_details.sub_total'
                ]
            );

        return $a;
    }

    public function syncOflineBills(Request $request)
    {

        $all_data = $request->all();

        \DB::beginTransaction();
        try {
            foreach ($all_data as $data_main) {
                $convertedDate = $this->current_date_to_nepali() . ' ' . date('H:i:s');
                $fiscalYear = $this->fiscalYear($convertedDate);
                $billingTable = new Billing();
                $billingTable->customer_id = $data_main['customer_id'];
                $billingTable->user_id = $data_main['user_id'];
                $billingTable->date_np = $data_main['to_year'] . "-" . $data_main['to_month'] . "-30";
                $billingTable->from_year = $data_main['from_year'];
                $billingTable->from_month = $data_main['from_month'];
                $billingTable->to_year = $data_main['to_year'];
                $billingTable->to_month = $data_main['to_month'];
                $billingTable->gross_amount = $data_main['gross_amount'];
                $billingTable->discount_amount = $data_main['discount_amount'];
                $billingTable->net_amount = $data_main['net_amount'];
                $billingTable->tender_amount = $data_main['tender_amount'];
                $billingTable->advance_amount = (-1 * $data_main['due_amount']);
                $billingTable->due_amount = $data_main['due_amount'];
                $billingTable->vat_amount = $data_main['vat_amount'];
                $billingTable->created_date_time = $data_main['created_date_time'];
                $billingTable->discount_percent = $data_main['discount_percent'];
                $billingTable->fiscal_year_id = $fiscalYear->id;
                $billingTable->total_amount = $data_main['total_amount'];
                $billingSaved = $billingTable->save();


                $data = $data_main['billingCategory'];
                //return $data[0];
                $customer_details  = Customer::select('*')->where('id', '=', $data_main['customer_id'])->first();
                $previous_advance  = $customer_details->advance_amount;
                $extra_amount_paid = $customer_details->advance_amount + $data_main['due_amount'];
                $customer_details->advance_amount = (-1 * $data_main['due_amount']);
                $customer_details->due_amount = $data_main['due_amount'];

                $customer_details->last_bill_date = $data_main['to_year'] . "-" . $data_main['to_month'] . "-28";
                $customer_details->save();
                // monthly payable 
                $billingDetails = new BillingDetail();
                $billingDetails->billing_id = $billingTable->id;
                $billingDetails->billing_category_id = \App\Models\BillingCategory::MONTHLY_PAYABLE_CATEGORY_ID;
                $billingDetails->total_month_paid = $data_main['total_months'];
                $billingDetails->billing_amount = $customer_details->monthly_payable * $data_main['total_months'];
                $billingDetails->sub_total = $customer_details->monthly_payable * $data_main['total_months'];

                $billingDetails->save();

                // detect if customer has paid due
                if ($previous_advance < 0) {
                    $billingDetails = new BillingDetail();
                    $billingDetails->billing_id = $billingTable->id;
                    $billingDetails->billing_category_id = \App\Models\BillingCategory::DUE_AMOUNT_CATEGORY_ID;
                    $billingDetails->total_month_paid = 1;
                    $billingDetails->billing_amount = -1 * $previous_advance;
                    $billingDetails->sub_total = -1 * $previous_advance;
                    $billingDetails->save();
                }
                if ($previous_advance > 0) {
                    $billingDetails = new BillingDetail();
                    $billingDetails->billing_id = $billingTable->id;
                    $billingDetails->billing_category_id = \App\Models\BillingCategory::ADVANCE_AMOUNT_CATEGORY_ID;
                    $billingDetails->total_month_paid = 1;
                    $billingDetails->billing_amount = -1 * $previous_advance;
                    $billingDetails->sub_total = -1 * $previous_advance;
                    $billingDetails->save();
                }

                // return $data;

                if (!empty($data)) {
                    foreach ($data as $da) {

                        $billingDetails = new BillingDetail();
                        $billingDetails->billing_id = $billingTable->id;
                        $billingDetails->billing_category_id = $da['billing_category_id'];
                        $billingDetails->billing_amount = $da['billing_amount'];
                        $billingDetails->total_month_paid = 1;
                        $billingDetails->sub_total = $da['billing_amount'];
                        $billingDetails->save();
                    }
                }
            }
            $taxsableSalesVat = $data_main['gross_amount'] - ($data_main['discount_amount'] != Null  ? $data_main['discount_amount'] : 0);
            // $customer = Customer::where('id', $data_main['customer_id'])->first();

            if ($billingTable->id) {
                $response = Http::post('http://43.245.85.152:9050/api/bill', [
                    'username' => env('IRD_USERNAME'),
                    'password' => env('IRD_PASSWORD'),
                    'seller_pan' => env('COMPANY_PAN_VAT'),
                    'buyer_pan' => $customer_details->vat_pan_number != Null ? $customer_details->vat_pan_number : 0,
                    'buyer_name' => $customer_details->name,
                    'fiscal_year' => '2073.074',
                    'invoice_number' => $billingTable->id,
                    'invoice_date' => '2021.01.02',
                    'total_sales' => $data_main['net_amount'],
                    'taxable_sales_vat' => $taxsableSalesVat,
                    'vat' => 0.13 * $taxsableSalesVat,
                    'excisable_amount' => 0,
                    'excise' => 0,
                    'taxable_sales_hst' => 0,
                    'hst' => 0,
                    'amount_for_esf' => 0,
                    'esf' => 0,
                    'export_sales' => 0,
                    'tax_exempted_sales' => 0,
                    'isrealtime' => true,
                    'datetimeclient' => date("Y-m-d H:i:s")
                ]);

                \DB::table('ird_response_log')->insert([
                    'billing_id' => $billingTable->id,
                    'response' => $response,
                    'created_at' => date('Y-m-d H:i:s')
                ]);
            }
            \DB::commit();
            $bill = Billing::find($billingTable->id);
            if ($response == '200') {
                $bill->ird_status = 'synced';
                $bill->save();
            } else {
                $bill->ird_status = 'unsynced';
                $bill->save();
            }
            \DB::commit();

            $bill_information = $this->getAllbillingDetailsById($billingTable->id);
            $billing_details = $this->getBillingDetails($billingTable->id);
            $data = [
                'bill_information' => $bill_information,
                'billing_details' => $billing_details,
            ];

            return $this->successResponse([$data]);
        } catch (JWTException $exception) {
            return $this->errorResponse([
                'message' => 'Sorry, Unable to store bill data'
            ], Response::HTTP_INTERNAL_SERVER_ERROR);
        }
    }

    // 
    public function customer_statement($id)
    {
        try {
            if (!$id) {
                return $this->errorResponse(['message' => 'Details with' . $id . ' not found'], Response::HTTP_UNPROCESSABLE_ENTITY);
            }

            $data = Billing::query()
                ->with(['billing_details' => function ($query) {
                    $query->select('bc.title as billing_category', 'billing_details.*',)
                        ->join('billing_categories as bc', 'bc.id', 'billing_details.billing_category_id');
                }])
                ->select('billing.*', 'users.name as billing_by')
                ->join('users', 'users.id', 'billing.user_id')
                ->where('customer_id', $id)->where('billing.net_amount', '>', '0')->orderBy('id', 'desc')->limit(3)->get();
            return $this->successResponse([$data]);
        } catch (Exception $exception) {
            return $this->errorResponse([
                'message' => $exception
            ], Response::HTTP_INTERNAL_SERVER_ERROR);
        }
    }

    public function duplicate_bill_print($id)
    {
        try {
            if (!$id) {
                return $this->errorResponse(['message' => 'Details with' . $id . ' not found'], Response::HTTP_UNPROCESSABLE_ENTITY);
            }

            $data = Billing::where('id', $id)->first();
            $data->printcount += 1;
            $data->save();

            return $this->successResponse([$data]);
        } catch (Exception $exception) {
            return $this->errorResponse([
                'message' => $exception
            ], Response::HTTP_INTERNAL_SERVER_ERROR);
        }
    }
}
