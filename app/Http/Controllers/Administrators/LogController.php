<?php

namespace App\Http\Controllers\Administrators;

use App\Http\Controllers\Controller;
use App\Interfaces\LogInterface;
use App\Models\Customer;
use App\Models\User;
use Illuminate\Http\Request;

class LogController extends Controller{

    private $logInterface;
    public function __construct(LogInterface $logInterface)
    {
        $this->logInterface = $logInterface;
    }

    public function index(Request $request){
        $data = $this->logInterface->userLogIndex($request->all());
        return view('administrators.activity-log.index',$data);
    }

}