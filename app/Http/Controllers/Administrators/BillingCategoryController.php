<?php

namespace App\Http\Controllers\Administrators;

use App\Http\Controllers\Controller;
use App\Models\BillingCategory;
use App\Http\Requests\BillingCategoryStoreRequest;
use App\Interfaces\BillingCategoryInterface;
use App\Traits\AuthTrait;
use Illuminate\Support\Facades\Auth;
use Exception;

class BillingCategoryController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    use AuthTrait;
    private $billingCategoryInterface;
    private $billingCategory;

    public function __construct(BillingCategoryInterface $billingCategoryInterface, BillingCategory $billingCategory)
    {
        $this->billingCategoryInterface = $billingCategoryInterface;
        $this->billingCategory = $billingCategory;
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        $billingCategories = $this->billingCategory->getActive();
        return view('administrators.billing-category.index', ['billingCategories' => $billingCategories]);
    }

    public function create()
    {
        return view('administrators.billing-category.create')->with('billingCategoryModel', new BillingCategory);
    }

    public function store(BillingCategoryStoreRequest $request)
    {
        $request['created_by'] = $this->authId();

        $save = $this->billingCategory->create($request->all());

        if ($save) {
            return back()->with('success', 'Billing Category created successfully!');
        }
        return back()->with('error', 'Could not create billing category at the moment!');
    }

    public function update(BillingCategoryStoreRequest $request, $id)
    {
        $billingCategoryModel = $this->billingCategory->find($id);

        if ($request->isMethod('post')) {
            try {
                $billingCategoryUpdate = $this->billingCategoryInterface->update($request->all(), $id);

                return back()->with('success', 'Billing Category updated successfully!');
            } catch (Exception $e) {
                return back()->with('error', 'Could not update billing category at the moment!' . $e);
            }
        }
        return view('administrators.billing-category.create')->with('billingCategoryModel', $billingCategoryModel);
    }
}
