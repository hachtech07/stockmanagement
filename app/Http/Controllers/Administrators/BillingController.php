<?php

namespace App\Http\Controllers\Administrators;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Http\Requests\BillingStoreRequest;
use App\Interfaces\BillingInterface;
use App\Models\Bill_return;
use App\Models\Bill_return_details;
use App\Models\Billing;
use App\Models\BillingDetail;
use App\Models\BillingCategory;
use App\Models\Customer;
use App\Traits\DateConveterTrait;
use App\Traits\FiscalYear;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use DataTables;
use Exception;
use PDF;

class BillingController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    use DateConveterTrait, FiscalYear;
    private $billingInterface;

    public function __construct(BillingInterface $billingInterface)
    {
        $this->billingInterface = $billingInterface;
    }
    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index(Request $request)
    {
        if ($request->isMethod('post') || $request->isMethod('get')) {
            $data = $this->billingInterface->getBillIndex($request->all());
        }
        return view('administrators.billing.index', $data);
    }

    public function billReturn(Request $request, $id)
    {
        try {
            $this->billingInterface->cancelBill($request->all(), $id);
            return redirect()->back()->with('success', 'Returned successfully');
        } catch (Exception $e) {
            return back()->with('error', 'fail to return selected item');
        }
    }

    public function billReturnIndex(Request $request)
    {
        try {
            if ($request->isMethod('post') || $request->isMethod('get')) {
                $data = $this->billingInterface->getAllReturnedBillIndex($request->all());
            }
            return view('administrators.billing.return', $data);
        } catch (Exception $e) {
            return back()->with('error', 'Somethings went wrong !!!');
        }
    }

    public function billReturnDetailsIndex($id)
    {
        try {
            $data = $this->billingInterface->getAllReturnBillDetail($id);
            return view('administrators.billing.return-detail', $data);
        } catch (Exception $e) {
            return back()->with('error', 'Somethings went wrong !!!');
        }
    }

    public function printBillReturnBill($id)
    {
        try {
            $data = $this->billingInterface->billReturnPrint($id);
            $pdf = PDF::loadView('administrators.billing.printReturnBill', $data);
            $pdf->setPaper('A2', 'landscape');
            // return $pdf->download('bill_return.pdf');
            return $pdf->download('retuern-bill.pdf');
        } catch (Exception $e) {
            return back()->with('error', 'Somethings went wrong !!!');
        }
    }

    public function searchCustomer(Request $request)
    {
        $searchParam = $request->get('param');
        $customers = [];
        $customers = Customer::query();
        if (Auth::user()->user_type == 'branch admin') {
            $customers = $customers->where('branch_id', Auth::user()->branch_id)->where('name', 'like', '%' . $searchParam . '%')->get();
            return json_encode($customers);
        }
        $customers = $customers->where('name', 'like', '%' . $searchParam . '%')->get();
        return json_encode($customers);
    }

    public function view($id)
    {
        $data = $this->billingInterface->viewBilling($id);
        return view('administrators.billing.view', $data);
    }

    public function create(Request $request, $customerId = NULL)
    {
        $yearmonth = config('yearmonth');
        if ($customerId == NULL) {
            return back()->with('error', 'Select customer to add billing.');
        }
        $billingCategories = BillingCategory::getActiveNonMonthlyCharges();

        $customerBilling    = Billing::getLastBilling($customerId);
        // if(!empty($customerBilling)){
        $payable            = $customerBilling->customer->monthly_payable + $customerBilling->due_amount - $customerBilling->advance_amount;

        $defaultPayableMonth = 1;

        //for pre select in year and months
        if ($customerBilling->to_month < 12) {
            $billingYearFrom    = $customerBilling->to_year;
            $billingMonthFrom   = $customerBilling->to_month + 1;
            $billingYearTo      = (($customerBilling->to_month + 1) > 12) ? $customerBilling->to_year + 1 : $customerBilling->to_year;
            $billingMonthTo     = (($customerBilling->to_month + 1) > 12) ?  1 : $customerBilling->to_month + 1;
        } else {
            $billingYearFrom    = $customerBilling->to_year + 1;
            $billingMonthFrom   = 1;
            $billingYearTo      = $customerBilling->to_year + 1;
            $billingMonthTo     = 2;
        }

        // when customer have paid advance and paid advance is greater than monthly payable
        if ($customerBilling->advance_amount > $customerBilling->customer->monthly_payable) {
            $billingMonthTo += 1;

            $nextPayableMonth = 1;
            while ($customerBilling->advance_amount > $nextPayableMonth * $customerBilling->customer->monthly_payable) {
                $nextPayableMonth++;
            }

            $defaultPayableMonth = ($customerBilling->advance_amount == $nextPayableMonth * $customerBilling->customer->monthly_payable) ? $nextPayableMonth + 1 : $nextPayableMonth;
            $billingMonthTo  = $nextPayableMonth + $billingMonthFrom - 1;

            if ($billingMonthTo > 12) {

                $remainder = $billingMonthTo % 12;
                $quotient  = ($billingMonthTo - $remainder) / 12;

                $billingMonthTo = $remainder + 1;
                $billingYearTo  = $billingYearTo + $quotient;
                $payable         = ($customerBilling->customer->monthly_payable * ($nextPayableMonth + 1)) + $customerBilling->due_amount - $customerBilling->advance_amount;
            } else {
                $payable         = ($customerBilling->customer->monthly_payable * $nextPayableMonth) + $customerBilling->due_amount - $customerBilling->advance_amount;
            }
        }



        return view('administrators.billing.create', [
            'yearmonth'         => $yearmonth,
            'billingCategories' => $billingCategories,
            'customerBilling'   => $customerBilling,
            'billingYearFrom'   => $billingYearFrom,
            'billingMonthFrom'  => $billingMonthFrom,
            'billingYearTo'     => $billingYearTo,
            'billingMonthTo'    => $billingMonthTo,
            'payable'           => $payable,
            'defaultPayableMonth' => $defaultPayableMonth,
        ]);
    }

    public function store_bck(Request $request)
    {
        $request->validate([
            'customer'          => 'required',
            'months'            => 'required|numeric|min:1|max:12',
            'billingCategory'   => 'required',
            'discount_amount'   => 'numeric',
            'gross_amount'      => 'required|numeric',
            'net_amount'        => 'required|numeric',
        ]);

        DB::beginTransaction();
        try {

            $totalAmount = 0;
            $grossAmount   = $request->get('gross_amount');
            $discountAmount = $request->get('discount_amount');
            $netAmount      = $request->get('net_amount');

            // get category id from array
            $categoryIds = array_keys($request->get('billingCategory'));

            $billingCategories = BillingCategory::getCategoriesDetail($categoryIds);
            foreach ($billingCategories as $categoryDetail) {
                if ($categoryDetail->monthly_charge === 'yes') {
                    $totalAmount += $categoryDetail->default_price * $request->get('months');
                } else {
                    $totalAmount += $categoryDetail->default_price * 1;
                }
            }

            $billingModel = new Billing;
            $billingModel->user_id        = Auth::id();
            $billingModel->customer_id    = $request->get('customer');
            $billingModel->gross_amount   = $totalAmount;
            $billingModel->discount_amount = $discountAmount;
            $billingModel->net_amount     = $totalAmount - $discountAmount;

            $billingModel->save();

            foreach ($billingCategories as $key => $billingCategory) {
                $billingDetailModel = new \App\Models\BillingDetail;

                $billingDetailModel->billing_id           = $billingModel->id;
                $billingDetailModel->billing_category_id  = $billingCategory->id;
                $billingDetailModel->billing_amount       = $billingCategory->default_price;

                $billingDetailModel->save();
            }

            DB::commit();
            return back()->with('success', 'Bill paid successfully!');
        } catch (\Exception $e) {
            DB::rollback();
            return back()->with('error', 'Could not create billing category at the moment!' . $e);
        }
    }

    public function store(BillingStoreRequest $request)
    {
        DB::beginTransaction();
        try {

            $hasPaidDue     = false;
            $hasPaidAdvance = false;

            $totalAmount    = 0;
            $grossAmount    = $request->get('gross_amount');
            $discountAmount = $request->get('discount_amount') || 0;
            $netAmount      = $request->get('net_amount');
            $tenderAmount   = $request->get('tender_amount');
            $fromYear       = $request->get('from_year');
            $fromMonth      = $request->get('from_month');
            $toYear         = $request->get('to_year');
            $toMonth        = $request->get('to_month');
            $months         = BillingCategory::getNumberOfMonthPaid($fromYear, $fromMonth, $toYear, $toMonth);

            $customerId     = $request->get('customer');
            $customerDetail = Customer::find($customerId); // get monthly payable amount
            $lastCustomerBilling    = Billing::getLastBilling($customerId); // get due amount or advance amount

            // get category id from array

            $totalAmount = ($customerDetail->monthly_payable * $months) + $lastCustomerBilling->due_amount - $lastCustomerBilling->advance_amount;
            if ($request->get('billingCategory')) {
                $categoryIds = array_keys($request->get('billingCategory'));
                $billingCategories = \App\Models\BillingCategory::getCategoriesDetail($categoryIds);

                // add extra charges
                foreach ($billingCategories as $categoryDetail) {
                    if ($categoryDetail->monthly_charge === \App\Models\BillingCategory::MONTHLY_CHARGE) {
                        $totalAmount += $categoryDetail->default_price * $months;
                    } else {
                        $totalAmount += $categoryDetail->default_price * 1;
                    }
                }
            }

            $billingModel = new Billing;
            $billingModel->user_id        = Auth::id();
            $billingModel->customer_id    = $request->get('customer');
            $billingModel->date_np        = date('Y-m-d');
            $billingModel->from_year      = $fromYear;
            $billingModel->from_month     = $fromMonth;
            $billingModel->to_year        = $toYear;
            $billingModel->to_month       = $toMonth;
            $billingModel->gross_amount   = $totalAmount;
            $billingModel->discount_amount = $discountAmount;
            $billingModel->net_amount     = $totalAmount - $discountAmount;
            $billingModel->tender_amount  = $tenderAmount;

            if (($totalAmount - $discountAmount) > $tenderAmount) {
                $billingModel->due_amount     = ($totalAmount - $discountAmount) - $tenderAmount;
                $billingModel->advance_amount = 0.00;
            } else if (($totalAmount - $discountAmount) < $tenderAmount) {
                $billingModel->due_amount     = 0.00;
                $billingModel->advance_amount = $tenderAmount - ($totalAmount - $discountAmount);
            } else {
                $billingModel->due_amount     = 0.00;
                $billingModel->advance_amount = 0.00;
            }

            $billingModel->save();

            // monthly payable 
            $billingData[] = [
                'billing_id'            => $billingModel->id,
                'billing_category_id'   => \App\Models\BillingCategory::MONTHLY_PAYABLE_CATEGORY_ID,
                'total_month_paid'      => $months,
                'billing_amount'        => $customerDetail->monthly_payable,
                'sub_total'             => $months * $customerDetail->monthly_payable,
            ];

            // detect if customer has paid due
            if (($lastCustomerBilling->due_amount > 0) && ($tenderAmount >= $netAmount)) {
                $billingData[] = [
                    'billing_id'            => $billingModel->id,
                    'billing_category_id'   => \App\Models\BillingCategory::DUE_AMOUNT_CATEGORY_ID,
                    'total_month_paid'      => 1,
                    'billing_amount'        => $lastCustomerBilling->due_amount,
                    'sub_total'             => $lastCustomerBilling->due_amount,
                ];
            }

            // detect if customer has paid advance
            if ($tenderAmount > $netAmount) {
                $billingData[] = [
                    'billing_id'            => $billingModel->id,
                    'billing_category_id'   => \App\Models\BillingCategory::ADVANCE_AMOUNT_CATEGORY_ID,
                    'total_month_paid'      => 1,
                    'billing_amount'        => $tenderAmount - $totalAmount - $discountAmount,
                    'sub_total'             => $tenderAmount - $totalAmount - $discountAmount,
                ];
            }

            if ($request->get('billingCategory')) {
                foreach ($billingCategories as $key => $billingCategory) {
                    $mon = ($billingCategory->monthly_charge == BillingCategory::MONTHLY_CHARGE) ? $months : 1;

                    $billingData[] = [
                        'billing_id'           => $billingModel->id,
                        'billing_category_id'  => $billingCategory->id,
                        'total_month_paid'     => $mon,
                        'billing_amount'       => $billingCategory->default_price,
                        'sub_total'            => $billingCategory->default_price * $mon,
                    ];
                }
            }

            BillingDetail::insert($billingData);

            DB::commit();
            return back()->with('success', 'Bill paid successfully!');
        } catch (\Exception $e) {
            DB::rollback();
            return back()->with('error', 'Could not create billing category at the moment!' . $e);
        }
    }
}
