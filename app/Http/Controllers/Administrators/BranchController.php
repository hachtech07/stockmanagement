<?php

namespace App\Http\Controllers\Administrators;

use App\Models\Branch;
use Illuminate\Http\Request;
use Yajra\DataTables\DataTables;
use App\Interfaces\BranchInterface;
use App\Http\Controllers\Controller;
use App\Http\Requests\BranchStoreRequest;
use Exception;
use Illuminate\Support\Facades\DB;


class BranchController extends Controller
{
    private $branchInterface;
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct(BranchInterface $branchInterface)
    {
        $this->branchInterface = $branchInterface;
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        $branches = $this->branchInterface->getAllBranch();
        return view('administrators.branch.index', ['branches' => $branches]);
    }

    public function getbranchdata(Request $request)
    {
        if ($request->ajax()) {
            $data = Branch::getBranches();
            return Datatables::of($data)
                ->addIndexColumn()
                ->addColumn('status', function ($row) {
                    $row->status == Branch::ACTIVE ? $color = "text-success" : $color = "text-warning";
                    $status = '<span class="icon icon-circle s-12  mr-2 ' . $color . '"></span>' . $row->status . '';
                    return $status;
                })
                ->editColumn('action', function ($row) {
                    return '<a class="edit btn btn-primary btn-sm" href="' . route('branch.update', $row->id) . '">Edit</a>';
                })
                ->rawColumns(['action', 'status'])
                ->make(true);
        }
    }

    public function create(BranchStoreRequest $request)
    {
        if($request->isMethod('POST'))
        {
            
            try{
                DB::beginTransaction();
                $save = $this->branchInterface->create($request->all());

            if ($save) {
                DB::commit();
                return back()->with('success', 'Branch created successfully!');
            }
            return back()->with('error', 'Could not create branch at the moment!');
        } catch (Exception $e) {
            DB::rollback();
            return back()->with('error', "cannot create the branch");
        }
        }

        return view('administrators.branch.create');

    }

    public function update(BranchStoreRequest $request, $id)
    {
        $branchDetail =  $this->branchInterface->findById($id);
        if(!$branchDetail){
            return back()->with('error', 'Branch could not found!');
        }

        if ($request->isMethod('PUT')) {
            try {
                DB::beginTransaction();
            $save = $this->branchInterface->create($request->all(), $id);
            if ($save) {
                DB::commit();
                return back()->with('success', 'Branch updated successfully!');
            }
            return back()->with('error', 'Could not update branch at the moment!');
        }catch(Exception $e){
            DB::rollback();
            return back()->with('error', "cannot update the branch");
        }
        }

        return view('administrators.branch.update', ['branchDetail' => $branchDetail]);
    }
}
