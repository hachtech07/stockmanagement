<?php

namespace App\Http\Controllers\Administrators;

use App\Exceptions\CustomException;
use App\Exports\CustomerDataExport;
use App\Http\Controllers\Controller;
use App\Imports\CustomersImport;
use Illuminate\Http\Request;
use App\Models\Customer;
use App\Http\Requests\CustomerStoreRequest;
use App\Interfaces\BillingInterface;
use App\Interfaces\CustomerInterface;
use Maatwebsite\Excel\Excel;
use Illuminate\Support\Facades\Auth;
use App\Interfaces\BranchInterface;
use App\Interfaces\CustomerCategoryInterface;
use App\Models\Fiscal_year;
use App\Traits\AuthTrait;
use App\Traits\DateConveterTrait;
use App\Traits\FiscalYear;
use Illuminate\Support\Facades\DB;
use Exception;

class CustomerController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    use AuthTrait, DateConveterTrait, FiscalYear;
    private $excel;
    private $customerInterface;
    private $customerCategoryInterface;
    private $branchInterface;
    private $billingInterface;

    public function __construct(Excel $excel, CustomerInterface $customerInterface, BranchInterface $branchInterface, CustomerCategoryInterface $customerCategoryInterface, BillingInterface $billingInterface)
    {
        $this->excel = $excel;
        $this->customerInterface = $customerInterface;
        $this->customerCategoryInterface = $customerCategoryInterface;
        $this->branchInterface = $branchInterface;
        $this->billingInterface = $billingInterface;
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        $customers = $this->customerInterface->getAllCustomer();
        return view('administrators.customer.index', ['customers' => $customers]);
    }

    public function getcustomerdata(Request $request)
    {
        if ($request->isMethod('post') || $request->isMethod('get')) {
            try {
                $branches         = $this->branchInterface->getAllBranch();
                $data = $this->customerInterface->fetchCustomerData($request->all());
                $data['branches'] = $branches;
                return view('administrators.customer.index', $data);
            } catch (Exception $e) {
                return back()->with('error', 'Could not fetch at the moment!' . $e);
            }
        }
        return view('administrators.customer.index');
    }

    public function export_customer_data(Request $request)
    {
        ini_set('memory_limit', '-1');
        return $this->excel->download(new CustomerDataExport($request), 'customers.xlsx');
    }

    public function create(CustomerStoreRequest $request)
    {
        $data = $this->customerInterface->createData();
        if ($request->isMethod('post')) {
            try {
                DB::beginTransaction();
                $customerModel = $this->customerInterface->create($request->all(), $this->authId());

                $billingModel = $this->billingInterface->create($request->all(), $customerModel->id, $customerModel->due_amount, $customerModel->advance_amount);

                DB::commit();
                return redirect()->route('customer')->with('success', 'Customer created successfully!');
            } catch (CustomException $exception) {
                return back()->with('error', $exception->getMessage())->withInput();
            } catch (Exception $e) {
                DB::rollback();
                return back()->with('error', 'Could not save at the moment!' . $e);
            }
        }
        return view('administrators.customer.create', $data);
    }

    public function update(CustomerStoreRequest $request, $id)
    {
        $data = $this->customerInterface->updateData($id);
        if (!$data['customerDetail']) {
            return back()->with('error', 'Customer Couldnot found');
        }
        if ($request->isMethod('post')) {
            try {
                DB::beginTransaction();
                $customerUpdate = $this->customerInterface->update($request->all(), $id);
                DB::commit();
                return redirect()->route('customer')->with('success', 'Customer successfully updated!');
            } catch (CustomException $exception) {
                return back()->with('error', $exception->getMessage())->withInput();
            } catch (Exception $e) {
                DB::rollback();
                return back()->with('error', 'Could not create at the moment!' . $e);
            }
        }

        return view('administrators.customer.update', $data);
    }

    public function import_bck(Request $request)
    {
        if ($request->isMethod('post')) {
            ini_set('memory_limit', '-1');
            try {
                DB::beginTransaction();
                $importData = $this->excel->toArray(new CustomersImport, $request->file('file'));
                $chunks = array_chunk($importData[0], 1);
                foreach ($chunks as $chunk) {
                    Customer::insert($chunk);
                }

                DB::commit();
                return back()->with('success', 'Customer successfully imported.');
            } catch (Exception $e) {
                DB::rollback();
                return back()->with('error', 'Could not import data. Pleasee check your data.' . $e);
            }
        }
        return view('administrators.customer.import');
    }

    public function import(Request $request)
    {
        ini_set('memory_limit', '-1');
        if ($request->isMethod('post')) {
            $validatedData = $request->validate([
                'file' => 'required|mimes:xls,xlsx',
            ]);

            try {
                DB::beginTransaction();
                $this->excel->import(new CustomersImport(), $request->file('file'));
                DB::commit();
                return back()->with('success', 'Customer imported successfully');
            } catch (Exception $e) {
                DB::rollback();
                return back()->with('error', 'Could not import customers. Please check your data.' . $e);
            }
        }

        return view('administrators.customer.import');
    }

    public function download()
    {
        $file_path_full = base_path() . "/public/assets/files/sample.xlsx";
        $file_path      = pathinfo(base_path() . "/public/assets/files/sample.xlsx");
        $basename       = $file_path['basename'];
        $path           = $file_path['dirname'];

        return response()->download($file_path_full, $basename, ['Content-Type' => 'application/force-download']);
    }

    public function transactionHistory($id, Request $request)
    {

        if ($request->isMethod('post') || $request->isMethod('get')) {
            try {
                $customer = $this->customerInterface->transactionHistory($id);
                $mybills = $this->billingInterface->getAllMyBillIndex($id, $request->all());
                $fiscal_years = Fiscal_year::all();
                $data = [
                    'customer' => $customer,
                    // 'billings'=>$mybills,
                    'fiscal_years' => $fiscal_years
                ];
                return view('administrators.customer.transaction-history', array_merge($data, $mybills));
            } catch (Exception $e) {
                return back()->with('error', 'Could not fetch at the moment!');
            }
        }
    }

    public function search(Request $request, $searchParam = NULL)
    {

        if ($request->isMethod('post') || $request->isMethod('get')) {
            $this->validate($request, [
                'search_param'  => 'min:3'
            ]);
            $data = $this->customerInterface->search($request->get('search_param'));
        }
        return view('administrators.customer.search', $data);
    }

    public function lastCustomerNumber($id)
    {
        try {
            $customer = $this->customerInterface->lastCustomerNumber($id);
            return json_encode($customer);
        } catch (Exception $e) {
            return back()->with('error', 'Could not fetch at the moment!');
        }
    }
}
