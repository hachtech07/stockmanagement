<?php

namespace App\Http\Controllers\Administrators;

use App\Http\Controllers\Controller;
use App\Http\Requests\MobileImageRequest;
use App\Models\MobileImage;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\File;

class MobileImageController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
    }

    public function index()
    {
        $data = MobileImage::get();
        return view('administrators.mobile-image.index', ['images' => $data]);
    }

    public function create(MobileImageRequest $request)
    {
        if ($request->isMethod('post')) {

            $image = $request->image->getClientOriginalName();
            $imageName = time() . $image;

            $path = $request->image->move(public_path('images'), $imageName);

            $mobileImage = new \App\Models\MobileImage;

            $mobileImage->image_name = $request->get('image_name');
            $mobileImage->image_path = $path;
            $mobileImage->image     = $imageName;
            $mobileImage->status = $request->get('status');

            $mobileImage->save();

            return back()->with('success', 'Mobile Image Created Successfully');
        }

        return view('administrators.mobile-image.create');
    }

    public function update(Request $request, $id)
    {
        $imageModel = MobileImage::find($id);

        if ($request->isMethod('post')) {
            $this->validate($request, [
                'image_name'   =>   'required|string',
                'image'        =>   'image|mimes:jpeg,png,jpg,gif|max:2048',
                'status'       =>   'required'
            ]);

            $imageModel->image_name = $request->get('image_name');
            if (!$request->image == null) {
                if (File::exists($imageModel->image_path)) {
                    File::delete($imageModel->image_path);
                }
                $image = $request->image->getClientOriginalName();
                $imageName = time() . $image;
                $path = $request->image->move(public_path('images'), $imageName);
                $imageModel->image_path = $path;
                $imageModel->image = $imageName;
            }
            $imageModel->status = $request->get('status');
            $imageModel->save();

            return back()->with('success', 'Mobile Image Updated Successfully!');
        }

        return view('administrators.mobile-image.update', ['image' => $imageModel]);
    }

    public function change_status(Request $request)
    {
        $imageModel = MobileImage::find($request->get('id'));

        if ($imageModel->status == "active") {
            $imageModel->status = "inactive";
            $msg_type = "info";
            $message = "Image Deactivated Successfully";
        } else {
            $imageModel->status = "active";
            $msg_type = "success";
            $message = "Image Activated Successfully";
        }
        $imageModel->save();


        return back()->with($msg_type, $message);
    }
}
