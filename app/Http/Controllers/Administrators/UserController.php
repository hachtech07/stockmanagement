<?php

namespace App\Http\Controllers\Administrators;

use App\Exceptions\CustomException;
use App\Http\Controllers\Controller;
use App\Http\Requests\ChangeUserPassword;
use App\Http\Requests\ModelHasRoleRequest;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use App\Models\User;
use App\Http\Requests\UserStoreRequest;
use App\Interfaces\ModelHasRoleInterface;
use App\Interfaces\UserRepositoryInterface;
use App\Models\Branch;
use  App\Models\Role;
use Exception;
use Illuminate\Support\Facades\DB;


class UserController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    private $userInterface;
    private $modelHasRoleInterface;
    public function __construct(
        UserRepositoryInterface $userInterface,
        ModelHasRoleInterface $modelHasRoleInterface
    ) {
        $this->userInterface = $userInterface;
        $this->modelHasRoleInterface = $modelHasRoleInterface;
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index(Request $request)
    {
        $users = $this->userInterface->getUserInfo();

        return view('administrators.user.index', ['users' => $users]);
    }

    public function create(Request $request)
    {
        $branches   = Branch::getActiveBranches();
        $roles      = Role::getRoles();
        return view('administrators.user.create', ['branches' => $branches, 'roles' => $roles]);
    }

    public function store(UserStoreRequest $userRequest, ModelHasRoleRequest $modelRequest)
    {

        try {
            DB::beginTransaction();
            $user           = $this->userInterface->create($userRequest->all());
            $modelRequest   = ['model_id' => $user->id, 'model_type' => 'App\models\User', 'role_id' => $modelRequest->get('role_id')];
            $modelHasRole   = $this->modelHasRoleInterface->create($modelRequest);
            DB::commit();

            return back()->with('success', 'User created successfully!');
        } catch (Exception $e) {
            DB::rollback();
            return back()->with('error', "cannot create the user");
        }
    }
    public function searchUser(Request $request)
    {
        $searchParam = $request->get('param');
        $users = User::query();
        $users = $users->where('name', 'like', '%' . $searchParam . '%')->get();
        return json_encode($users);
    }

    public function update(UserStoreRequest $userRequest, $id)
    {
        $userModel = $this->userInterface->findById($id);
        if (!$userModel) {
            return back()->with('error', "User canot found");
        }
        $branches   = Branch::getBranches();

        if ($userRequest->isMethod('PUT')) {


            try {
                DB::beginTransaction();
                $user = $this->userInterface->create($userRequest->all(), $id);
                if ($user) {
                    DB::commit();
                    return back()->with('success', 'User created successfully!');
                }
            } catch (CustomException $exception) {
                return back()->with('error', $exception->getMessage());
            } catch (Exception $e) {
                DB::rollback();
                return back()->with('error', "cannot update the user");
            }
        }

        return view('administrators.user.update', ['userModel' => $userModel, 'branches' => $branches]);
    }

    public function updatePass(ChangeUserPassword $request, $id)
    {
        $userModel = $this->userInterface->findById($id);

        if ($request->isMethod('post')) {
            try{
                DB::beginTransaction();

                $changePassword = $this->userInterface->updateUserPassword($request->all(),$id);
                if ($changePassword) {
                    DB::commit();
                    return back()->with('success', 'Password changed successfully!');
                }
            }catch(Exception $e){
                DB::rollback();
                return back()->with('error', "cannot change the password");
            }

            return back()->with('success', 'User password changed successfully!');
        }
        return view('administrators.user.changepassword', ['user' => $userModel]);
    }
}
