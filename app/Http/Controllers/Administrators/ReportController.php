<?php

namespace App\Http\Controllers\Administrators;

use PDF;
use Exception;
use App\Traits\AuthTrait;
use Illuminate\Http\Request;
use Maatwebsite\Excel\Excel;
use App\Exports\DueReportExport;
use Yajra\DataTables\DataTables;
use App\Traits\DateConveterTrait;
use App\Interfaces\ReportInterface;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use App\Exports\DiscountReportExport;
use App\Exports\BillReturnReportExport;
use App\Exports\CollectionReportExport;
use App\Exports\CategoryWiseReportExport;
use App\Interfaces\BillingInterface;

class ReportController extends Controller
{
    use DateConveterTrait, AuthTrait;
    private $excel;
    private $reportInterface;
    private $billingInterface;
    public function __construct(Excel $excel, ReportInterface $reportInterface, BillingInterface $billingInterface)
    {
        $this->excel = $excel;
        $this->reportInterface = $reportInterface;
        $this->billingInterface = $billingInterface;
    }

    public function discount_report(Request $request)
    {

        if ($request->isMethod('post') || $request->isMethod('get')) {
            try {
                $data = $this->reportInterface->getDiscountReportIndex($request->all());
                return view('administrators.reports.discount-report', $data);
            } catch (Exception $e) {
                return back()->with('error', 'Could not download at the moment!');
            }
        }
    }

    public function export_discount(Request $request)
    {
        try {
            return $this->excel->download(new DiscountReportExport($request), 'discount-report.xlsx');
        } catch (Exception $e) {
            return back()->with('error', 'Could not download at the moment!' . $e);
        }
    }

    public function export_collection(Request $request)
    {
        try {
            return $this->excel->download(new CollectionReportExport($request), 'collection-report.xlsx');
        } catch (Exception $e) {
            return back()->with('error', 'Could not download at the moment!');
        }
    }

    public function export_category_wise(Request $request)
    {
        try {
            return $this->excel->download(new CategoryWiseReportExport($request), 'category-wise-report.xlsx');
        } catch (Exception $e) {
            return back()->with('error', 'Could not export at the moment!');
        }
    }

    public function export_due(Request $request)
    {
        try {
            return $this->excel->download(new DueReportExport($request), 'due-report.xlsx');
        } catch (Exception $e) {
            return back()->with('error', 'Could not export at the moment!');
        }
    }

    public function export()
    {
        try {
            $fileName = $this->reportInterface->exportAll();
            return redirect(url('/reports/discount-report') . "/export/" . $fileName);
        } catch (Exception $e) {
            return back()->with('error', 'Could not export at the moment!');
        }
    }

    public function due_report(Request $request)
    {
        if ($request->isMethod('post') || $request->isMethod('get')) {
            try {
                $data = $this->reportInterface->getDueReportIndex($request->all());
                return view('administrators.reports.due-report', $data);
            } catch (Exception $e) {
                return back()->with('error', 'Could not fetch at the moment!');
            }
        }
    }

    public function collection_report(Request $request)
    {
        if ($request->isMethod('post') || $request->isMethod('get')) {
            try {
                $data = $this->reportInterface->getCollectionReportIndex($request->all());
                return view('administrators.reports.collection-report', $data);
            } catch (Exception $e) {
                return back()->with('error', 'Could not fetch at the moment!');
            }
        }
    }

    public function advance_report(Request $request)
    {
        if ($request->ajax()) {

            $data = $this->reportInterface->getAdvanceReport();

            if ($this->authusertype() == 'branch admin') {
                $data->where('branch_id', '=', $this->authbranchid());
            }

            if (($request->get('from') != "") && ($request->get('to') != "")) {
                $data = $data->whereBetween('last_bill_date', [$request->get('from'), date('Y-m-d', strtotime($request->get('to') . ' +1 day'))]);
            }

            $data = $data->get();
            return Datatables::of($data)
                ->addIndexColumn()
                ->make(true);
        }

        return view('administrators.reports.advance-report');
    }

    public function categories_wise_report(Request $request)
    {
        if ($request->isMethod('post') || $request->isMethod('get')) {
            try {
                $data = $this->reportInterface->getCategoryWiseReportIndex($request->all());
                return view('administrators.reports.category-wise-report', $data);
            } catch (Exception $e) {
                return back()->with('error', 'Could not fetch at the moment!');
            }
        }
    }

    public function english_to_nepali($yy, $mm, $dd)
    {
        return $this->eng_to_nep($yy, $mm, $dd);
    }

    public function bill_return_report(Request $request)
    {
        if ($request->isMethod('post') || $request->isMethod('get')) {
            try {
                $data = $this->reportInterface->getBillReturnReportIndex($request->all());
                return view('administrators.reports.bill-return-report', $data);
            } catch (Exception $e) {
                return back()->with('error', 'Could not fetch at the moment!');
            }
        }
    }

    public function export_bill_return(Request $request)
    {
        try {
            return $this->excel->download(new BillReturnReportExport($request), 'bill-return-report.xlsx');
        } catch (Exception $e) {
            return back()->with('error', 'Could not export at the moment!');
        }
    }

    public function materialized_view_report(Request $request)
    {
        if ($request->isMethod('post') || $request->isMethod('get')) {
            try {
                $data = $this->reportInterface->getMaterializedViewReportIndex($request->all());
                return view('administrators.reports.materialize-report', $data);
            } catch (Exception $e) {
                return back()->with('error', 'Could not fetch at the moment!');
            }
        }
    }

    public function sales_report(Request $request)
    {
        if ($request->isMethod('post') || $request->isMethod('get')) {
            try {
                $data = $this->reportInterface->getMaterializedViewReportIndex($request->all());
                return view('administrators.reports.sales-report', $data);
            } catch (Exception $e) {
                return back()->with('error', 'Could not fetch at the moment!');
            }
        }
    }

    public function printBillingDetail(Request $request)
    {
        if ($request->isMethod('post') || $request->isMethod('get')) {
            try {
                $data = $this->reportInterface->printMaterializedReport($request->all());
                $pdf = PDF::loadView('administrators.reports.print-ird-detail-materialized', $data);
                $pdf->setPaper('A2', 'landscape');

                return $pdf->download('details.pdf');
            } catch (Exception $e) {
                return back()->with('error', 'Could print at the moment!');
            }
        }
    }

    public function printSalesReport(Request $request)
    {
        if ($request->isMethod('post') || $request->isMethod('get')) {
            try {
                $data = $this->reportInterface->printMaterializedReport($request->all());
                $pdf = PDF::loadView('administrators.reports.print-ird-detail', $data);
                $pdf->setPaper('A2', 'landscape');

                return $pdf->download('details.pdf');
            } catch (Exception $e) {
                return back()->with('error', 'Could print at the moment!');
            }
        }
    }


    public function billPrintDetails($id)
    {
        $bill = $this->billingInterface->getMaterializedBillDetail($id);
    }
}
