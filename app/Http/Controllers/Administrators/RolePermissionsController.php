<?php

namespace App\Http\Controllers\Administrators;

use App\Http\Controllers\Controller;
use App\Traits\AuthTrait;
use Illuminate\Http\Request;
use Illuminate\Support\Collection;
use Spatie\Permission\Models\Role;
use Spatie\Permission\Models\Permission;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Exception;

class RolePermissionsController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    use AuthTrait;
    private $role;
    private $permission;

    public function __construct(Permission $permission, Role $role)
    {
        $this->permission = $permission;
        $this->role = $role;
    }

    public function index()
    {
        $roles = $this->role->getRoles();
        return view('administrators.role-permissions.index', ['roles' => $roles]);
    }

    public function addPermissions(Request $request, $id)
    {
        $permissions = $this->permission->select('id', 'name')->orderBy('id', 'asc')->get();
        return view('administrators.role-permissions.add_permissions', ['permissions' => $permissions, 'roleId' => $id]);
    }

    public function create(Request $request)
    {
        $permissionData = [];
        $permissions = $_POST['permission_id'];
        $user_id = $this->authId();
        foreach ($permissions as $key => $permission) {
            $permissionData[] = [
                'permission_id' => $key,
                'model_type'    => 'App\Models\User',
                'model_id'      => $user_id
            ];
        }

        DB::beginTransaction();
        try {
            DB::delete('delete from model_has_permissions where model_id =' . $this->authId());
            DB::table('model_has_permissions')->insert($permissionData);

            DB::commit();
        } catch (Exception $e) {
            DB::rollback();
            var_dump($e);
            exit;
        }
    }
}
