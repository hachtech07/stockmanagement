<?php

namespace App\Http\Controllers\Administrators;

use App\Http\Controllers\Controller;
use App\Http\Requests\RoleRequest;
use App\Interfaces\RolePermissionInterface;
use App\Models\Permission;
use Illuminate\Http\Request;
use App\Models\Role;
use Exception;
use Illuminate\Support\Facades\DB;

class RoleController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    private $rolePermissionInterface;
    public function __construct(RolePermissionInterface $rolePermissionInterface)
    {
        $this->rolePermissionInterface = $rolePermissionInterface;
    }

    public function index()
    {
        $roles = Role::getRoles();
        return view('administrators.role.index', ['roles' => $roles]);
    }

    public function create()
    {
        return view('administrators.role.create')->with('role', new \App\Models\Role);
    }

    public function store(Request $request)
    {
        $request->validate([
            'name'  => 'required|unique:roles',
        ]);

        try {
            $roleModel = new Role;

            $roleModel->name       = $request->get('name');
            $roleModel->guard_name = Role::GUARD;

            $roleModel->save();

            return back()->with('success', 'Role created successfully!');
        } catch (Exception $e) {
            return back()->with('error', 'Could not create role.<br/>' . $e);
        }
    }

    public function update(RoleRequest $request, $id)
    {
        $roleModel =  $this->rolePermissionInterface->findRoleById($id);
        if (!$roleModel) {
            return back()->with('error', 'Could not found');
        }
        if ($request->isMethod('post')) {

            try {
                DB::beginTransaction();
                $this->rolePermissionInterface->update($request->all(), $roleModel->id);
                DB::commit();
                return back()->with('success', 'Role created successfully!')->with('role', Role::find($id));
            } catch (\Exception $e) {
                DB::rollback();
                return back()->with('error', 'Could not update role.' . $e);
            }
        }

        return view('administrators.role.create')->with('role', $roleModel);
    }

    public function addRolePermission(Request $request, $role_id)
    {
        $permissions = Permission::getRolePermission($role_id);

        if ($request->isMethod('post')) {


            try {
                DB::beginTransaction();

                $this->rolePermissionInterface->add($request->all());
                DB::commit();
                return back()->with('success', 'Permission successfully added to a role.');
            } catch (Exception $e) {
                DB::rollback();

                return back()->with('error', 'Could not add permission.' . $e);
            }
        } else {
            return view('administrators.role.add_role_permission', ['permissions' => $permissions, 'role_id' => $role_id]);
        }
    }

    public function addPermissions(Request $request, $user_id)
    {
        $permissions = Permission::getUserPermission($user_id);
        if ($request->isMethod('post')) {
            $permissionData = [];

            DB::beginTransaction();
            try {
                $permissions = $request->get('permission_id');
                $user_id     = $request->get('user_id');

                foreach ($permissions as $key => $permission) {
                    $permissionData[] = [
                        'permission_id' => $key,
                        'model_type'    => 'App\Models\User',
                        'model_id'      => $user_id
                    ];
                }

                DB::delete('delete from model_has_permissions where model_id =' . $user_id);
                DB::table('model_has_permissions')->insert($permissionData);

                DB::commit();

                return back()->with('success', 'Permission successfully added to a role.');
            } catch (Exception $e) {
                DB::rollback();

                return back()->with('error', 'Could not add permission.' . $e);
            }
        } else {
            return view('administrators.role.add_permissions', ['permissions' => $permissions, 'user_id' => $user_id]);
        }
    }
}
