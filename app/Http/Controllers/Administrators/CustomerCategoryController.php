<?php

namespace App\Http\Controllers\Administrators;

use App\Http\Controllers\Controller;
use App\Http\Requests\CustomerCategoryStoreRequest;
use App\Interfaces\CustomerCategoryInterface;

class CustomerCategoryController extends Controller
{
    private $customerCategoryInterface;
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct(CustomerCategoryInterface $customerCategoryInterface)
    {
        $this->customerCategoryInterface = $customerCategoryInterface;
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */


    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        $customerCategories = $this->customerCategoryInterface->getCustomerCategory();
        return view('administrators.customer-category.index', ['customerCategories' => $customerCategories]);
    }

    public function create(CustomerCategoryStoreRequest $request)
    {
        if ($request->isMethod('post')) {

            $save = $this->customerCategoryInterface->create($request->all());

            if ($save) {
                return back()->with('success', 'Customer category created successfully!');
            }
            return back()->with('error', 'Could not create customer category at the moment!');
        }
        return view('administrators.customer-category.create');
    }

    public function update(CustomerCategoryStoreRequest $request, $id)
    {
        $customerCategoryDetail =$this->customerCategoryInterface->getCustomerCategoryById($id);

        if ($request->isMethod('post')) {

            $save = $this->customerCategoryInterface->create($request->all(), $id);

            if ($save) {
                return back()->with('success', 'Branch updated successfully!');
            }
            return back()->with('error', 'Could not update branch at the moment!');
        }

        return view('administrators.customer-category.update', ['customerCategoryDetail' => $customerCategoryDetail]);
    }
}
