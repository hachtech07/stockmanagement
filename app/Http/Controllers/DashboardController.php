<?php

namespace App\Http\Controllers;

use App\Interfaces\DashboardInterface;
use App\Models\Customer;
use App\Models\User;
use App\Traits\DateConveterTrait;
use App\Models\Branch;
use App\Traits\AuthTrait;
use Yajra\DataTables\DataTables;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Config;

class DashboardController extends Controller
{
	use DateConveterTrait, AuthTrait;
	private $dashboardInterface;
	private $user;
	private $customer;
	private $branch;

	public function __construct(DashboardInterface $dashboardInterface, Branch $branch, User $user, Customer $customer)
	{
		$this->dashboardInterface = $dashboardInterface;
		$this->user = $user;
		$this->branch = $branch;
		$this->customer = $customer;
	}

	public function index()
	{
		return view('dashboard/index');
	}

	public function get_my_user_by_branch()
	{
		$users = $this->user->where('branch_id', $this->authbranchid())
			->where('id', '!=', $this->authid())
			->get();

		return $users;
	}

	public function get_my_branch_name()
	{
		$branch		 	= $this->branch->findOrFail($this->authbranchid());
		$branch_name 	= $branch->branch_title;
		return $branch_name;
	}

	public function get_user_by_branch($id)
	{
		$users = $this->user->where('branch_id', $id)->where('user_type', 'collector')->get();
		return json_encode($users);
	}

	public function get_customers_by_branch($id)
	{
		$customers = $this->customer->get_customer_by_branch_id($id);
		return json_encode($customers);
	}

	public function get_all_branch_name()
	{
		$branch = $this->branch->where('id', '>', Config::get('constant.admin_id'))->get();
		return $branch;
	}

	public function dashboard_report(Request $request)
	{
		if ($request->ajax()) {
			$data = $this->dashboardInterface->dashboard_report($request->all());
			return Datatables::of($data)
				->addIndexColumn()
				->make(true);
		}
		return view('dashboard.index');
	}

	public function total_customers()
	{
		$branch_id = $this->authbranchid();
		$data = $this->customer->where(['status' => 'active', 'branch_id' => $branch_id])
			->get()
			->count();
		return $data;
	}

	public function total_collectors()
	{
		$branch_id = $this->authbranchid();
		$data = $this->user->where(['user_type' => 'collector', 'branch_id' => $branch_id])
			->get()
			->count();
		return $data;
	}

	public function get_due_and_collected($year = null)
	{
		return $this->dashboardInterface->get_due_and_collected($year);
	}

	public function get_category_wise_total()
	{
		return $this->dashboardInterface->get_category_wise_total();
	}

	public function get_month_wise_bill_generated($year = null)
	{
		return $this->dashboardInterface->get_month_wise_bill_generated($year);
	}

	// public function total_bill_amount_returned()
	// {
	// 	return $this->dashboardInterface->total_bill_returned();
	// }
}
