<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\ProductSubCategoryRequest;
use App\Interfaces\ProductCategoryInterface;
use App\Interfaces\ProductSubCategoryInterface;
use Illuminate\Http\Request;
use Exception;
use Illuminate\Support\Facades\DB;

class ProductSubCategoryController extends Controller
{

    private $productSubCategoryInterface,$productCategoryInterface;
    public function __construct(ProductSubCategoryInterface $productSubCategoryInterface,ProductCategoryInterface $productCategoryInterface)
    {
        $this->productSubCategoryInterface = $productSubCategoryInterface;
        $this->productCategoryInterface = $productCategoryInterface;
    }

    public function subCategories($id){
        $subcategories = $this->productSubCategoryInterface->listSubCategory($id);
        return view('admin.category.list-subcategory',['subcategories'=>$subcategories]);
    }
    public function createSubCategory(ProductSubCategoryRequest $request,$id){
        $productCategory = $this->productCategoryInterface->getProductCategoryById($id);
        if(!$productCategory){
            return back()->with('error', "Product category can't found");
        }
        if ($request->isMethod('post')) {

            try{
            DB::beginTransaction();
            $category = $this->productSubCategoryInterface->createSubCategory($request->all());
            DB::commit();
            return back()->with('success', 'Product Subcategory successfully! created');

        }catch(Exception $e){
            DB::rollback();
            return back()->with('error', "Fail to create product Subcategory");
        }

        }
        return view('admin.category.create-subcategory',['productCategory'=>$productCategory]);
    }
    public function getSubCategory($id){
        $subcategories = $this->productSubCategoryInterface->listSubCategory($id);
        return json_encode($subcategories);
    }
}