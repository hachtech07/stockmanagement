<?php

namespace App\Http\Controllers\Admin;

use App\Exceptions\CustomException;
use App\Exports\StockReport;
use App\Http\Controllers\Controller;
use App\Http\Requests\ProductRequest;
use App\Http\Requests\RoleRequest;
use App\Imports\ProductImport;
use App\Interfaces\ProductCategoryInterface;
use App\Interfaces\ProductInterface;
use App\Interfaces\RolePermissionInterface;
use App\Models\Permission;
use Illuminate\Http\Request;
use App\Models\Role;
use App\Traits\AuthTrait;
use App\Traits\DateConveterTrait;
use Exception;
use Illuminate\Support\Facades\DB;
use Maatwebsite\Excel\Excel;

class ProductController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    use DateConveterTrait, AuthTrait;
    private $productCategoryInterface, $productInterface,$excel;
    public function __construct(ProductCategoryInterface $productCategoryInterface, ProductInterface $productInterface,Excel $excel)
    {
        $this->productCategoryInterface = $productCategoryInterface;
        $this->productInterface         = $productInterface;
        $this->excel = $excel;
    }

    public function index(Request $request)
    {
        if ($request->isMethod('post') || $request->isMethod('get')) {
            // try {
            $purchases = $this->productInterface->findAll($request->all());
            return view('admin.product.index', $purchases);
            // } catch (Exception $e) {
            //     return back()->with('error', 'Could not fetch at the moment!');
            // }
        }
    }
    public function english_to_nepali($yy, $mm, $dd)
    {
        return $this->eng_to_nep($yy, $mm, $dd);
    }
    public function create(ProductRequest $request)
    {

        $categories = $this->productCategoryInterface->getProductCategory();
        if ($request->isMethod('post')) {
            try {
                DB::beginTransaction();
                $this->productInterface->create($request->all());
                DB::commit();
                return back()->with('success', 'Product added successfully!');
            } catch (CustomException $ce) {
                DB::rollback();
                return back()->with('error', $ce->getMessage());
            } catch (Exception $e) {
                DB::rollback();
                return back()->with('error',$e);
            }
        }
        return view('admin.product.create', ['categories' => $categories]);
    }
    public function update(ProductRequest $request, $id)
    {
        $product = $this->productInterface->findById($id);
        if (!$product) {
            return back()->with('error', "Product canot found");
        }

        if ($request->isMethod('put')) {
            try {
                DB::beginTransaction();
                $this->productInterface->updateStock($request->all(), $id);
                DB::commit();
                return back()->with('success', 'Stock updated successfully!');
            } catch (CustomException $ce) {
                DB::rollback();
                return back()->with('error', $ce->getMessage());
            } catch (Exception $e) {
                DB::rollback();
                return back()->with('error', "Product fail to add", $e);
            }
        }
        return view('admin.product.update-stock', ['product' => $product]);
    }
    public function export_stock(Request $request)
    {
        try {
            return $this->excel->download(new StockReport($request), 'Stock-report.xlsx');
        } catch (Exception $e) {
            return back()->with('error', 'Could not download at the moment!');
        }
    }

    public function import(Request $request)
    {
        ini_set('memory_limit', '-1');
        if ($request->isMethod('post')) {
            $validatedData = $request->validate([
                'file' => 'required|mimes:xls,xlsx',
            ]);

            try {
                DB::beginTransaction();
                $this->excel->import(new ProductImport(), $request->file('file'));
                DB::commit();
                return back()->with('success', 'Product imported successfully');
            } catch (Exception $e) {
                DB::rollback();
                return back()->with('error', 'Could not import product. Please check your excel data.' . $e);
            }
        }

        return view('admin.product.import');
    }

    public function download()
    {
        $file_path_full = base_path() . "/public/assets/files/sample.xlsx";
        $file_path      = pathinfo(base_path() . "/public/assets/files/sample.xlsx");
        $basename       = $file_path['basename'];
        $path           = $file_path['dirname'];

        return response()->download($file_path_full, $basename, ['Content-Type' => 'application/force-download']);
    }
}
