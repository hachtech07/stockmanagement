<?php

namespace App\Http\Controllers\Admin;

use App\Exceptions\CustomException;
use App\Http\Controllers\Controller;
use App\Http\Requests\ChangePasswordRequest;
use App\Interfaces\ProfileInterface;
use App\Interfaces\UserRepositoryInterface;
use Illuminate\Support\Facades\DB;

class ProfileController extends Controller
{


    /**
     * @var UserRepositoryInterface
     */
    private $userInterface;
    /**
     * @var ProfileInterface
     */
    private $profileInterface;

    public function __construct(UserRepositoryInterface $userInterface, ProfileInterface $profileInterface)
    {
        $this->userInterface = $userInterface;
        $this->profileInterface = $profileInterface;
    }

    public function index()
    {
        return view('admin.profile.index');
    }

    public function change_pass(ChangePasswordRequest $request)
    {
        $user = $this->userInterface->authenticatedUser();
        $userDetail = $this->userInterface->findById($user->id);
        if ($request->isMethod('post')) {

            DB::beginTransaction();
            try {
                $this->profileInterface->changeUserPassword($request->all(), $user->id);
                DB::commit();
                return back()->with('success', 'Password changed successfully!');

            } catch (CustomException $exception) {
                return back()->with('error', $exception->getMessage());
            } catch (\Exception $e) {
                DB::rollback();
                return back()->with('error', "cannot change the password");
            }
        }
        return view('admin.profile.changeuserpassword', ['userDetail' => $userDetail]);
    }
}