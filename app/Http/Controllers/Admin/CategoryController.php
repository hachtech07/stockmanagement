<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\RoleRequest;
use App\Interfaces\RolePermissionInterface;
use App\Models\Permission;
use Illuminate\Http\Request;
use App\Models\Role;
use Exception;
use Illuminate\Support\Facades\DB;

class CategoryController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    // private $rolePermissionInterface;
    public function __construct()
    {
    }

    public function index()
    {
        
        return view('admin.category.index');
    }
    public function create(Request $request){
        if ($request->isMethod('post')) {


        }
        return view('admin.category.create');
    }
    public function update(Request $request){
        if ($request->isMethod('put')) {


        }
        return view('admin.category.update');
    }
}