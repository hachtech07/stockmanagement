<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\ProductCategoryRequest;
use App\Http\Requests\RoleRequest;
use App\Interfaces\ProductCategoryInterface;
use App\Interfaces\RolePermissionInterface;
use App\Models\Permission;
use App\Models\ProductCategory;
use Illuminate\Http\Request;
use App\Models\Role;
use Exception;
use Illuminate\Support\Facades\DB;

class ProductCategoryController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    private $productCategoryInterface;
    public function __construct(ProductCategoryInterface $productCategoryInterface)
    {
        $this->productCategoryInterface = $productCategoryInterface;
    }

    public function index()
    {
        $productCategories = $this->productCategoryInterface->getProductCategory();
        
        return view('admin.category.index',['productCategories'=>$productCategories]);
    }
    public function create(ProductCategoryRequest $request){
        if ($request->isMethod('post')) {

            try{
            DB::beginTransaction();
            $category = $this->productCategoryInterface->create($request->all());
            DB::commit();
            return back()->with('success', 'Product category successfully! created');

        }catch(Exception $e){
            DB::rollback();
            return back()->with('error', "Fail to create product category");
        }

        }
        return view('admin.category.create');
    }

    public function createSubCategory(ProductCategoryRequest $request,$id){
        $productCategory = $this->productCategoryInterface->getProductCategoryById($id);
        if(!$productCategory){
            return back()->with('error', "Product category can't found");
        }
        if ($request->isMethod('post')) {

            try{
            DB::beginTransaction();
            $category = $this->productCategoryInterface->createSubCategory($request->all());
            DB::commit();
            return back()->with('success', 'Product Subcategory successfully! created');

        }catch(Exception $e){
            DB::rollback();
            return back()->with('error', "Fail to create product Subcategory");
        }

        }
        return view('admin.category.create-subcategory',['productCategory'=>$productCategory]);
    }

    public function subCategories($id){
        $sub = $this->productCategoryInterface->getProductCategoryById($id);
        $subcategories = $this->productCategoryInterface->listSubCategory($id);
        return view('admin.category.list-subcategory',['subcategories'=>$subcategories,'sub'=>$sub]);
    }
    public function update(ProductCategoryRequest $request,$id){
        $productCategory = $this->productCategoryInterface->getProductCategoryById($id);
        if(!$productCategory){
            return back()->with('error', "Product category can't found");
        }
        if ($request->isMethod('put')) {
            try{
                DB::beginTransaction();
                $category = $this->productCategoryInterface->create($request->all(),$id);
                DB::commit();
            return back()->with('success', 'Product category successfully! updated');
            }catch(Exception $e){
                DB::rollback();
                return back()->with('error', "Fail to update product category");
            }

        }
        return view('admin.category.update',['productCategory'=>$productCategory]);
    }

    public function getSubCategory($id){
        $subcategories = $this->productCategoryInterface->listSubCategory($id);
        return json_encode($subcategories);
    }
    }

