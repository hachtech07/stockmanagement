<?php
namespace App\Http\Controllers\Admin;

use App\Exports\SalesReportExport;
use App\Http\Controllers\Controller;
use App\Http\Requests\SalesRequest;
use App\Interfaces\ProductInterface;
use App\Interfaces\SalesInterface;
use Exception;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Maatwebsite\Excel\Excel;

class SalesController extends Controller
{
    /**
     * @var ProductInterface
     */
    private $productInterface,$salesInterface;

    public function __construct(ProductInterface $productInterface,SalesInterface $salesInterface,Excel $excel)
    {
        $this->productInterface = $productInterface;
        $this->salesInterface   = $salesInterface;
        $this->excel            = $excel;
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        if ($request->isMethod('post') || $request->isMethod('get')) {
        $sales = $this->salesInterface->findAll($request->all());
        return view('admin.stockout.index',$sales);
        }
    }
    public function saleDetail($id){
        $product = $this->productInterface->findProductWithCategory($id);
        // dd($product);
        if(!$product){
            return back()->with('error', "product does not exist");
        }
        return view('admin.stockout.create',['product'=>$product]);
        

    }

    public function sales (SalesRequest $request){
        if($request->isMethod('post')){

            try {
                DB::beginTransaction();
                $this->salesInterface->create($request->all());
                DB::commit();
                return back()->with('success', 'Successfully sold !!');
            } catch (Exception $e) {
                
            }  DB::rollback();
            return back()->with('error',$e->getMessage());
        }
        return view('admin.stockout.create');
    }

    public function export_sale(Request $request)
    {
        try {
            return $this->excel->download(new SalesReportExport($request), 'Sales-report.xlsx');
        } catch (Exception $e) {
            return back()->with('error', $e);
        }
    }
}
