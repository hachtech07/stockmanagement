<?php

namespace App\Http\Controllers\Admin;

use App\Exceptions\CustomException;
use App\Http\Controllers\Controller;
use App\Http\Requests\ChangeUserPassword;
use App\Http\Requests\UserStoreRequest;
use App\Interfaces\UserRepositoryInterface;
use Illuminate\Http\Request;
use Exception;
use Illuminate\Support\Facades\DB;

class UserController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    private $userInterface;
    public function __construct(UserRepositoryInterface $userInterface)
    {
        $this->userInterface = $userInterface;
    }

    public function index()
    {
        $users = $this->userInterface->all();
        return view('admin.user.index',['users'=>$users]);
    }
    public function create(UserStoreRequest $request){
        if ($request->isMethod('post')) {

            try {
                $users   = $this->userInterface->create($request->all());
                DB::commit();
    
                return back()->with('success', 'User created successfully!');
            } catch (Exception $e) {
                DB::rollback();
                return back()->with('error', "cannot create the user".$e);
            }
        }
        return view('admin.user.create');
        }
       
    public function update(UserStoreRequest $request,$id){
        $user = $this->userInterface->findById($id);
        // dd($user);
        if (!$user) {
            return back()->with('error', "User canot found");
        }
        if ($request->isMethod('put')) {
           
            try {
                DB::beginTransaction();
                $user = $this->userInterface->create($request->all(), $id);
                if ($user) {
                    DB::commit();
                    return back()->with('success', 'User updated successfully!');
                }
            } catch (Exception $e) {
                DB::rollback();
                return back()->with('error', "cannot update the user");
            }
        

        }
        return view('admin.user.update',['user'=>$user]);
    }

    public function import(Request $request){
        
        return view('admin.user.import');
    }

    public function changeUserPassword(ChangeUserPassword $request, $id){
        $user = $this->userInterface->findById($id);
        if (!$user) {
            return back()->with('error', "User canot found");
        }

        if ($request->isMethod('post')) {
            try{
                DB::beginTransaction();

                $changePassword = $this->userInterface->updateUserPassword($request->all(),$id);
                if ($changePassword) {
                    DB::commit();
                    return back()->with('success', 'Password changed successfully!');
                }
            }catch(Exception $e){
                DB::rollback();
                return back()->with('error', "cannot change the password");
            }

            return back()->with('success', 'User password changed successfully!');
        }
        return view('admin.user.changepassword', ['user' => $user]);
    }
    }
