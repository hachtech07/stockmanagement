<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use App\Mail\ForgotPassword;
use Illuminate\Support\Facades\Mail;
use Carbon\Carbon;
use Illuminate\Support\Facades\Auth;
use App\Models\User;
use App\Traits\RespondTrait;


class AuthController extends Controller
{
    use RespondTrait;
    /**
     * Create a new AuthController instance.
     *
     * @return void
     */
    public function __construct() {
        $this->middleware('auth:api', ['except' => ['login', 'register', 'forgot', 'forgotpassword','resetpassword']]);
    }

    /**
     * Get a JWT via given credentials.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function login(Request $request){
    	$validator = Validator::make($request->all(), [
            'email' => 'required|email',
            'password' => 'required|string|min:6',
        ]);

        if ($validator->fails()) {
            // return $this->errorResponse($validator->errors(),422);
            return $this->errorResponse($validator->errors(), 422);
        }

        if (! $token = auth()->attempt($validator->validated())) {
            return $this->errorResponse('Unauthorized', 401);
        }

        return $this->successResponse($this->createNewToken($token));
    }

    /**
     * Register a User.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function register(Request $request) {
        $validator = Validator::make($request->all(), [
            'name' => 'required|string|between:2,100',
            'email' => 'required|string|email|max:100|unique:users',
            'password' => 'required|string|confirmed|min:6',
        ]);

        if($validator->fails()){
            return response()->json($validator->errors()->toJson(), 400);
        }

        $user = User::create(array_merge(
                    $validator->validated(),
                    ['password' => bcrypt($request->password)]
                ));

        return response()->json([
            'message' => 'User successfully registered',
            'user' => $user
        ], 201);
    }


    /**
     * Log the user out (Invalidate the token).
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function logout() {
        auth()->logout();

        return response()->json(['message' => 'User successfully signed out']);
    }

    /**
     * Refresh a token.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function refresh() {
        return $this->createNewToken(auth()->refresh());
    }

    /**
     * Get the authenticated User.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function userProfile() {
        return response()->json(auth()->user());
    }

    /**
     * Get the token array structure.
     *
     * @param  string $token
     *
     * @return \Illuminate\Http\JsonResponse
     */
    protected function createNewToken($token){
        return response()->json([
            'access_token' => $token,
            'token_type' => 'bearer',
            'expires_in' => auth()->factory()->getTTL() * 60,
            'user' => auth()->user()
        ]);
    }

    public function forgot()
    {
        return view('auth.forgot');
    }

    public function forgotPassword(Request $request)
    {
        Validator::make($request->all(), [
            'email'          => 'required|email'
        ])->validate();

        $user = User::where('email', $request->email)->first();
        if (!$user) {
            $message = "Sorry We Dont have Such Email Registered !!";
            return back()->with('error', $message);
        }
        $now = Carbon::now();
        
        if ($now->greaterThan($user->token_expired_at))
        { 
            $user->reset_token = mt_rand(100000, 999999);
            $user->token_expired_at = Carbon::now()->addMinutes(30);
        }
        else{
            return back()->with('info', 'Email has been already sent to '.$user->email);
        }

        $user->save();
        $redirectUrl = 'http://127.0.0.1:8000/resetpassword';
        Mail::to($user->email)
            ->send(new forgotpassword($user, $redirectUrl));
        return back()->with('success', 'Email Sent successfully to '.$user->email);
    }

    public function resetpassword(Request $request, $token, $id)
    {
        $data = array(['token' => $token , 'id' => $id]);


        if($request -> isMethod('post')) {
            Validator::make($request->all(), [
                'password' => 'required_with:confirm_password|same:confirm_password',
                'confirm_password' => 'required'
            ])->validate();

            $data = $this->checkResetCode($id, $token);
    
            if (!$data['status']) {
                return back() -> withErrors($data['message'])->withInput();
            }
            $user = $data['user'];
            $user->password = bcrypt($request->password);
            $user->save();
            return redirect('/login')->with('success', 'Password changed Successfully!!');
        }

        return view('auth.reset', compact('data'));
    }

    private function checkResetCode($id, $resetCode)
    {
        $now = Carbon::now();
        $user = User::select('email', 'id', 'token_expired_at')->where(['id' => $id, 'reset_token' => $resetCode])->first();

        if ($user == null) {
            $message = "Invalid Reset Code !!";
            $data = array("status" => false, "message" => $message);
        } else if ($now->greaterThan($user->token_expired_at)) {
            $message = "Sorry the token has expired !!";
            $data = array("status" => false, "message" => $message);
        } else {
            $data = array("status" => true, "user" => $user, "message" => "Reset Code Is Valid!!");
        }
        return $data;
    }
}