<?php

namespace App\DataTables;

//use App\Models\ExportDataTable;

use Illuminate\Support\Facades\Auth;
use Yajra\DataTables\Html\Button;
use Yajra\DataTables\Html\Column;
use Yajra\DataTables\Html\Editor\Editor;
use Yajra\DataTables\Html\Editor\Fields;
use Yajra\DataTables\Services\DataTable;

class ExportDataTable extends DataTable
{
    /**
     * Build DataTable class.
     *
     * @param mixed $query Results from query() method.
     * @return \Yajra\DataTables\DataTableAbstract
     */
    public function dataTable($query)
    {
        return datatables()
            ->eloquent($query)
            ->addColumn('action', 'exportdatatable.action');
    }

    /**
     * Get query source of dataTable.
     *
     * @param \App\Models\ExportDataTable $model
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function query(ExportDataTable $model)
    {
        $data = \App\Models\Billing::selectRaw("customers.name as customer_name, users.name as collector_name, billing.from_year, billing.to_year, billing.date_np, billing.to_month, billing.from_month, billing.gross_amount, billing.discount_amount, billing.net_amount, billing.tender_amount, billing.due_amount, billing.created_at")
        ->join('customers', 'billing.customer_id', '=', 'customers.id')
        ->join('users', 'billing.user_id', '=', 'users.id')
        ->where('net_amount', ">",0)
        ->where('customers.branch_id', '=', Auth::user()->branch_id);
        return $this->applyScopes($data);
    }

    /**
     * Optional method if you want to use html builder.
     *
     * @return \Yajra\DataTables\Html\Builder
     */
    public function html()
    {
        return $this->builder()
        ->columns($this->getColumns())
        ->minifiedAjax()
        ->dom('Bfrtip')
        ->orderBy(1)
        ->buttons(
            Button::make('csv'),
            Button::make('excel')
        );
    }

    /**
     * Get columns.
     *
     * @return array
     */
    protected function getColumns()
    {
        return [
            Column::make('customer_name'),
            Column::make('collector_name'),
            Column::make('from_year'),
            Column::make('to_year'),
            Column::make('created_at'),
            Column::make('gross_amount'),
            Column::make('discount_amount'),
            Column::make('net_amount'),
            Column::make('tender_amount'),
            Column::make('due_amount'),
        ];
    }

    /**
     * Get filename for export.
     *
     * @return string
     */
    protected function filename()
    {
        return 'Export_' . date('YmdHis');
    }
}
