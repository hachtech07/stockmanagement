<?php

namespace App\Interfaces;


interface RolePermissionInterface
{
    public function add($attributes);
    public function update(array $attributes ,$id);
    public function findRoleById($id);
}
