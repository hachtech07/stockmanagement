<?php

namespace App\Interfaces;

interface LogInterface {
    public function getAllLog();
    public function userLogIndex($attributes);
    
}