<?php

namespace App\Interfaces;


use Illuminate\Support\Collection;

interface UserRepositoryInterface
{
    public function all(): Collection;
    public function create(array $attribute, $id = null);

    public function getById();

    public function getUserInfo();

    public function findById($id);

    public function authenticatedUser();

    public function updateUserPassword(array $attributes, $id);
}
