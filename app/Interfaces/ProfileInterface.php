<?php

namespace App\Interfaces;

interface ProfileInterface{
    public  function update(array $attributes,$id);
    public function changeUserPassword(array $attribute, $id);
}
