<?php

namespace App\Interfaces;

interface ProductInterface{

    public function create($attributes);
    public function findById($id);
    public function getProducts();
    public function findAll($attributes);
    public function updateStock($attributes,$id);
    public function findProductWithCategory($id);
}