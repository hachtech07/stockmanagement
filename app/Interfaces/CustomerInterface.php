<?php

namespace App\Interfaces;

use GuzzleHttp\Psr7\Request;
use Illuminate\Support\Collection;

interface CustomerInterface
{
    public function getAllCustomer();
    public function create(array $attributes, $id);
    public function update(array $attributes, $id);
    public function getCustomerById($id);
    public function getCustomerData();
    public function searchCustomer($searchParam);
    public function fetchCustomerData(array $attributes);
    public function updateData($id);
    public function createData();
    public function search($searchParam = null);
    public function lastCustomerNumber($branchId=null);
    public function transactionHistory($id);
}
