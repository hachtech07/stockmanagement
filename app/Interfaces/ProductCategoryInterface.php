<?php

namespace App\Interfaces;

use GuzzleHttp\Psr7\Request;
use Illuminate\Support\Collection;

interface ProductCategoryInterface
{
    public function getProductCategory();
    public function getActiveProductCategory();
    public function create(array $attributes, $id = null);
    public function createSubCategory($attributes,$id = null);
    public function listSubCategory($id);
    public function getProductCategoryById($id);
}
