<?php

namespace App\Interfaces;

use Illuminate\Support\Collection;

interface BillingInterface
{
    public function create(array $attributes, $customer_id, $due_amount, $advance_amount);
    public function getAllBilling();
    public function getAllMyBill($id);
    public function getAllMyBillIndex($id,$attributes);
    public function getBillIndex(array $attributes);
    public function viewBilling($id);
    public function getAllReturnedBillIndex($attributes);
    public function getAllReturnedBill();
    public function getAllReturnBillDetail($id);
    public function billReturnPrint($id);
    public function getMaterializedBill($id);
    public function getMaterializedBillDetail($id);
    public function cancelBill($attributes,$id);
}
