<?php

namespace App\Interfaces;

use Illuminate\Support\Collection;

interface BillingCategoryInterface
{
    public function update(array $attributes, $id);
}
