<?php

namespace App\Interfaces;

use Illuminate\Database\Eloquent\Model;


interface EloquentInterface
{
    /**
     * @param array $attributes
     * @return Model
     */
    // public function create(array $attributes): Model;

    /**
     * @param $id
     * @return Model
     */
    public function find($id): ?Model;
}
