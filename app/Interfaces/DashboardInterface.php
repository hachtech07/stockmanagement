<?php

namespace App\Interfaces;

use Illuminate\Support\Collection;

interface DashboardInterface
{
    public function total_bill_collected();
    public function total_bill_collected_till_date();
    public function total_bill_generated();
    public function total_bill_generated_till_date();
    public function get_all_collector();
    public function get_all_customers();
    public function get_due_and_collected($year = null);
    public function get_category_wise_total();
    public function get_month_wise_bill_generated($year = null);
    public function dashboard_report(array $attributes);
    public function getAllIndexItems();
    public function total_bill_returned();
}
