<?php

namespace App\Interfaces;

use Illuminate\Support\Collection;

interface BranchInterface
{
    public function getAllBranch();
    public function getActiveBranch();
    public function create(array $attributes, $id = null);
    public function getBranchById($id);
    public function findById($id);
    public function delete($id);
}
