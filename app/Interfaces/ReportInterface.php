<?php

namespace App\Interfaces;

use Illuminate\Support\Collection;

interface ReportInterface
{
    public function getDiscountReport();
    public function getDueReport();
    public function getCollectionReport();
    public function getAdvanceReport();
    public function getCategoryWiseReport();
    public function getExportQuery();
    public function getDiscountReportIndex(array $attributes);
    public function getDueReportIndex(array $attributes);
    public function getCategoryWiseReportIndex(array $attributes);
    public function getCollectionReportIndex(array $attributes);
    public function exportAll();
    public function getBillReturnReportIndex(array $attributes);
    public function getMaterializedViewReportIndex(array $attributes);
    public function printMaterializedReport(array $attributes);
}
