<?php

namespace App\Interfaces;

use GuzzleHttp\Psr7\Request;
use Illuminate\Support\Collection;

interface ProductSubCategoryInterface
{
    public function createSubCategory($attributes,$id = null);
    public function listSubCategory($id);
}
