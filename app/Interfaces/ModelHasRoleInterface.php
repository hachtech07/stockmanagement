<?php

namespace App\Interfaces;


use App\Models\User;
use Illuminate\Support\Collection;

interface ModelHasRoleInterface
{

    public function create(array $attributes);
}
