<?php

namespace App\View\Components;

use Illuminate\View\Component;

class Option extends Component
{
    public $lists;
    public $id;
    public $title;

    /**
     * Create a new component instance.
     *
     * @return void
     */
    public function __construct($lists, $id, $title)
    {
        $this->list = $lists;
        $this->id = $id;
        $this->title = $title;
    }

    /**
     * Get the view / contents that represent the component.
     *
     * @return \Illuminate\Contracts\View\View|string
     */
    public function render()
    {
        return view('components.select');
    }
}
