<?php

namespace App\View\Components;

use Illuminate\View\Component;

class DashboardCard extends Component
{

    /**
     * The title of card
     *
     * @var integer
     */
    public $title;

    /**
     *  total
     *
     * @var string
     */
    public $total;

    /**
     * A fontawesome icon name
     *
     * @var string
     */
    public $icon;

    /**
     * A background color
     *
     * @var string
     */
    public $backgroundColor;

    /**
     * A iconBackground color
     *
     * @var string
     */
    public $iconBackgroundColor;


    /**
     * Create a new component instance.
     *
     * @return void
     */
    public function __construct($title, $total, $icon, $backgroundColor,$iconBackgroundColor)
    {
        $this->title = $title;
        $this->total  = $total;
        $this->icon  = $icon;
        $this->backgroundColor = $backgroundColor;
        $this->iconBackgroundColor = $iconBackgroundColor;
    }

    /**
     * Get the view / contents that represent the component.
     *
     * @return \Illuminate\Contracts\View\View|\Closure|string
     */
    public function render()
    {
        return view('components.dashboard-card');
    }
}
