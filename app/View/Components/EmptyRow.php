<?php

namespace App\View\Components;

use Illuminate\View\Component;

class EmptyRow extends Component
{

    /**
     * The title name.
     *
     * @var integer
     */
    public $colspan;

    /**
     * Create a new component instance.
     *
     * @return void
     */
    public function __construct($colspan)
    {
        $this -> colspan = $colspan;
    }

    /**
     * Get the view / contents that represent the component.
     *
     * @return \Illuminate\Contracts\View\View|\Closure|string
     */
    public function render()
    {
        return view('components.empty-row');
    }
}
