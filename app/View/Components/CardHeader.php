<?php

namespace App\View\Components;

use Illuminate\View\Component;

class CardHeader extends Component
{

    /**
     * The title name.
     *
     * @var integer
     */
    public $title;

    /**
     * A href link.
     *
     * @var string
     */
    public $link;

    /**
     * A fontawesome icon name
     *
     * @var string
     */
    public $icon;

    /**
     * A button label
     *
     * @var string
     */
    public $buttonLabel;

    /**
     * Create a new component instance.
     *
     * @return void
     */
    public function __construct($title, $link, $icon, $buttonLabel)
    {
        $this -> title = $title;
        $this -> link  = $link;
        $this -> icon  = $icon;
        $this -> buttonLabel = $buttonLabel;
    }

    /**
     * Get the view / contents that represent the component.
     *
     * @return \Illuminate\Contracts\View\View|\Closure|string
     */
    public function render()
    {
        return view('components.cardheader');
    }
}
