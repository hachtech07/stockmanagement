<?php

namespace App\View\Components\Input;

use Illuminate\View\Component;

class Branch extends Component
{
    /**
     * Create a new component instance.
     *
     * @return void
     */
    private $branches;
    public function __construct($branches)
    {
        $this->branches = $branches;
    }

    /**
     * Get the view / contents that represent the component.
     *
     * @return \Illuminate\Contracts\View\View|\Closure|string
     */
    public function render()
    {
        return view('components.input.branch');
    }
}
