<?php

namespace App\View\Components;

use Illuminate\View\Component;

class tr extends Component
{
     /**
     * The title name.
     *
     * @var json object
     */
    public $tr;
    public $controller_name;

    /**
     * Create a new component instance.
     *
     * @return void
     */
    public function __construct($tr, $controller_name)
    {
        $this -> tr = $tr;
        $this -> controller_name = $controller_name;
    }

    /**
     * Get the view / contents that represent the component.
     *
     * @return \Illuminate\Contracts\View\View|string
     */
    public function render()
    {
        return view('components.tr');
    }
}
