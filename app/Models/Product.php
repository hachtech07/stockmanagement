<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    use HasFactory;
    protected $table = 'products';
    protected $fillable = [
        'product_name',
        'product_category_id',
        'product_sub_category_id',
        'color',
        'model',
        'mrp',
        'cp',
        'stock_alert',
        'entry_date',
        'fiscal_year_id',
        'batch_no',
        'created_by',
        'unit'
    ];

    public static function getBatchList(){
        return self::select('batch_no')->get();
    }
    public static function findById($id){
        return self::find($id);
    }
}
