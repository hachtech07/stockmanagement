<?php

namespace App\Models;

use Eloquent as Model;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Illuminate\Support\Facades\Auth;

use Spatie\Activitylog\Traits\LogsActivity;

class Customer extends Model
{
    use HasFactory, Notifiable, LogsActivity;

    const ACTIVE    = 'active';
    const INACTIVE  = 'inactive';
    public static $status = [self::ACTIVE, self::INACTIVE];
    protected $table = 'customers';
    protected static $logName = 'Customer log';
    protected static $logFillable = true;
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'customer_number',
        'name',
        'address',
        'phone_number',
        'municipality',
        'ward',
        'house_number',
        'branch_id',
        'status',
        'customer_category_id',
        'due_amount',
        'advance_amount',
        'monthly_payable',
        'last_bill_date',
        'created_by',
        'updated_by',
        'created_at',
        'updated_at',
        'collector_id',
        'vat_pan_number'
    ];
    // protected static $logFillable = true;
    protected static $logAttributes = [
        'customer_number',
        'name',
        'address',
        'phone_number',
        'municipality',
        'ward',
        'house_number',
        'branch_id',
        'status',
        'customer_category_id',
        'due_amount',
        'advance_amount',
        'monthly_payable',
        'last_bill_date',
        // 'card_number',
        'created_by',
        'updated_by',
        'created_at',
        'updated_at',
        'collector_id',
        'vat_pan_number'
    ];
    public function branch()
    {
        return $this->belongsTo(Branch::class, 'branch_id');
    }

    public function customerCategory()
    {
        return $this->belongsTo(CustomerCategory::class);
    }

    public function billingCategory()
    {
        return $this->belongsTo(\App\Models\BillingCategory::class, 'billing_category_id');
    }

    public static function getAllCustomer($userId)
    {
        $userRole = \App\Models\User::getUserRole($userId);
        if ($userRole === 'super admin') {
            return self::all();
        }
        $userDetail = \App\Models\User::find($userId);
        return self::where('branch_id', $userDetail->branch_id)->get();
    }
    public static function searchCustomer($userId, $searchKey)
    {
        $userDetail = \App\Models\User::find($userId);
        return self::where('branch_id', $userDetail->branch_id)
            ->where('collector_id', auth()->user()->id)->where('status', 'active')->where(function ($query) use ($searchKey) {
                $query->where('customer_number', 'like', '%' . $searchKey . '%')
                    ->orWhere('name', 'like', '%' . $searchKey . '%');
            })->get();
    }

    public static function get_my_customer_by_branch()
    {
        return self::select('customer_number', 'name')
            ->where('branch_id', Auth::user()->branch_id)
            ->get();
    }

    public static function get_customer_by_branch_id($id)
    {
        return self::select('customer_number', 'name')
            ->where('branch_id', $id)
            ->get();
    }
}
