<?php

namespace App\Models;

use Eloquent as Model;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Illuminate\Support\Facades\Config;

class Branch extends Model
{
    use HasFactory, Notifiable;

    const ACTIVE    = 'active';
    const INACTIVE  = 'inactive';
    public static $status = [self::ACTIVE, self::INACTIVE];

    protected $guarded = [];
    protected static $logName = 'Branch log';
    protected static $logFillable = true;
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'branch_code',
        'branch_title',
        'address',
        'status',
    ];

    protected static $logAttributes = [
        'branch_code',
        'branch_title',
        'address',
        'status',
];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    public static function getBranches()
    {
        return self::select('id', 'branch_code', 'branch_title', 'address', 'status')->where('id', '>', Config::get('constant.zero'))->get();
    }

    public static function getActiveBranches()
    {
        return self::select('id', 'branch_code', 'branch_title', 'address', 'status')->where('id', '>', Config::get('constant.zero'))->where('status', 'active')->get();
    }

    public static function getAllBranch()
    {
        return self::select('id', 'branch_title')->get();
    }
}
