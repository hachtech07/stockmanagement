<?php

namespace App\Models;

use Eloquent as Model;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;

class ProductSubCategory extends Model
{
    use HasFactory, Notifiable;

    const ACTIVE    = 'active';
    const INACTIVE  = 'inactive';
    public static $status = [self::ACTIVE, self::INACTIVE];
    protected $table = 'sub_category';
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'category_id',
        'subcategory_name',
        'subcategory_status'
    ];

    public static function getSubCategories($id){
        return self::where('category_id',$id)->get();
    }

    public static function getSubCategory($id){
        return self::find($id);
    }
}
