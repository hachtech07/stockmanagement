<?php

namespace App\Models;

use Illuminate\Support\Facades\Auth;
use Spatie\Permission\Traits\HasRoles;
use Tymon\JWTAuth\Contracts\JWTSubject;
use Illuminate\Notifications\Notifiable;
use Spatie\Activitylog\Traits\LogsActivity;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Symfony\Component\HttpKernel\Exception\HttpException;
use Spatie\Activitylog\LogOptions;

class User extends Authenticatable implements JWTSubject
// class User extends Authenticatable
{
    use HasFactory, Notifiable, HasRoles;

    const ACTIVE    = 'active';
    const INACTIVE  = 'inactive';

    public static $status = [self::ACTIVE, self::INACTIVE];

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'first_name',
        'last_name',
        'username',
        'password',
        'status'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password',
        'remember_token',
        'reset_token',
        'token_expired_at'
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    protected static $logName = 'User log';


    public static function getUserBranch()
    {
        $branch_id = self::select('branch_id')->where('id', \Auth::id())->get();
        if ($branch_id && !empty($branch_id)) {
            return $branch_id[0]['branch_id'];
        }
        return new HttpException("Access denied", 403);
    }

    public static function getBranchUsers()
    {
    }

    public static function getUserRole($id)
    {
        $columnNames = config('permission.column_names');

        $userRole = \App\Models\Role::leftJoin('model_has_roles', 'roles.id', '=', 'model_has_roles.role_id')->select('roles.name')->where('model_has_roles.' . $columnNames['model_morph_key'], $id)->get();

        if ($userRole && !empty($userRole)) {
            return $userRole[0]['name'];
        }
        return new HttpException("Access denied", 403);;
    }

    /**
     * Get the identifier that will be stored in the subject claim of the JWT.
     *
     * @return mixed
     */
    public function getJWTIdentifier()
    {
        return $this->getKey();
    }

    /**
     * Return a key value array, containing any custfom claims to be added to the JWT.
     *
     * @return array
     */
    public function getJWTCustomClaims()
    {
        return [];
    }

    // public function role()
    // {
    //     return $this -> hasOne(ModelHasRole::class, 'model_id');
    // }

    public function branch()
    {
        return $this->belongsTo(\App\Models\Branch::class, 'branch_id');
    }

    // public function role()
    // {
    //     return $this -> belongsTo(\App\Models\Role::class, 'model_id');
    // }
    public static function getAllUserListByBranch($branch_id)
    {
        return User::select("id")->where('branch_id', "=", $branch_id);
    }


    public static function get_my_user_by_branch()
    {
        return self::select('id', 'name')
            ->where('branch_id', Auth::user()->branch_id)
            ->where('id', '!=', Auth::user()->id)
            ->get();
    }
    public function setPasswordAttribute($password)
    {
        $this->attributes['password'] = bcrypt($password);
    }

    public static function getAllUser(){
        return self::all();
    }

    // public function getActivitylogOptions(): LogOptions
    // {
    //     return LogOptions::defaults()
    //     ->logFillable();
    //     // Chain fluent methods for configuration options
    // }

     // Customize log description
    //  public function getDescriptionForEvent(string $eventName): string
    //  {
    //      return "This model has been {$eventName}";
    //  }
}
