<?php

namespace App\Models;

use Eloquent as Model;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;

class Permission extends Model
{
    use HasFactory, Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name',
        'guard_name',
    ];

    public static function getUserPermission($user_id)
    {
        return self::leftJoin('model_has_permissions', function($join) use($user_id){
            $join -> on('model_has_permissions.permission_id', '=', 'permissions.id')
                 -> where(['model_has_permissions.model_id' => $user_id]);
        }) -> select('permissions.id', 'permissions.name', 'model_has_permissions.permission_id') -> get();
    }

    public static function getRolePermission($role_id)
    {
        return self::leftJoin('role_has_permissions', function($join) use($role_id){
            $join -> on('role_has_permissions.permission_id', '=', 'permissions.id')
                 -> where(['role_has_permissions.role_id' => $role_id]);
        }) 
        -> select('permissions.id', 'permissions.name', 'role_has_permissions.permission_id') -> get();
    }

}
