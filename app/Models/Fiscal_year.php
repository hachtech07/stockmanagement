<?php

namespace App\Models;

use Eloquent as Model;

class Fiscal_year extends Model
{

    protected $primaryKey = 'id';
    public $timestamps = false;
    protected $table = 'fiscal_year';

    protected $fillable = [
        'fiscal_year',
        'fiscal_year_start',
        'fiscal_year_end'
    ];

    public function listFiscalYears()
    {
        return self::all();
    }
}
