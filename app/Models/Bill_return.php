<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Bill_return extends Model
{
    use HasFactory;
    protected $fillable = [
        'customer_id',
        'user_id',
        'date_np',
        'from_year',
        'from_month',
        'to_year',
        'to_month',
        'gross_amount',
        'discount_amount',
        'net_amount',
        'tender_amount',
        'due_amount',
        'created_date_time',
        'advance_amount',
        'credit_note',
        'billing_id',
        'returned_by',
        'vat_amount',
        'fiscal_year',
        'remarks',
        'ird_status'
    ];
    public function customer()
    {
        return $this->belongsTo(\App\Models\Customer::class, 'customer_id');
    }
}
