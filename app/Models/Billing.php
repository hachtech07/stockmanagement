<?php

namespace App\Models;

use Eloquent as Model;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;

use Spatie\Activitylog\Traits\LogsActivity;
use Illuminate\Notifications\Notifiable;
use JWTAuth;
use Illuminate\Support\Facades\DB;

class Billing extends Model
{
    use Notifiable, LogsActivity;

    const ACTIVE    = 'active';
    const INACTIVE  = 'inactive';

    public static $status = [self::ACTIVE, self::INACTIVE];

    public $table = 'billing';
    protected $guarded = [];
    protected static $logName = 'Bill log';
    protected static $logFillable = true;
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'customer_id',
        'user_id',
        'date_np',
        'from_year',
        'from_month',
        'to_year',
        'to_month',
        'gross_amount',
        'discount_amount',
        'discount_percent',
        'net_amount',
        'tender_amount',
        'due_amount',
        'created_date_time',
        'advance_amount',
        'previous_due_amount',
        'previous_advance_amount',
        'fiscal_year_id',
        'ird_status',
        'total_amount'
    ];
    protected static $logAttributes = [
        'customer_id',
        'user_id',
        'date_np',
        'from_year',
        'from_month',
        'to_year',
        'to_month',
        'gross_amount',
        'discount_amount',
        'net_amount',
        'tender_amount',
        'due_amount',
        'created_date_time',
        'advance_amount',
        'total_amount'
    ];

    public function customer()
    {
        return $this->belongsTo(\App\Models\Customer::class, 'customer_id');
    }

    public function user()
    {
        return $this->belongsTo(\App\Models\User::class, 'user_id');
    }

    public static function getLastBilling($customerId)
    {
        return self::where('customer_id', $customerId)->orderBy('id', 'DESC')->first();
    }
    public static function getTodaysNoOfBilling()
    {
        return self::where('user_id', auth()->user()->id)->whereRaw('date(created_at) = CURDATE()')->count();
    }

    public static function getTodaysTotalAmountOfBilling()
    {
        $data = self::where('user_id', auth()->user()->id)->whereRaw('date(created_at) = CURDATE()')->sum('tender_amount');
        return $data;
    }
    public static function getTodaysBillList($id)
    {
        $data = self::with('customer', 'user')->where('user_id', $id)->whereDate('created_at', '=', date('Y-m-d'))->orderBy('created_at', 'DESC')->get();
        return $data;
    }
    public static function getBillDetails($id)
    {
        $data = self::where('id', $id)->get();
        return $data;
    }

    public function getActivitylogOptions(): LogOptions
    {
        return LogOptions::defaults()
            ->logFillable();
        // Chain fluent methods for configuration options
    }

    public function billing_details()
    {
        return $this->hasMany(\App\Models\BillingDetail::class, 'billing_id');
    }
}
