<?php

namespace App\Models;

use Eloquent as Model;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;

class ProductCategory extends Model
{
    use HasFactory, Notifiable;

    const ACTIVE    = 'active';
    const INACTIVE  = 'inactive';
    public static $status = [self::ACTIVE, self::INACTIVE];
    protected $table = 'product_categories';
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name',
        'parent_id',
        'status'
    ];
    public static function get_product_categories()
    {
        return self::where('status', 'active')
            ->get();
    }

    public function getCategories(){
        return self::where('status', 'active')->whereNull('parent_id')
        ->get();
    }

    public function getSubCategories($id){
        return self::where('status','active')->whereNotNull('parent_id')->get();
    }
}
