<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Sales extends Model
{
    use HasFactory;
    protected $table = 'sales';
    protected $fillable = [
        'product_name',
        'product_id',
        'product_category',
        'product_sub_category',
        'model',
        'color',
        'mrp',
        'fiscal_year_id',
        'batch_no',
        'sold_by',
        'quantity'
    ];

    public static function getBatchList(){
        return self::select('batch_no')->get();
    }
    public static function findById($id){
        return self::find($id);
    }
}
