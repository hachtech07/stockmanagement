<?php

namespace App\Models;

use Eloquent as Model;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Illuminate\Support\Facades\Config;

class BillingCategory extends Model
{
    use HasFactory, Notifiable;

    const ACTIVE            = 'active';
    const INACTIVE          = 'inactive';

    const MONTHLY_CHARGE    = 'yes';
    const NON_MONTHLY_CHARGE = 'no';

    const MONTHLY_PAYABLE_CATEGORY_ID   = 1;
    const DUE_AMOUNT_CATEGORY_ID        = 2;
    const ADVANCE_AMOUNT_CATEGORY_ID    = 3;
    public $table = 'billing_categories';
    public static $status = [self::ACTIVE, self::INACTIVE];
    protected static $logName = 'BillingCategory log';
    protected static $logFillable = true;
    protected $guarded = [];

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'title',
        'default_price',
        'monthly_charge',
        'created_by',
        'updated_by',
        'status'
    ];

    protected static $logAttributes = [
        'title',
        'default_price',
        'monthly_charge',
        'created_by',
        'updated_by',
        'status'
];

    public static function getActive()
    {
        return self::select('id', 'title', 'default_price', 'monthly_charge', 'status')->where('id', '>', Config::get('constant.advance'))->get();
    }

    public static function getCategoriesDetail(array $categoryIds)
    {
        return self::select('id', 'title', 'default_price', 'monthly_charge')->whereIn('id', $categoryIds)->get();
    }

    public static function getSubscribedCategory($id)
    {
        return self::where('monthly_charge', self::MONTHLY_CHARGE)->where('id', $id)->where('status', self::ACTIVE)->get();
    }

    public static function getActiveMonthlyCharges()
    {
        return self::where('monthly_charge', self::MONTHLY_CHARGE)->where('status', self::ACTIVE)->get();
    }

    public static function getActiveNonMonthlyCharges()
    {
        return self::where('monthly_charge', self::NON_MONTHLY_CHARGE)->where('id', '>', Config::get('constant.advance'))->where('status', self::ACTIVE)->get();
    }

    public static function getNumberOfMonthPaid($fromYear, $fromMonth, $toYear, $toMonth)
    {
        $fromMonth = $fromMonth - 1; // -1 to calculate month difference

        if ($fromYear ==  $toYear) {
            $months = $toMonth - $fromMonth;
        } else if ($toYear > $fromYear) {
            $months = ($toMonth + (12 * ($toYear - $fromYear))) - $fromMonth;
        } else {
            return false;
        }

        return $months;
    }
}
