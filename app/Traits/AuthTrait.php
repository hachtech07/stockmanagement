<?php

namespace App\Traits;

use Illuminate\Support\Facades\Auth;

trait AuthTrait{

    public function authId()
	{
		return Auth::id();
	}

	public function authUserType()
	{
		return Auth::user()->user_type;
	}

	public function authBranchId()
	{
		return Auth::user()->branch_id;
	}

	public function authUser()
	{
		return Auth::user();
	}

}