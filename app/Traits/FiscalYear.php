<?php

namespace App\Traits;

use App\Models\Fiscal_year;
use App\Traits\DateConveterTrait;

trait FiscalYear
{

    use DateConveterTrait;

    function fiscalYear($dateTime)
    {

        $fiscalYear = Fiscal_year::where('fiscal_year_end', '>=', $dateTime)
            ->where('fiscal_year_start', '<=', $dateTime)
            ->first();
        return $fiscalYear;
    }

    function formattedFiscalDate($convertedDate){
        $fiscalYear         = $this->fiscalYear($convertedDate);
        $fiscal_start       = explode('-',$fiscalYear->fiscal_year_start)[0];
        $fiscals            = substr(explode('-',$fiscalYear->fiscal_year_end)[0],1,3);
        $fiscal_year        = $fiscal_start.'.'.$fiscals;
        return $fiscal_year;

    }
}
