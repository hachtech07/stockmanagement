<?php

namespace App\Repository;

use App\Repository\BaseRepository;
use App\Interfaces\DashboardInterface;
use App\Models\Bill_return;
use App\Models\Billing;
use App\Models\BillingCategory;
use App\Models\User;
use Carbon\Carbon;
use App\Models\Customer;
use App\Models\BillingDetail;
use Illuminate\Support\Facades\DB;
use App\Models\Branch;
use App\Traits\AuthTrait;
use App\Traits\DateConveterTrait;
use App\Traits\FiscalYear;

class DashboardRepository extends BaseRepository implements DashboardInterface
{

    /**
     * UserRepository constructor.
     *
     * @param User $model
     */
    use DateConveterTrait;
    use AuthTrait;
    use FiscalYear;
    private $billingCategory;
    private $billing;
    private $user;
    private $billReturn;

    public function __construct(BillingCategory $billingCategory, Billing $billing, User $user, Bill_return $billReturn)
    {
        $this->billing = $billing;
        $this->billingCategory = $billingCategory;
        $this->user = $user;
        $this->billReturn = $billReturn;
    }

    /**
     * @return Collection
     */

    public function total_bill_collected()
    {
        $convertedDate = $this->current_date_to_nepali() . ' ' . date('H:i:s');
        $currentFiscalYear = $this->fiscalYear($convertedDate);
        $branch_id         = $this->authbranchid();
        // $query             = DB::query()
        //                     ->selectRaw("SUM(X.tender_amount) as tender_amount")
        //                     ->from(DB::raw("(SELECT

        //                 IF(
        //                     (
        //                         ISNULL(
        //                             SUM(bill_returns.tender_amount)
        //                         ) = 1
        //                     ),
        //                     billing.tender_amount,

        //                         (
        //                             billing.tender_amount - SUM(bill_returns.tender_amount)
        //                         )

        //                 ) AS tender_amount
        //             FROM
        //                 `billing`
        //             LEFT JOIN `bill_returns` ON `bill_returns`.`billing_id` = `billing`.`id`"))
        //             ->where('billing.tender_amount','>','0.00')
        //             ->where('billing.fiscal_year_id',$currentFiscalYear->id)
        //             ->whereRaw('date(billing.created_date_time) = CURDATE()')
        //             ->groupBy(DB::raw("bill_returns.billing_id,billing.id) X"))->get();
        $query = $this->billing->where('billing.fiscal_year_id', $currentFiscalYear->id)->whereRaw('date(created_at) = CURDATE()');

        if ($this->authusertype() == 'branch admin') {
            $query->whereIn('user_id', $this->user->getAllUserListByBranch($branch_id));
        }
        // return $query[0]->tender_amount;
        return $query->sum('tender_amount');
    }

    public function total_bill_collected_till_date()
    {
        $convertedDate = $this->current_date_to_nepali() . ' ' . date('H:i:s');
        $currentFiscalYear = $this->fiscalYear($convertedDate);
        $branch_id         = $this->authbranchid();
        // $query             = DB::query()
        //                     ->selectRaw("SUM(X.tender_amount) as tender_amount")
        //                     ->from(DB::raw("(SELECT

        //                 IF(
        //                     (
        //                         ISNULL(
        //                             SUM(bill_returns.tender_amount)
        //                         ) = 1
        //                     ),
        //                     billing.tender_amount,

        //                         (
        //                             billing.tender_amount - SUM(bill_returns.tender_amount)
        //                         )

        //                 ) AS tender_amount
        //             FROM
        //                 `billing`
        //             LEFT JOIN `bill_returns` ON `bill_returns`.`billing_id` = `billing`.`id`"))
        //             ->where('billing.tender_amount','>','0.00')
        //             ->where('billing.fiscal_year_id',$currentFiscalYear->id)
        //             ->groupBy(DB::raw("bill_returns.billing_id,billing.id) X"))->get();
        $query             = $this->billing->query()->where('billing.fiscal_year_id', $currentFiscalYear->id);
        
        if ($this->authusertype() == 'branch admin') {
            $query->whereIn('user_id', $this->user->getAllUserListByBranch($branch_id));
        }
        // return $query[0]->tender_amount;
        return $query->sum("tender_amount");
    }

    public function total_bill_generated()
    {
        $branch_id         = $this->authbranchid();
        $data             = $this->billing->whereRaw('date(created_at) = CURDATE()');

        if ($this->authusertype() == 'branch admin') {
            $data->whereIn('user_id', $this->user->getAllUserListByBranch($this->authbranchid()));
        }

        $data = $data->where('net_amount', '>', '0.00')->get()->count();
        return $data;
    }

    public function total_bill_generated_till_date()
    {
        $branch_id         = $this->authbranchid();
        $data             = $this->billing->query();

        if ($this->authusertype() == 'branch admin') {
            $data->whereIn('user_id', $this->user->getAllUserListByBranch($this->authbranchid()));
        }

        $convertedDate = $this->current_date_to_nepali() . ' ' . date('H:i:s');
        $currentFiscalYear = $this->fiscalYear($convertedDate);

        $data = $data->where('fiscal_year_id', $currentFiscalYear->id)->where('net_amount', '>', '0')->count();
        return $data;
    }

    public function get_all_collector()
    {
        $data = $this->user->where('user_type', 'collector');
        if ($this->authusertype() == 'branch admin') {
            $data->where('branch_id', $this->authbranchid());
        }
        $data = $data->get()->count();
        return $data;
    }

    public function get_all_customers()
    {
        $data = Customer::query();

        if ($this->authusertype() == 'branch admin') {
            $data->where('branch_id', $this->authbranchid());
        }

        $data = $data->get()->count();
        return $data;
    }

    public function get_due_and_collected($year = null)
    {
        $convertedDate = $this->current_date_to_nepali() . ' ' . date('H:i:s');
        $currentFiscalYear = $this->fiscalYear($convertedDate);
        // $collected = DB::query()
        //                 ->selectRaw("SUM(X.tender_amount) as t_tender_amount,
        //                 X.year,
        //                 X.month")
        //             ->from(DB::raw("(SELECT
    
        //             IF(
        //                 (
        //                     ISNULL(
        //                         SUM(bill_returns.tender_amount)
        //                     ) = 1
        //                 ),
        //                 billing.tender_amount,
                        
        //                     (
        //                         billing.tender_amount - SUM(bill_returns.tender_amount)
        //                     )
                        
        //             ) AS tender_amount,
        //             YEAR(billing.created_at) as year, MONTH(billing.created_at) as month
        //         FROM
        //             `billing`
        //         LEFT JOIN `bill_returns` ON `bill_returns`.`billing_id` = `billing`.`id`"))
        //         ->where('billing.tender_amount','>','0.00')
        //         ->groupBy(DB::raw("bill_returns.billing_id,billing.id) X"));
        // $dues = DB::query()
        //         ->selectRaw("SUM(X.due_amount) AS t_due_amount,
        //         X.year,
        //         X.month")
        //         ->from(DB::raw(" (
        //             SELECT
        //                 IF(
        //                     (
        //                         ISNULL(SUM(bill_returns.due_amount)) = 1
        //                     ),
        //                     billing.due_amount,
        //                     (
        //                         billing.due_amount - SUM(bill_returns.due_amount)
        //                     )
        //                 ) AS due_amount,
        //                 YEAR(billing.created_at) AS YEAR,
        //                 MONTH(billing.created_at) AS MONTH
        //             FROM
        //                 `billing`
        //             LEFT JOIN `bill_returns` ON `bill_returns`.`billing_id` = `billing`.`id`"))
        //             ->where('billing.due_amount','>','0.00')
        //             ->groupBy(DB::raw("bill_returns.billing_id,billing.id) X"));
        $collected = $this->billing->selectRaw("SUM(tender_amount) as t_tender_amount, YEAR(created_at) as year, MONTH(created_at) as month")->where("tender_amount", ">", 0)->groupBy('year', 'month')->orderBy('created_at', 'ASC');
        $dues = $this->billing->selectRaw("SUM(due_amount) as t_due_amount, YEAR(created_at) as year, MONTH(created_at) as month")->where("due_amount", ">", 0)->groupBy('year', 'month')->orderBy('created_at', 'ASC');


        $branch_id = $this->authbranchid();

        if ($this->authusertype() == 'branch admin') {
            $collected->whereIn('billing.user_id', $this->user->getAllUserListByBranch($branch_id));
            $dues->whereIn('billing.user_id', $this->user->getAllUserListByBranch($branch_id));
        }

        if ($year) {
            $collected->whereYear('billing.created_at', $year);
            $dues->whereYear('billing.created_at', $year);
        } else {
            $collected->whereYear('billing.created_at', Carbon::now()->year);
            $dues->whereYear('billing.created_at', Carbon::now()->year);
        }

        $collected = $collected->get();
        $dues = $dues->get();

        $collected_count = $collected->count();
        $dues_count = $dues->count();

        for ($i = 1; $i <= 12; $i++) {
            foreach ($dues as $due) {
                if ($due->month == $i) {
                    $data['dueData'][] = $due->t_due_amount;
                    $i++;
                }
            }
            $data['dueData'][] = 0;
        }

        for ($i = 1; $i <= 12; $i++) {
            foreach ($collected as $col) {
                if ($col->month == $i) {
                    $data['colData'][] = $col->t_tender_amount;
                    $i++;
                }
            }
            $data['colData'][] = 0;
        }

        $data['record_count'] = 1;
        if ($collected_count == 0 && $dues_count == 0) {
            $data['record_count'] = 0;
        }

        return json_encode($data);
    }

    public function get_category_wise_total()
    {
        // $category_wise_data = DB::query()
        //                     ->selectRaw("billing_category_id,
        //                     category_title,
        //                     user_id,
        //                     (
        //                     SUM(paidTotal) - SUM(returnTotal)
        //                     ) subtotal")
        //                     ->from(DB::raw("(
        //                         SELECT
        //                         billing.id AS billingId,
        //                         billing_details.billing_category_id AS billing_category_id,
        //                         0 AS returnTotal,
        //                         billing_details.created_at AS last_bill_date,
        //                         billing_details.sub_total AS paidTotal,
        //                         customers.name AS customer_name,
        //                         customers.customer_number,
        //                         users.id as user_id,
        //                         users.name AS collector,
        //                         branches.branch_title AS branch,
        //                         billing_categories.title AS category_title
        //                         FROM
        //                         billing_details
        //                         LEFT JOIN `billing` ON `billing`.`id` = `billing_details`.`billing_id`
        //                         LEFT JOIN `customers` ON `billing`.`customer_id` = `customers`.`id`
        //                         LEFT JOIN `users` ON `billing`.`user_id` = `users`.`id`
        //                         LEFT JOIN branches ON branches.id = users.branch_id
        //                         LEFT JOIN billing_categories ON billing_categories.id = billing_details.billing_category_id
                                
        //                         UNION ALL
                                
        //                         SELECT
        //                         bill_returns.billing_id AS billingId,
        //                         bill_return_details.billing_category_id AS billing_category_id,
        //                         bill_return_details.sub_total AS returnTotal,
        //                         '' AS last_bill_date,
        //                         0 AS paidTotal,
        //                         customers.name AS customer_name,
        //                         customers.customer_number,
        //                         users.id as user_id,
        //                         users.name AS collector,
        //                         branches.branch_title AS branch,
        //                         billing_categories.title AS category_title
        //                         FROM
        //                         bill_return_details
        //                         JOIN bill_returns ON bill_returns.id = bill_return_details.billing_return_id
        //                         LEFT JOIN `billing` ON `billing`.`id` = `bill_returns`.`billing_id`
        //                         LEFT JOIN `customers` ON `billing`.`customer_id` = `customers`.`id`
        //                         LEFT JOIN `users` ON `billing`.`user_id` = `users`.`id`
        //                         LEFT JOIN branches ON branches.id = users.branch_id
        //                         LEFT JOIN billing_categories ON billing_categories.id = bill_return_details.billing_category_id
                                
        //                         ) a"))
        //                         ->where('a.billing_category_id','!=',2)
        //                         ->where('a.billing_category_id','!=',3)
        //                         ->groupBy('a.category_title');

        $category_wise_data =  BillingDetail::selectRaw("billing_details.billing_category_id, billing_categories.title as category_title, SUM(billing_details.sub_total) as subtotal")
            ->join('billing_categories', 'billing_categories.id', 'billing_details.billing_category_id')
            ->join('billing', 'billing.id', 'billing_details.billing_id')
            ->whereNotIn('billing_details.billing_category_id', [2, 3])
            ->groupBy('billing_details.billing_category_id');


        $branch_id = $this->authbranchid();
        if ($this->authusertype() == 'branch admin') {
            $category_wise_data->whereIn('billing.user_id', $this->user->getAllUserListByBranch($branch_id));
        }

        $category_wise_data = $category_wise_data->get();
        $category_wise_count = $category_wise_data->count();

        $data[] = '';
        foreach ($category_wise_data as $cat_data) {
            $data['label'][] = $cat_data->category_title;
            $data['data'][] = $cat_data->subtotal;
        }

        $data['record_count'] = 1;
        if ($category_wise_count == 0) {
            $data['record_count'] = 0;
        }

        return json_encode($data);
    }

    public function get_month_wise_bill_generated($year = null)
    {
        $month_wise_bill_generated = $this->billing->selectRaw("COUNT(billing.id) as generated_count, YEAR(created_at) as year, MONTH(created_at) as month")
            ->where('net_amount', '>', '0')
            ->groupBy('year', 'month')
            ->orderBy('created_at', 'ASC');

        $branch_id = $this->authbranchid();
        if ($this->authusertype() == 'branch admin') {
            $month_wise_bill_generated->whereIn('billing.user_id', $this->user->getAllUserListByBranch($branch_id));
        }

        if ($year) {
            $month_wise_bill_generated->whereYear('created_at', $year);
        } else {
            $month_wise_bill_generated->whereYear('created_at', Carbon::now()->year);
        }

        $month_wise_bill_generated = $month_wise_bill_generated->get();
        $month_wise_count = $month_wise_bill_generated->count();

        for ($i = 1; $i <= 12; $i++) {
            foreach ($month_wise_bill_generated as $month_gen) {
                if ($month_gen->month == $i) {
                    $data['colData'][] = $month_gen->generated_count;
                    $i++;
                }
            }
            $data['colData'][] = 0;
        }

        $data['record_count'] = 1;
        if ($month_wise_count == 0) {
            $data['record_count'] = 0;
        }

        return json_encode($data);
    }

    public function dashboard_report(array $attributes)
    {
        // $data = DB::query()
        //     ->selectRaw(
        //         "X.branch_id,
        //             X.last_bill_date,
        //              X.user_id,
        //             X.user_name,
        //             FORMAT(SUM(X.monthly),
        //             2) AS monthly,
        //             FORMAT(SUM(X.card),
        //             2) AS card,
        //             FORMAT(SUM(X.registration_fee),
        //             2) AS registration_fee,
        //             FORMAT(SUM(festival_charges),
        //             2) AS festival_charges,
        //             FORMAT(SUM(X.Others),
        //             2) AS Others,
        //             FORMAT(SUM(X.total),
        //             2) AS total"
        //     )
        //     ->from(DB::raw("(
        //                 SELECT
        //                     user_id,
        //                     last_bill_date,
        //                     customer_name,
        //                     branch_id,
        //                     user_name,
        //                     CASE WHEN category = 'Monthly' THEN SUM(paidTotal) - SUM(returnTotal) ELSE 0
        //                 END monthly,
        //                 CASE WHEN category = 'Card' THEN SUM(paidTotal) - SUM(returnTotal) ELSE 0.0
        //                 END card,
        //                 CASE WHEN category = 'Registration Fee' THEN SUM(paidTotal) - SUM(returnTotal) ELSE 0.0
        //                 END registration_fee,
        //                 CASE WHEN category = 'Festival Charges' THEN SUM(paidTotal) - SUM(returnTotal) ELSE 0.0
        //                 END festival_charges,
        //                 CASE WHEN category = 'Others' THEN SUM(paidTotal) - SUM(returnTotal) ELSE 0.0
        //                 END Others,
        //                 (SUM(paidTotal) - SUM(returnTotal)) AS total
        //                 FROM
        //                     (
        //                     SELECT
        //                         billing.id AS billingId,
        //                         users.id as user_id,
        //                         billing_details.billing_category_id AS categoryId,
        //                         0 AS returnTotal,
        //                         billing_details.created_at AS last_bill_date,
        //                         billing_details.sub_total AS paidTotal,
        //                         customers.name AS customer_name,
        //                         customers.customer_number,
        //                         users.name AS user_name,
        //                         branches.branch_title AS branch,
        //                         branches.id as branch_id,
        //                         billing_categories.title AS category
        //                     FROM
        //                         billing_details
        //                     LEFT JOIN `billing` ON `billing`.`id` = `billing_details`.`billing_id`
        //                     LEFT JOIN `customers` ON `billing`.`customer_id` = `customers`.`id`
        //                     LEFT JOIN `users` ON `billing`.`user_id` = `users`.`id`
        //                     LEFT JOIN branches ON branches.id = users.branch_id
        //                     LEFT JOIN billing_categories ON billing_categories.id = billing_details.billing_category_id
        //                     UNION ALL
        //                 SELECT
        //                     bill_returns.billing_id AS billingId,
        //                          users.id as user_id,
        //                     bill_return_details.billing_category_id AS categoryId,
        //                     bill_return_details.sub_total AS returnTotal,
        //                     bill_return_details.created_at AS last_bill_date,
        //                     0 AS paidTotal,
        //                     customers.name AS customer_name,
        //                     customers.customer_number,
        //                     users.name AS user_name,
        //                     branches.branch_title AS branch,
        //                     branches.id as branch_id,
        //                     billing_categories.title AS category
        //                 FROM
        //                     bill_return_details
        //                 JOIN bill_returns ON bill_returns.id = bill_return_details.billing_return_id
        //                 LEFT JOIN `billing` ON `billing`.`id` = `bill_returns`.`billing_id`
        //                 LEFT JOIN `customers` ON `billing`.`customer_id` = `customers`.`id`
        //                 LEFT JOIN `users` ON `billing`.`user_id` = `users`.`id`
        //                 LEFT JOIN branches ON branches.id = users.branch_id
        //                 LEFT JOIN billing_categories ON billing_categories.id = bill_return_details.billing_category_id
        //                 ) a"))
        //                 ->where('a.categoryId', '!=', 2)
        //                 ->where('a.categoryId', '!=', 3);

        // if ($this->authusertype() == 'branch admin') {
        //     $branchId = $this->authbranchid();
        //     $data->whereRaw(DB::raw("a.branch_id = '$branchId'"));
        // }

        // $from = '';
        // $to = '';

        // if ($attributes) {

        //     if (array_key_exists('from', $attributes)) {
        //         $from = $attributes['from'];
        //     }
        //     if (array_key_exists('to', $attributes)) {
        //         $to    = $attributes['to'];
        //     }
        //     if ($from) {
        //         if ($from && $to) {

        //             $data->whereDate('a.last_bill_date', '>=', $this->convert_date_to_english($from));
        //             $data->whereDate('a.last_bill_date', '<=', $this->convert_date_to_english($to));
        //         } else if ($to) {
        //             $data->whereDate('a.last_bill_date', '<=', $this->convert_date_to_english($to));
        //         } else {
        //             $data->whereDate('a.last_bill_date', '>=', $this->convert_date_to_english($from));
        //             $data->whereDate('a.last_bill_date', '<=', date('Y-m-d'));
        //         }
        //     }

        //     if (array_key_exists('user_id', $attributes)) {
        //         if ($attributes['user_id'] != '') {
        //             $user_id = $attributes['user_id'];
        //             $data->whereRaw(DB::raw("a.user_id = '$user_id'"));
        //         }
        //     }

        //     if (array_key_exists('branch_id', $attributes)) {
        //         if ($attributes['branch_id'] != '') {
        //             $branch_id = $attributes['branch_id'];
        //             $data->whereRaw(DB::raw("a.branch_id = '$branch_id'"));
        //         }
        //     }
        // }

        // $data = $data->groupBy(DB::raw("a.billingId,a.categoryId) X group by X.user_id,X.user_name"));

        // return $data;

        $data =  DB::table('billing_details as bd')
            ->select(
                "X.user_id",
                "X.user_name",
                DB::raw(" format(SUM(X.monthly),2) AS monthly,format(SUM(X.card),2) AS card,format(SUM(X.registration_fee),2) AS registration_fee,format(SUM(festival_charges),2) AS festival_charges, format( SUM(X.Others),2) AS Others,format( SUM(X.total),2) AS total from (select u.id AS user_id,u.name user_name"),
                DB::raw("CASE WHEN title = 'Monthly' THEN  SUM(sub_total) ELSE 0 END monthly"),
                DB::raw("CASE WHEN title = 'Card' THEN SUM(sub_total) ELSE 0.0 END card"),
                DB::raw("CASE WHEN title = 'Registration Fee' THEN SUM(sub_total) ELSE 0.0 END registration_fee"),
                DB::raw("CASE WHEN title = 'Festival Charges' THEN SUM(sub_total) ELSE 0.0 END festival_charges"),
                DB::raw("CASE WHEN title = 'Others' THEN SUM(sub_total) ELSE 0.0 END Others"),
                DB::raw("(SUM(sub_total)) AS total")

            )

            ->leftJoin('billing_categories as bc', 'bc.id', 'bd.billing_category_id')
            ->leftjoin('billing as b', 'b.id', 'bd.billing_id')
            ->leftjoin('users as u', 'u.id', 'b.user_id')
            ->where('bd.billing_category_id','!=',2)
            ->where('bd.billing_category_id','!=',3);

        if ($this->authusertype() == 'branch admin') {
            $branchId = $this->authbranchid();
            $data->whereRaw(DB::raw("u.branch_id = '$branchId'"));
        }

        $from = '';
        $to = '';

        if ($attributes) {

            if (array_key_exists('from', $attributes)) {
                $from = $attributes['from'];
            }
            if (array_key_exists('to', $attributes)) {
                $to    = $attributes['to'];
            }
            if ($from) {
                if ($from && $to) {

                    $data->whereDate('bd.created_at', '>=', $this->convert_date_to_english($from));
                    $data->whereDate('bd.created_at', '<=', $this->convert_date_to_english($to));
                } else if ($to) {
                    $data->whereDate('bd.created_at', '<=', $this->convert_date_to_english($to));
                } else {
                    $data->whereDate('bd.created_at', '>=', $this->convert_date_to_english($from));
                    $data->whereDate('bd.created_at', '<=', date('Y-m-d H:i:s'));
                }
            }

            if (array_key_exists('user_id', $attributes)) {
                if ($attributes['user_id'] != '') {
                    $user_id = $attributes['user_id'];
                    $data->whereRaw(DB::raw("b.user_id = '$user_id'"));
                }
            }

            if (array_key_exists('branch_id', $attributes)) {
                if ($attributes['branch_id'] != '') {
                    $branch_id = $attributes['branch_id'];
                    $data->whereRaw(DB::raw("u.branch_id = '$branch_id'"));
                }
            }
        }

        $data = $data->groupBy(DB::raw("u.id, bc.title, u.name)X group by X.user_id,X.user_name"));

        return $data;
    }

    public function getAllIndexItems()
    {
        $data = [
            'total_bill'         => $this->total_bill_collected(),
            'bill_count'         => $this->total_bill_generated(),
            'all_collectors'     =>  $this->get_all_collector(),
            'all_customer'        => $this->get_all_customers(),
            'current_date'        => $this->convert_date_to_nepali(date('Y-m-d')),
            'all_branch'        => Branch::getAllBranch(),
            'my_users'            => $this->user->get_my_user_by_branch(),
            'total_bill_till_date' => $this->total_bill_collected_till_date(),
            'total_bill_generate' => $this->total_bill_generated_till_date(),
            'due_and_collected' => $this->get_due_and_collected(),
            'category_wise_collected' => $this->get_category_wise_total(),
            'month_wise_bill_generated' => $this->get_month_wise_bill_generated(),
            'total_bill_return' => $this->total_bill_returned(),
            'total_bill_return_today' => $this->total_bill_returned_today()
        ];
        return $data;
    }

    public function total_bill_returned()
    {
        $convertedDate = $this->current_date_to_nepali() . ' ' . date('H:i:s');
        $currentFiscalYear = $this->fiscalYear($convertedDate);
        $branch_id         = $this->authbranchid();

        $query             = $this->billReturn->query()->where('fiscal_year', $currentFiscalYear->id);

        if ($this->authusertype() == 'branch admin') {
            $query->whereIn('user_id', $this->user->getAllUserListByBranch($branch_id));
        }

        return $query->sum("tender_amount");
    }

    public function total_bill_returned_today()
    {
        $convertedDate = $this->current_date_to_nepali() . ' ' . date('H:i:s');
        $currentFiscalYear = $this->fiscalYear($convertedDate);
        $branch_id         = $this->authbranchid();

        $query             = $this->billReturn->query()->whereRaw('date(created_at) = CURDATE()');

        if ($this->authusertype() == 'branch admin') {
            $query->whereIn('user_id', $this->user->getAllUserListByBranch($branch_id));
        }

        return $query->sum("tender_amount");
    }
}
