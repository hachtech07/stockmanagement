<?php

namespace App\Repository;

use App\Models\User;
use App\Repository\BaseRepository;
use App\Interfaces\UserRepositoryInterface;
use App\Traits\AuthTrait;
use Illuminate\Support\Collection;

class UserRepository extends BaseRepository implements UserRepositoryInterface
{

    /**
     * UserRepository constructor.
     *
     * @param User $model
     */
    use AuthTrait;
    public function __construct(User $model)
    {
        parent::__construct($model);
    }

    /**
     * @return Collection
     */
    public function all(): Collection
    {
        return $this->model->all();
    }

    public function getById()
    {
        return $this->model->where('id', '>', 1)->where('id', '!=', $this->authId())->get();
    }

    public function getUserInfo()
    {
        $user = $this->model->selectRaw("users.id ,name,user_type, email, branches.branch_title, users.status, users.user_type")->join('branches', 'users.branch_id', '=', 'branches.id')->where('users.id', '!=', $this->authId());
        if (auth()->user()->user_type == 'branch admin') {
            $user->where('users.branch_id', auth()->user()->branch_id)->where('users.user_type', 'collector');
        }

        $data = $user->get();
        return  $data;
    }

    public function findById($id)
    {
        $user = $this->model->find($id);
        return $user;
    }

    public function authenticatedUser(){
        $user = $this->authuser();
        return $user;
    }

    public function create(array $attributes, $id = null)
    {
        if ($id != null || !empty($id)) {
            $user = $this->model->find($id);
            return $user->update($attributes);
        }
        return $this->model->create([
            'first_name'=>$attributes['first_name'],
            'last_name'=>$attributes['last_name'],
            'username'=>$attributes['username'],
            'password'  =>$attributes['password'],
            'status'    =>$attributes['status']

        ]);
    }

    public function updateUserPassword(array $attributes, $id){
        $user = $this->model->find($id);
        return $user->update([
            'password' => $attributes['password']
        ]);
    }
}
