<?php

namespace App\Repository;

use App\Exceptions\CustomException;
use App\Interfaces\SalesInterface;
use App\Models\Fiscal_year;
use App\Models\Product;
use App\Models\ProductCategory;
use App\Models\ProductSubCategory;
use App\Models\Sales;
use App\Models\User;
use App\Repository\BaseRepository;
use App\Traits\AuthTrait;
use App\Traits\DateConveterTrait;
use App\Traits\FiscalYear;
use Illuminate\Support\Facades\Auth;

class SalesRepository extends BaseRepository implements SalesInterface
{

    use DateConveterTrait, AuthTrait, FiscalYear;
    public function __construct(Sales $model,ProductCategory $productCategory,Fiscal_year $fiscalYear,User $user,Product $product,ProductSubCategory $productSubCategory)
    {
        parent::__construct($model);
        $this->productCategory = $productCategory;
        $this->fiscalYear = $fiscalYear;
        $this->user       = $user;
        $this->product = $product;
        $this->$productSubCategory = $productSubCategory;
    }

    public function create($attributes)
    {
        $product = $this->product->findById($attributes['product_id']);
    // dd($product);
        if(!$product){
            throw new CustomException('Product does not exist');
        }
        $convertedDate = $this->current_date_to_nepali() . ' ' . date('H:i:s');
        $fiscalYear = $this->fiscalYear($convertedDate);
        $attribute = [];
        if($attributes['quantity'] > $product->unit  ){
            throw new CustomException('You can not sell more than you have');
        }
        if($attributes['quantity'] == 0 && $product->unit == 0  ){
            throw new CustomException('Nothing left to sale please restock');
        }
        if($attributes['quantity'] <= 0){
            throw new CustomException('Please increase the quantity ');
        }
        $attribute['sold_by'] = Auth::id();
        $attribute['fiscal_year_id'] = $fiscalYear->id;
        $attribute['product_id'] = $product->id;
        $attribute['product_category'] = $product->product_category_id;
        $attribute['product_sub_category'] = $product->product_sub_category_id;
        // $attribute['entry_date'] = $convertedDate;
        $products = array_merge($attribute,$attributes);
        // dd($attribute);
        $product->unit = $product->unit - $attributes['quantity'];
        $product->save();
        return $this->model->create($products);
    }

    public function findById($id)
    {
        $product = $this->model->find($id);
        return $product;
    }

    public function getSales()
    {
        $product = $this->model
                        ->select('sales.id','sales.product_name','product_categories.name as category_name','sub_category.subcategory_name as sub_category','sales.color','sales.model','sales.mrp','fiscal_year.fiscal_years','sales.batch_no','sales.quantity','users.first_name')
                        ->leftjoin('product_categories','product_categories.id','sales.product_category')
                        ->leftjoin('fiscal_year','fiscal_year.id','sales.fiscal_year_id')
                        ->leftjoin('sub_category','sub_category.id','sales.product_sub_category')
                        ->leftjoin('users','users.id','sales.sold_by');
        return $product;
    }

    public function findAll($attributes)
    {
        $categories = $this->productCategory->get_product_categories();
        $sales = $this->getSales();
        $fiscal_years = $this->fiscalYear->listFiscalYears();
        $batchs       = $this->model->getBatchList();
        $users        = $this->user->getAllUser();
        $selectedSubCategory= [];
        $froms = '';
        $tos = '';
        $fiscal_year = '';
        $product_name = '';
        $product_category = '';
        $batch_nos         = '';
        $sub_category       = '';

        if ($attributes) {
            if (array_key_exists('from', $attributes)) {
                $froms = $attributes['from'];
            }
            if (array_key_exists('fiscal_year', $attributes)) {
                $fiscal_year = $attributes['fiscal_year'];
            }
            if (array_key_exists('to', $attributes)) {
                $tos    = $attributes['to'];
            }
            if (array_key_exists('batch', $attributes)) {
                $batch_nos = $attributes['batch'];
            }
            if (array_key_exists('category_id', $attributes)) {
                $product_category = $attributes['category_id'];
            }
            if (array_key_exists('product_name', $attributes)) {
                $product_name = $attributes['product_name'];
            }
            if (array_key_exists('product_sub_category_id', $attributes)) {
                $sub_category = $attributes['product_sub_category_id'];
            }
            if ($froms) {
                if ($froms && $tos) {
                    $sales->whereDate('sales.created_at', '>=', $this->convert_date_to_english($froms));
                    $sales->whereDate('sales.created_at', '<=', $this->convert_date_to_english($tos));
                } else if ($tos) {
                    $sales->whereDate('sales.created_at', '<=', $this->convert_date_to_english($tos));
                } else {
                    $sales->whereDate('sales.created_at', '>=', $this->convert_date_to_english($froms));
                    $sales->whereDate('sales.created_at', '<=', date('Y-m-d H:i:s'));
                }
            }

            if ($batch_nos != '') {
                $sales->where('sales.batch_no', $batch_nos);
            }
            if ($product_category != '') {
                $selectedSubCategory = $this->productSubCategory->where('category_id',$product_category)->get();
                $sales->where('sales.product_category', $product_category);
            }
            if($sub_category !=null){
                $sales->where('sales.product_sub_category', $sub_category);
            }
            if ($product_name != '') {
                // $sales->where('sales.product_name', $product_name);
                $sales->where('sales.product_name', 'LIKE', '%' . $product_name . '%');
            }
            if($fiscal_year !=null){
                $sales->where('fiscal_year_id', $fiscal_year);

            }
        }

        $sal = $sales->orderBy('sales.created_at', 'DESC')->paginate('5');
        $productIndex = [
            'sales'=>$sal,
            'categories'=>$categories,
            'categorys'=>$product_category,
            'fiscal_years'=>$fiscal_years,
            'product_name'=>$product_name,
            'users'       =>$users,
            'batchs'      =>$batchs,
            'batch_nos'   =>$batch_nos,
            'fiscal_year' =>$fiscal_year,
            'tos'         =>$tos,
            'froms'        =>$froms,
            'sub_category'=>$sub_category,
            'selectedSubCategories'=>$selectedSubCategory
        ];
        return $productIndex;
    }


}