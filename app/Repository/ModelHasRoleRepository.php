<?php

namespace App\Repository;

use App\Interfaces\ModelHasRoleInterface;
use App\Models\ModelHasRole;
use App\Models\User;
use App\Repository\BaseRepository;

class ModelHasRoleRepository extends BaseRepository implements ModelHasRoleInterface
{

    /**
     * UserRepository constructor.
     *
     * @param User $model
     */
    public function __construct(ModelHasRole $model)
    {
        parent::__construct($model);
    }

    public function create(array $attributes)
    {
        return $this->model->create($attributes);
    }
}

// [
//     'model_id'   => $attributes['id'],
//     'model_type' => 'App\models\User',
//     'role_id'    => $attributes['role_id']
// ]