<?php

namespace App\Repository;

use App\Interfaces\ProductSubCategoryInterface;
use App\Models\ProductCategory;
use App\Models\ProductSubCategory;
use App\Repository\BaseRepository;

class ProductSubCategoryRepository extends BaseRepository implements ProductSubCategoryInterface
{

    public function __construct(ProductSubCategory $model)
    {
        parent::__construct($model);
    }

    public function createSubCategory($attributes, $id = null)
    {
        return $this->model->create($attributes);
    }

    public function listSubCategory($id)
    {
        return $this->model->getSubCategories($id);
    }
}