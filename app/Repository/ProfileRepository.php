<?php

namespace App\Repository;

use App\Exceptions\CustomException;
use App\Repository\BaseRepository;
use App\Interfaces\ProfileInterface;
use App\Models\User;
use Illuminate\Support\Facades\Hash;

class ProfileRepository extends BaseRepository implements ProfileInterface
{

    /**
     * UserRepository constructor.
     *
     * @param User $model
     */
    public function __construct(User $model)
    {
        parent::__construct($model);
    }

    /**
     * @return Collection
     */
    

     public function update(array $attributes ,$id)
     {
         $user = $this->model->find($id);
         return $user->update([
             'name' => $attributes['name']
         ]);
         
     }
    public function changeUserPassword(array $attributes, $id){
        $user = $this->model->find($id);
        if(!Hash::check($attributes['old_password'],$user->password)){
            throw  new CustomException('Old password did not match');
        }
        return $user->update([
            'password' => $attributes['password']
        ]);
    }
}
