<?php

namespace App\Repository;

use App\Exceptions\CustomException;
use App\Interfaces\ProductInterface;
use App\Models\Fiscal_year;
use App\Models\Product;
use App\Models\ProductCategory;
use App\Models\ProductSubCategory;
use App\Models\User;
use App\Repository\BaseRepository;
use App\Traits\AuthTrait;
use App\Traits\DateConveterTrait;
use App\Traits\FiscalYear;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class ProductRepository extends BaseRepository implements ProductInterface
{

    use DateConveterTrait, AuthTrait, FiscalYear;
    public function __construct(Product $model,ProductCategory $productCategory,Fiscal_year $fiscalYear,User $user,ProductSubCategory $productSubCategory)
    {
        parent::__construct($model);
        $this->productCategory = $productCategory;
        $this->fiscalYear = $fiscalYear;
        $this->user       = $user;
        $this->productSubCategory = $productSubCategory;
    }

    public function create($attributes)
    {
        $convertedDate = $this->current_date_to_nepali() . ' ' . date('H:i:s');
        $fiscal_year = $this->fiscalYear($convertedDate);
        // dd($fiscal_year);
        $attribute = [];
        if($attributes['stock_alert'] >  $attributes['unit']){
            throw new CustomException('Stock alert  cannot be greater than stock');
        }
        // $attribute['product_category_id'] = $attributes['product_sub_category_id'];
        // $attribute['created_by'] = Auth::id();
        // $attribute['fiscal_year_id'] = $fiscal_year->id;
        // $attribute['entry_date'] = $convertedDate;
        // $originalAttribute = $attributes;
        // unset($originalAttribute['product_category_id']);
        // $product = array_merge($attribute,$originalAttribute);
        // dd($product);
        $product =[
            'product_name'=>$attributes['product_name'],
            'product_category_id'=>$attributes['product_category_id'],
            'product_sub_category_id'=>$attributes['product_sub_category_id'],
            'color'              =>$attributes['color'],
            'model'              =>$attributes['model'],
            'mrp'                =>$attributes['mrp'],
            'cp'                 =>$attributes['cp'],
            'stock_alert'        =>$attributes['stock_alert'],
            'fiscal_year_id'     =>$fiscal_year->id,
            'created_by'         =>Auth::id(),
            'unit'               =>$attributes['unit'],
            'entry_date'         =>$convertedDate,
            'batch_no'           =>$attributes['batch_no']

        ];
        // dd($product);
        return $this->model->create($product);
    }

    public function findById($id)
    {
        $product = $this->model->find($id);
        return $product;
    }

    public function getProducts()
    {
        $product = $this->model
                        ->select('products.id','products.product_name','product_categories.name as category_name','sub_category.subcategory_name as sub_category','products.color','products.model','products.mrp','products.cp','products.entry_date','products.created_at','fiscal_year.fiscal_years','products.batch_no','products.unit','products.stock_alert','users.first_name')
                        ->leftjoin('product_categories','product_categories.id','products.product_category_id')
                        ->leftjoin('fiscal_year','fiscal_year.id','products.fiscal_year_id')
                        ->leftjoin('sub_category','sub_category.id','products.product_sub_category_id')
                        ->leftjoin('users','users.id','products.created_by');
        // $product = DB::table('product_categories as cat')
        //                 ->selectRaw("products.product_name,
        //                 products.id,
        //                 products.color,
        //                 products.model,
        //                 products.mrp,
        //                 products.cp,
        //                 products.unit,
        //                 products.created_at,
        //                 products.stock_alert,
        //                 products.entry_date,
        //                 products.batch_no,
        //                 fiscal_year.fiscal_years,
        //                 parent.name AS category,
        //                 cat.name AS subcategory,
        //                 cat.id as sub_id,
        //                 parent.id as parent_id")
        //                 ->leftJoin('product_categories as parent','parent.id','cat.parent_id')
        //                 ->leftJoin('products','products.product_category_id','parent.id')
        //                 ->leftJoin('fiscal_year','fiscal_year.id','products.fiscal_year_id')
        //                 ->whereRaw(DB::raw("product_name IS NOT NULL"));
        return $product;
    }

    public function findAll($attributes)
    {
        $categories = $this->productCategory->getCategories();
        $products = $this->getProducts();
        // dd($products->get());
        $fiscal_years = $this->fiscalYear->listFiscalYears();
        $batchs       = $this->model->getBatchList();
        $users        = $this->user->getAllUser();
        
        $selectedSubCategory = [];
        $froms = '';
        $tos = '';
        $fiscal_year = '';
        $product_name = '';
        $product_category = '';
        $sub_category     = '';
        $batch_nos         = '';

        if ($attributes) {
            if (array_key_exists('from', $attributes)) {
                $froms = $attributes['from'];
            }
            if (array_key_exists('fiscal_year', $attributes)) {
                $fiscal_year = $attributes['fiscal_year'];
            }
            if (array_key_exists('to', $attributes)) {
                $tos    = $attributes['to'];
            }
            if (array_key_exists('batch', $attributes)) {
                $batch_nos = $attributes['batch'];
            }
            if (array_key_exists('category_id', $attributes)) {
                $product_category = $attributes['category_id'];
            }
            if (array_key_exists('product_name', $attributes)) {
                $product_name = $attributes['product_name'];
            }
            if (array_key_exists('product_sub_category_id', $attributes)) {
                $sub_category = $attributes['product_sub_category_id'];
            }
            if ($froms) {
                if ($froms && $tos) {
                    $products->whereDate('products.created_at', '>=', $this->convert_date_to_english($froms));
                    $products->whereDate('products.created_at', '<=', $this->convert_date_to_english($tos));
                } else if ($tos) {
                    $products->whereDate('products.created_at', '<=', $this->convert_date_to_english($tos));
                } else {
                    $products->whereDate('products.created_at', '>=', $this->convert_date_to_english($froms));
                    $products->whereDate('products.created_at', '<=', date('Y-m-d H:i:s'));
                }
            }

            if ($batch_nos != '') {
                $products->where('products.batch_no', $batch_nos);
            }
            if ($product_category != '') {
                $selectedSubCategory = $this->productSubCategory->where('category_id',$product_category)->get();
                $products->where('products.product_category_id', $product_category);
            }
            if($sub_category !=null){
                $products->where('products.product_sub_category_id', $sub_category);
            }
            if ($product_name != '') {
                // $products->where('products.product_name', $product_name);
                $products->where('products.product_name', 'LIKE', '%' . $product_name . '%');
            }
            if($fiscal_year !=null){
                $products->where('fiscal_year_id', $fiscal_year);

            }
        }

        $pro = $products->groupBy('products.id')->orderBy('products.created_at', 'DESC')->paginate('5');
        $productIndex = [
            'purchases'=>$pro,
            'categories'=>$categories,
            'categorys'=>$product_category,
            'fiscal_years'=>$fiscal_years,
            'product_name'=>$product_name,
            'users'       =>$users,
            'batchs'      =>$batchs,
            'batch_nos'   =>$batch_nos,
            'fiscal_year' =>$fiscal_year,
            'tos'         =>$tos,
            'froms'        =>$froms,
            'selectedSubCategories'=>$selectedSubCategory,
            'sub_category'=>$sub_category
        ];
        return $productIndex;
    }

    public function updateStock($attributes,$id)
    {
        $product = $this->findById($id);
        if($attributes['unit'] < $product->unit){
            throw new CustomException('Unit can not less than existing one');
        }

        if($attributes['stock_alert'] >  $attributes['unit']){
            throw new CustomException('Stock alert  cannot be greater than stock');
        }
        return $product->update($attributes); 

    }
    public function findProductWithCategory($id)
    {
        $product = $this->model->select('products.*','product_categories.name as product_category','sub_category.subcategory_name as sub_category_name')
                                ->leftjoin('product_categories','product_categories.id','products.product_category_id')
                                ->leftjoin('sub_category','sub_category.id','products.product_sub_category_id')
                                ->where('products.id',$id)
                                ->first();
        return $product;
    }


}