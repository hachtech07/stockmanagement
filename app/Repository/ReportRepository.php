<?php

namespace App\Repository;

use App\Models\Billing;
use App\Models\Customer;
use App\Repository\BaseRepository;
use App\Interfaces\ReportInterface;
use App\Models\Bill_return;
use App\Models\Bill_return_details;
use App\Models\BillingDetail;
use App\Traits\DateConveterTrait;
use Illuminate\Support\Facades\Config;
use App\Models\Branch;
use App\Models\User;
use App\Models\BillingCategory;
use App\Models\Fiscal_year;
use App\Traits\AuthTrait;
use App\Traits\FiscalYear;
use Illuminate\Support\Facades\DB;
use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;

class ReportRepository extends BaseRepository implements ReportInterface
{
    /**
     * UserRepository constructor.
     *
     * @param User $model
     */
    use DateConveterTrait, AuthTrait, FiscalYear;
    private $billing;
    private $customer;
    private $billingDetail;
    private $user;
    private $branch;
    private $billReturn;
    private $billReturnDetail;

    public function __construct(Billing $billing, Customer $customer, BillingDetail $billingDetail, User $user, Branch $branch, Fiscal_year $fiscal_year, Bill_return $billReturn, Bill_return_details $billReturnDetail)
    {
        $this->billing = $billing;
        $this->billingDetail = $billingDetail;
        $this->customer = $customer;
        $this->user     = $user;
        $this->branch   = $branch;
        $this->fiscal_year = $fiscal_year;
        $this->billReturn = $billReturn;
        $this->billReturnDetail = $billReturnDetail;
    }

    /**
     * @return Collection
     */

    public function getDiscountReport()
    {
        return $this->billing->selectRaw("customers.name as customer_name,customers.id as customer_id,branches.branch_title as branch, users.name as collector_name, billing.date_np, format(billing.gross_amount,2) as gross_amount, format(billing.discount_amount,2) as discount_amount, format(billing.vat_amount,2) as vat_amount,format(billing.net_amount,2) as net_amount, format(billing.tender_amount,2) as tender_amount,billing.discount_percent, billing.created_at")
            ->join('customers', 'billing.customer_id', '=', 'customers.id')
            ->join('users', 'billing.user_id', '=', 'users.id')
            ->join('branches', 'branches.id', 'users.branch_id')
            ->where('billing.discount_amount', '>', '0.00');
    }

    public function getDueReport()
    {
        $date = $this->convert_date_to_nepali(date(('Y-m-d')));
        $date_explode = explode("-", $date);
        $year = $date_explode[0];
        $month = $date_explode[1];
        // return $this->customer->selectRaw("
        //                 customer_number,
        //                 customers.name as name,
        //                 customers.id as customer_id,
        //                 customers.address,
        //                 customers.monthly_payable,
        //                 customers.due_amount as due,
        //                 municipality,
        //                 ward,
        //                 branches.branch_title as branch,
        //                 users.name as collector_name,
        //                     (
        //                         IF(
        //                             (
        //                                 (YEAR(last_bill_date) > '$year') OR(
        //                                 YEAR(last_bill_date) = '$year' AND MONTH(last_bill_date) >= '$month'
        //                             )
        //                         ),
        //                                 0,
        //                             (
        //                                 IF(
        //                                         YEAR(last_bill_date) < '$year',
        //                                     (
        //                                         (12 - MONTH(last_bill_date) +'$month') * monthly_payable
        //                                     ),
        //                             (
        //                                     ('$month' - MONTH(`last_bill_date`)) * monthly_payable
        //                                     )
        //                             )
        //                         )
        //                         ) + due_amount
        //                     ) AS due_amount,
        //                     last_bill_date")
        //     ->leftjoin('branches', 'branches.id', 'customers.branch_id')
        //     ->leftjoin('users', 'users.id', 'customers.collector_id')
        //     ->where('due_amount','>','0');
        $due =  DB::query()
                ->selectRaw("customer_number,
                name,
                customer_id,
                branch_id,
                collector_id,
                address,
                monthly_payable,
                last_bill_date,
                IF(due >= 0,due,0) AS due,
                advance_amount,
                municipality,
                ward,
                branch,
                collector_name,
                IF((due_amount < 0),advance_amount- due_amount,due_amount) as due_amount,
                last_bill_date")
                ->from(DB::raw("(SELECT
                customer_number,
                customers.branch_id,
                customers.collector_id,
                customers.name AS name,
                customers.id AS customer_id,
                customers.address,
                customers.monthly_payable,
                customers.due_amount AS due,
                customers.advance_amount as advance_amount,
                municipality,
                ward,
                branches.branch_title AS branch,
                users.name AS collector_name,
                (((2079 - YEAR(last_bill_date)) * 12) - MONTH(last_bill_date) + 2 ) as total_payable_month,
                
                 (
                IF(
                (
                (YEAR(last_bill_date) > '2079') OR(
                YEAR(last_bill_date) = '2079' AND MONTH(last_bill_date) >= '2'
                )
                ),
                0,
                (
                IF(
                YEAR(last_bill_date) < '2079',
                (
                (((2079 - YEAR(last_bill_date)) * 12) - MONTH(last_bill_date) + 2 ) * monthly_payable
                ),
                (
                ('2' - MONTH(`last_bill_date`)) * monthly_payable
                )
                )
                )
                ) + due_amount
                ) AS due_amount,
                last_bill_date
                FROM
                customers
                LEFT JOIN branches ON branches.id = customers.branch_id
                LEFT JOIN users ON users.id = customers.collector_id
                  )a"));
        return $due;
    }

    public function getCollectionReport()
    {
        // return $this->billing->select(
        //     "billing.id",
        //     "customers.name AS customer_name",
        //     "customers.id as customer_id",
        //     "customers.phone_number AS phone_number",
        //     "customers.customer_number AS customer_number",
        // "billing.previous_due_amount",
        // "billing.previous_advance_amount",
        //     "billing.from_year",
        //     "billing.from_month",
        //     "billing.to_year",
        //     "billing.date_np",
        //     "billing.to_month",
        //     DB::raw("IF(
        //         (ISNULL(
        //             SUM(bill_returns.gross_amount)
        //         )=1),

        //         format(billing.gross_amount,2),
        //         format((billing.gross_amount - SUM(bill_returns.gross_amount)),2)
        //     ) AS gross_amount,
        //     IF(
        //         (ISNULL(
        //             SUM(bill_returns.tender_amount)
        //         )=1),

        //         format(billing.total_amount,2),
        //         format((billing.total_amount - SUM(bill_returns.tender_amount)),2)
        //     ) AS net_amount,
        //     IF(
        //         (ISNULL(
        //             SUM(bill_returns.vat_amount)
        //         )=1),

        //         format(billing.vat_amount,2),
        //         format((billing.vat_amount - SUM(bill_returns.vat_amount)),2)
        //     ) AS vat_amount,
        //     IF(
        //         (ISNULL(
        //             SUM(bill_returns.tender_amount)
        //         )=1),

        //         format(billing.tender_amount,2),
        //         format((billing.tender_amount - SUM(bill_returns.tender_amount)),2)
        //     ) AS tender_amount,
        //     IF(
        //         (ISNULL(
        //             SUM(bill_returns.advance_amount)
        //         )=1),

        //         format(billing.advance_amount,2),
        //         format((billing.advance_amount - SUM(bill_returns.advance_amount)),2)
        //     ) AS advance_amount,
        //     IF(
        //         (ISNULL(
        //             SUM(bill_returns.due_amount)
        //         )=1),

        //         format(billing.due_amount,2),
        //         format((billing.due_amount - SUM(bill_returns.due_amount)),2)
        //     ) AS due_amount,
        //     IF(

        //         (ISNULL(
        //             SUM(bill_returns.discount_amount)
        //         )=1),

        //         format(billing.discount_amount,2),
        //         (billing.discount_amount - SUM(bill_returns.discount_amount)
        //         )
        //     ) AS discount_amount"),
        //     "billing.created_at"
        // )
        //     ->leftJoin('customers', 'customers.id', 'billing.customer_id')
        //     ->leftJoin('users', 'users.id', 'billing.user_id')
        //     ->leftJoin('branches', 'branches.id', 'users.branch_id')
        //     ->leftJoin('bill_returns', 'bill_returns.billing_id', 'billing.id')
        // ->groupBy(DB::raw("bill_returns.billing_id,billing.id"))
        // ->where('billing.net_amount', '>', '0.00');
        return $this->billing->selectRaw("customers.name as customer_name,customers.phone_number as phone_number,customers.customer_number as customer_number, 
            billing.from_year, billing.to_year, billing.date_np, billing.to_month, billing.from_month, format(billing.gross_amount,2) as gross_amount, 
            format(billing.discount_amount,2) as discount_amount, format(billing.net_amount,2) as net_amount, format(billing.tender_amount,2) as tender_amount, 
            format(billing.due_amount,2) as due_amount,format(billing.advance_amount,2) as advance_amount, billing.created_at, previous_due_amount, previous_advance_amount, billing.vat_amount, bill_returns.id as bill_return_id")
        ->join('customers', 'billing.customer_id', '=', 'customers.id')
        ->join('users', 'billing.user_id', '=', 'users.id')
        ->join('branches', 'branches.id', 'users.branch_id')
        ->leftJoin('bill_returns', 'bill_returns.billing_id', 'billing.id')
        ->where('billing.net_amount', ">", '0.00');
    }

    public function getAdvanceReport()
    {
        return $this->customer->select('customer_number', 'name', 'phone_number', 'address', 'municipality', 'ward', 'house_number', 'advance_amount', 'last_bill_date')
            ->where('advance_amount', '>', '0.00');
    }

    public function getCategoryWiseReport()
    {
        // $convertedDate = $this->current_date_to_nepali() . ' ' . date('H:i:s');
        // $currentFiscalYear = $this->fiscalYear($convertedDate);
        // $report = DB::query()
        //     ->selectRaw(
        //         "billingId,
        //                 userId,
        //                 categoryId,
        //                 branchId,
        //                 customer_number,
        //                 customer_id,
        //                 category,
        //                 branch,
        //                 fiscal_year,
        //                 last_bill_date,
        //                 SUM(paidTotal) billingSubTotal,
        //                 SUM(returnTotal) returnSubTotal,
        //                 (
        //                     SUM(paidTotal) - SUM(returnTotal)
        //                 ) sub_total,
        //                 customer_name,
        //                 collector"
        //     )
        //     ->from(DB::raw("( SELECT
        //                 billing.id AS billingId,
        //                 billing_details.billing_category_id AS categoryId,
        //                 0 AS returnTotal,
        //                 billing_details.created_at AS last_bill_date,
        //                 billing_details.sub_total AS paidTotal,
        //                 users.branch_id as branchId,
        //                 customers.name AS customer_name,
        //                 customers.customer_number as customer_number,
        //                 customers.id as customer_id,
        //                 billing.fiscal_year_id as fiscal_year,
        //                 users.name AS collector,
        //                 billing.user_id as userId,
        //                 branches.branch_title AS branch,
        //                 billing_categories.title AS category
        //             FROM
        //                 billing_details
        //             LEFT JOIN billing ON billing.id = billing_details.billing_id
        //             LEFT JOIN customers ON billing.customer_id = customers.id
        //             LEFT JOIN users ON billing.user_id = users.id
        //             LEFT JOIN branches ON branches.id = users.branch_id
        //             LEFT JOIN billing_categories ON billing_categories.id = billing_details.billing_category_id
        //             LEFT JOIN fiscal_year ON fiscal_year.id = billing.fiscal_year_id
        //             UNION ALL
        //             SELECT
        //                 bill_returns.billing_id AS billingId,
        //                 bill_return_details.billing_category_id AS categoryId,
        //                 bill_return_details.sub_total AS returnTotal,
        //                 '' AS last_bill_date,
        //                 0 AS paidTotal,
        //                 users.branch_id as branchId,
        //                 customers.name AS customer_name,
        //                 customers.customer_number as customer_number,
        //                 customers.id as customer_id,
        //                 bill_returns.fiscal_year as fiscal_year,
        //                 users.name AS collector,
        //                 bill_returns.user_id as userId,
        //                 branches.branch_title AS branch,
        //                 billing_categories.title AS category
        //             FROM
        //                 bill_return_details
        //             JOIN bill_returns ON bill_returns.id = bill_return_details.billing_return_id
        //             LEFT JOIN billing ON billing.id = bill_returns.billing_id
        //             LEFT JOIN customers ON billing.customer_id = customers.id
        //             LEFT JOIN users ON billing.user_id = users.id
        //             LEFT JOIN branches ON branches.id = users.branch_id
        //             LEFT JOIN billing_categories ON billing_categories.id = bill_return_details.billing_category_id
        //             LEFT JOIN fiscal_year ON fiscal_year.id = bill_returns.fiscal_year) a"))
        //     ->where('fiscal_year',$currentFiscalYear->id)
        //     ->where('categoryId', '!=', 2)
        //     ->where('categoryId', '!=', 3);
        return $this->billingDetail->selectRaw(
            "customers.name as customer_name, customers.customer_number,
            users.name as collector, billing_categories.title as category, format(billing_details.sub_total,2) as sub_total,
            billing_details.created_at as last_bill_date, format(billing_details.billing_amount,2) as billing_amount,branches.branch_title as branch"
        )
            ->join('billing', 'billing.id', 'billing_details.billing_id')
            ->join('customers', 'billing.customer_id', '=', 'customers.id')
            ->join('users', 'billing.user_id', '=', 'users.id')
            ->join(
                'billing_categories',
                'billing_categories.id',
                'billing_details.billing_category_id'
            )
            ->join(
                'branches',
                'branches.id',
                'users.branch_id'
            )
        ->where('billing_categories.id', '!=', 3)
        ->where('billing_categories.id', '!=', 2)
        ->where('net_amount', '>', '0.00');
    }

    public function getExportQuery()
    {
        return $this->billing->selectRaw("customers.name as customer_name,branches.branch_title as branch, users.name as collector_name, billing.date_np, format(billing.gross_amount,2) as gross_amount, format(billing.discount_amount,2) as discount_amount, format(billing.net_amount,2) as net_amount, format(billing.tender_amount,2) as tender_amount, billing.created_at")
            ->join('customers', 'billing.customer_id', '=', 'customers.id')
            ->join('users', 'billing.user_id', '=', 'users.id')
            ->join('branches', 'branches.id', 'users.branch_id')
            ->where('billing.discount_amount', '>', '0.00');
    }

    public function getDiscountReportIndex(array $attributes)
    {
        $branches         = $this->branch->getAllBranch();
        $my_users           = $this->user->get_my_user_by_branch();
        $selectusers = [];
        $report = $this->getDiscountReport();

        if ($this->authusertype() == 'branch admin') {
            $report->where('users.branch_id', '=', $this->authbranchid());
        }
        $froms = '';
        $tos = '';
        $filter_user = '';
        $branchs    = '';
        if ($attributes) {
            if (array_key_exists('from', $attributes)) {
                $froms = $attributes['from'];
            }
            if (array_key_exists('to', $attributes)) {
                $tos    = $attributes['to'];
            }
            if (array_key_exists('user_id', $attributes)) {
                $filter_user = $attributes['user_id'];
            }
            if (array_key_exists('branch_id', $attributes)) {
                $branchs    = $attributes['branch_id'];
            }
            if ($froms) {
                if ($froms && $tos) {

                    $report->whereDate('billing.created_at', '>=', $this->convert_date_to_english($froms));
                    $report->whereDate('billing.created_at', '<=', $this->convert_date_to_english($tos));
                } else if ($tos) {
                    $report->whereDate('billing.created_at', '<=', $this->convert_date_to_english($tos));
                } else {
                    $report->whereDate('billing.created_at', '>=', $this->convert_date_to_english($froms));
                    $report->whereDate('billing.created_at', '<=', date('Y-m-d H:i:s'));
                }
            }

            if ($filter_user != '') {
                $report->where('billing.user_id', $filter_user);
            }
            if ($branchs != '') {
                $report->where('users.branch_id', $branchs);
                $selectusers = $this->user->where('branch_id', $branchs)->where('user_type', 'collector')->get();
            }
        }

        $discount_report = $report->orderBy('billing.id', 'DESC')->paginate(Config::get('constant.paginate'));

        $discountReportDetail = [
            'froms' => $froms,
            'tos' => $tos,
            'discount_report' => $discount_report,
            'branches' => $branches,
            'my_users' => $my_users,
            'user' => $filter_user,
            'branchs' => $branchs,
            'selectusers'   => $selectusers
        ];

        return $discountReportDetail;
    }

    public function getDueReportIndex(array $attributes)
    {
        $branches         = $this->branch->getAllBranch();
        $users           = $this->user->get_my_user_by_branch();
        $report =  $this->getDueReport();
        $selectusers = [];
        if ($this->authusertype() == 'branch admin') {
            $report->where('branch_id', '=', $this->authbranchid());
        }

        $froms = '';
        $tos = '';
        $user_id = '';
        $branchs    = '';
        $address = '';

        if ($attributes) {
            if (array_key_exists('to', $attributes)) {
                $tos    = $attributes['to'];
            }
            if (array_key_exists('user_id', $attributes)) {
                $user_id = $attributes['user_id'];
            }
            if (array_key_exists('branch_id', $attributes)) {
                $branchs    = $attributes['branch_id'];
            }
            if (array_key_exists('address', $attributes)) {
                $address    = $attributes['address'];
            }

            if ($tos) {
                $report->whereDate('last_bill_date', '<=', $tos);
            }

            if ($user_id != '') {
                $report->where('collector_id', $user_id);
            }
            if ($branchs != '') {
                $report->where('branch_id', $branchs);
                $selectusers = $this->user->where('branch_id', $branchs)->where('user_type', 'collector')->get();
            }

            if ($address != '') {
                $report->where('address', 'like', '%' . $address . '%');
            }
        }

        $due_reports = $report->orderBy('due_amount', 'ASC')->paginate(Config::get('constant.paginate'));

        $dueReportDetail = [
            'froms' => $froms,
            'tos' => $tos,
            'due_reports' => $due_reports,
            'branches' => $branches,
            'user_id' => $user_id,
            'branchs' => $branchs,
            'users' => $users,
            'selectusers' => $selectusers,
            'address' => $address
        ];

        return $dueReportDetail;
    }

    public function getCategoryWiseReportIndex($attributes)
    {
        $all_branch         = $this->branch->getAllBranch();
        $my_users           = $this->user->get_my_user_by_branch();
        $selectusers = [];
        $fiscal_years       = $this->fiscal_year->listFiscalYears();

        $report = $this->getCategoryWiseReport();
        if ($this->authusertype() == 'branch admin') {
            $report->where('users.branch_id', '=', $this->authbranchid());
        }

        $froms = '';
        $tos = '';
        $categorys = '';
        $filter_user = '';
        $branchs    = '';

        if ($attributes) {
            if (array_key_exists('from', $attributes)) {
                $froms = $attributes['from'];
            }
            if (array_key_exists('fiscal_year', $attributes)) {
                $fiscal_year = $attributes['fiscal_year'];
            }
            if (array_key_exists('to', $attributes)) {
                $tos    = $attributes['to'];
            }
            if (array_key_exists('user_id', $attributes)) {
                $filter_user = $attributes['user_id'];
            }
            if (array_key_exists('branch_id', $attributes)) {
                $branchs    = $attributes['branch_id'];
            }
            if (array_key_exists('category_id', $attributes)) {
                $categorys = $attributes['category_id'];
            }
            if ($froms) {
                if ($froms && $tos) {
                    $report->whereDate('billing_details.created_at', '>=', $this->convert_date_to_english($froms));
                    $report->whereDate('billing_details.created_at', '<=', $this->convert_date_to_english($tos));
                } else if ($tos) {
                    $report->whereDate('billing_details.created_at', '<=', $this->convert_date_to_english($tos));
                } else {
                    $report->whereDate('billing_details.created_at', '>=', $this->convert_date_to_english($froms));
                    $report->whereDate('billing_details.created_at', '<=', date('Y-m-d H:i:s'));
                }
            }

            if ($filter_user != '') {
                $report->where('billing.user_id', $filter_user);
            }
            if ($branchs != '') {
                $report->where('users.branch_id', $branchs);
                $selectusers = $this->user->where('branch_id', $branchs)->where('user_type', 'collector')->get();
            }
            if ($categorys != '') {
                $report->where('billing_details.billing_category_id', $categorys);
            }
        }

        $category_wise_report = $report->groupBy(DB::raw("billing_id,billing_category_id"))->orderBy('billing_id', 'DESC')->paginate(Config::get('constant.paginate'));
        $categories = BillingCategory::where('id', '!=', 3)->where('id', '!=', 2)->where('status', 'active')->get();

        $categoryWiseReportDetail = [
            'froms' => $froms,
            'tos' => $tos,
            'category_wise_report' => $category_wise_report,
            'categories' => $categories,
            'all_branch' => $all_branch,
            'my_users' => $my_users,
            'categorys' => $categorys,
            'branchs' => $branchs,
            'user' => $filter_user,
            'selectusers' => $selectusers
        ];

        return $categoryWiseReportDetail;
    }

    public function getCollectionReportIndex(array $attributes)
    {
        $all_branch         = $this->branch->getAllBranch();
        $my_users           = $this->user->get_my_user_by_branch();
        $fiscal_years       = $this->fiscal_year->listFiscalYears();
        $selectusers = [];

        $report  = $this->getCollectionReport();

        if ($this->authusertype() == 'branch admin') {
            $report->where('users.branch_id', '=', $this->authbranchid());
        }

        $froms = '';
        $tos = '';
        $filter_user = '';
        $branchs    = '';
        $fiscal_year = '';
        if ($attributes) {
            if (array_key_exists('fiscal_year', $attributes)) {
                $fiscal_year = $attributes['fiscal_year'];
            }

            if (array_key_exists('from', $attributes)) {
                $froms = $attributes['from'];
            }
            if (array_key_exists('to', $attributes)) {
                $tos    = $attributes['to'];
            }
            if (array_key_exists('user_id', $attributes)) {
                $filter_user = $attributes['user_id'];
            }
            if (array_key_exists('branch_id', $attributes)) {
                $branchs    = $attributes['branch_id'];
            }
            if ($froms) {
                if ($froms && $tos) {
                    $report->whereDate('billing.created_at', '>=', $this->convert_date_to_english($froms));
                    $report->whereDate('billing.created_at', '<=', $this->convert_date_to_english($tos));
                } else if ($tos) {
                    $report->whereDate('billing.created_at', '<=', $this->convert_date_to_english($tos));
                } else {
                    $report->whereDate('billing.created_at', '>=', $this->convert_date_to_english($froms));
                    $report->whereDate('billing.created_at', '<=', date('Y-m-d H:i:s'));
                }
            }

            if ($filter_user != '') {
                $report->where('billing.user_id', $filter_user);
            }
            if ($branchs != '') {
                $report->where('users.branch_id', $branchs);
                $selectusers = $this->user->where('branch_id', $branchs)->where('user_type', 'collector')->get();
            }

            if ($fiscal_year != '') {
                $report->where('billing.fiscal_year_id', $fiscal_year);
            }
        } else {
            $convertedDate = $this->current_date_to_nepali() . ' ' . date('H:i:s');
            $currentFiscalYear = $this->fiscalYear($convertedDate);
            $report->where('billing.fiscal_year_id', $currentFiscalYear->id);
        }

        $collection_report = $report->orderBy('billing.id', 'DESC')->paginate(Config::get('constant.paginate'));

        $collectionReportDetail = [
            'froms' => $froms,
            'tos' => $tos,
            'collection_report' => $collection_report,
            'all_branch' => $all_branch,
            'my_users' => $my_users,
            'branchs' => $branchs,
            'user' => $filter_user,
            'fiscal_years' => $fiscal_years,
            'fiscal_year' => $fiscal_year,
            'selectusers' => $selectusers
        ];

        return $collectionReportDetail;
    }

    public function exportAll()
    {
        $report = $this->reportInterface->getExportQuery();

        if ($this->authusertype() == 'branch admin') {
            $report->where('users.branch_id', '=', $this->authbranchid());
        }

        $report = $report->get();
        $spreadsheet = new Spreadsheet();
        $sheet = $spreadsheet->getActiveSheet();
        $sheet->setCellValue('A1', 'Id');
        $sheet->setCellValue('B1', 'customer_name');
        $sheet->setCellValue('C1', 'collector_name');
        $sheet->setCellValue('D1', 'branch');
        $sheet->setCellValue('E1', 'created_at');
        $sheet->setCellValue('F1', 'gross_amount');
        $sheet->setCellValue('G1', 'discount_amount');
        $sheet->setCellValue('H1', 'net_amount');
        $rows = 2;
        foreach ($report as $reports) {
            $sheet->setCellValue('A' . $rows, $reports['id']);
            $sheet->setCellValue('B' . $rows, $reports['customer_name']);
            $sheet->setCellValue('C' . $rows, $reports['collector_name']);
            $sheet->setCellValue('D' . $rows, $reports['branch']);
            $sheet->setCellValue('E' . $rows, $reports['created_at']);
            $sheet->setCellValue('F' . $rows, $reports['gross_amount']);
            $sheet->setCellValue('G' . $rows, $reports['discount_amount']);
            $sheet->setCellValue('H' . $rows, $reports['net_amount']);
            $rows++;
        }
        $fileName = "emp";
        $writer = new Xlsx($spreadsheet);
        $writer->save($fileName);
        header("Content-Type: application/vnd.ms-excel");

        return $fileName;
    }

    public function getBillReturnReportIndex(array $attributes)
    {
        $branches         = $this->branch->getAllBranch();
        $users           = $this->user->get_my_user_by_branch();
        $report =  $this->getBillReturnReport();
        $fiscal_years       = $this->fiscal_year->listFiscalYears();
        $selectusers = [];
        if ($this->authusertype() == 'branch admin') {
            $report->where('customers.branch_id', '=', $this->authbranchid());
        }

        $froms = '';
        $tos = '';
        $user_id = '';
        $branchs    = '';
        $fiscal_year = '';

        if ($attributes) {
            if (array_key_exists('from', $attributes)) {
                $froms    = $attributes['from'];
            }

            if (array_key_exists('to', $attributes)) {
                $tos    = $attributes['to'];
            }

            if (array_key_exists('user_id', $attributes)) {
                $user_id = $attributes['user_id'];
            }

            if (array_key_exists('branch_id', $attributes)) {
                $branchs    = $attributes['branch_id'];
            }

            if (array_key_exists('fiscal_year', $attributes)) {
                $fiscal_year    = $attributes['fiscal_year'];
            }

            if ($froms) {
                if ($froms && $tos) {

                    $report->whereDate('bill_returns.created_at', '>=', $this->convert_date_to_english($froms));
                    $report->whereDate('bill_returns.created_at', '<=', $this->convert_date_to_english($tos));
                } else if ($tos) {
                    $report->whereDate('bill_returns.created_at', '<=', $this->convert_date_to_english($tos));
                } else {
                    $report->whereDate('bill_returns.created_at', '>=', $this->convert_date_to_english($froms));
                    $report->whereDate('bill_returns.created_at', '<=', date('Y-m-d H:i:s'));
                }
            }

            if ($user_id != '') {
                $report->where('bill_returns.user_id', $user_id);
            }
            if ($branchs != '') {
                $report->where('customers.branch_id', $branchs);
                $selectusers = $this->user->where('branch_id', $branchs)->where('user_type', 'collector')->get();
            }
            if ($fiscal_year != '') {
                $report->where('bill_returns.fiscal_year', $fiscal_year);
            }
        } else {
            $convertedDate = $this->current_date_to_nepali() . ' ' . date('H:i:s');
            $currentFiscalYear = $this->fiscalYear($convertedDate);
            $report->where('bill_returns.fiscal_year', $currentFiscalYear->id);
        }

        $billReturns = $report->groupBy('bill_returns.billing_id')->paginate(Config::get('constant.paginate'));

        $billReturnsDetails = [
            'froms' => $froms,
            'tos' => $tos,
            'billReturns' => $billReturns,
            'branches' => $branches,
            'user_id' => $user_id,
            'branchs' => $branchs,
            'users' => $users,
            'selectusers' => $selectusers,
            'fiscal_years' => $fiscal_years,
            'fiscal_year' => $fiscal_year,
        ];

        return $billReturnsDetails;
    }

    public function getBillReturnReport()
    {
        return $this->billReturn->selectRaw("customers.customer_number,customers.id as customer_id, customers.name as customer, customers.phone_number, bill_returns.credit_note, 
                sum(bill_returns.gross_amount) as gross_amount, 
                sum(bill_returns.net_amount) as net_amount, users.name as returned_by, bill_returns.billing_id, sum(bill_returns.discount_amount) as discount_amount, 
                sum(bill_returns.vat_amount) as vat_amount, usr.name as collector, bill_returns.created_at, bill_returns.tender_amount")
            ->leftJoin('customers', 'customers.id', 'bill_returns.customer_id')
            ->leftJoin('users', 'users.id', 'bill_returns.returned_by')
            ->leftJoin('users as usr', 'usr.id', 'bill_returns.user_id');
    }

    public function getMaterializedViewBillReport()
    {
        return $this->billing->select(
            DB::raw("CONCAT('BIL-',billing.id) AS bill_no"),
            "fiscal_year.fiscal_year",
            "customers.name AS customer_name",
            "customers.id as customer_id",
            "customers.phone_number AS phone_number",
            "customers.customer_number AS customer_number",
            "billing.from_year",
            "billing.from_month",
            "billing.to_year",
            "billing.date_np",
            "billing.to_month",
            "customers.vat_pan_number",
            "users.name as collector",
            "billing.discount_amount",
            "billing.gross_amount",
            "billing.vat_amount",
            "billing.tender_amount as net_amount",
            "billing.created_at as created_at",
            "ird_status",
            "printcount"
        )
            ->leftJoin('customers', 'customers.id', 'billing.customer_id')
            ->leftJoin('users', 'users.id', 'billing.user_id')
            ->leftJoin('branches', 'branches.id', 'users.branch_id')
            ->leftJoin('fiscal_year', 'fiscal_year.id', 'billing.fiscal_year_id');
    }


    public function getMaterializedViewBillReturnsReport()
    {
        return $this->billReturn->select(
            DB::raw("CONCAT('CRN-',credit_note) AS bill_no"),
            "fiscal_year.fiscal_year",
            "customers.name AS customer_name",
            "customers.id as customer_id",
            "customers.phone_number AS phone_number",
            "customers.customer_number AS customer_number",
            "bill_returns.from_year",
            "bill_returns.from_month",
            "bill_returns.to_year",
            "bill_returns.date_np",
            "bill_returns.to_month",
            "customers.vat_pan_number",
            "users.name as collector",
            "bill_returns.discount_amount",
            "bill_returns.gross_amount",
            "bill_returns.vat_amount",
            DB::raw("CONCAT('-',bill_returns.tender_amount) as net_amount"),
            "bill_returns.created_at as created_at",
            "ird_status",
            "printcount"
        )
            ->leftJoin('customers', 'customers.id', 'bill_returns.customer_id')
            ->leftJoin('users', 'users.id', 'bill_returns.user_id')
            ->leftJoin('branches', 'branches.id', 'users.branch_id')
            ->leftJoin('fiscal_year', 'fiscal_year.id', 'bill_returns.fiscal_year');
    }

    public function getMaterializedViewReportIndex(array $attributes)
    {
        $branches         = $this->branch->getAllBranch();
        $my_users           = $this->user->get_my_user_by_branch();
        $bills = $this->getMaterializedViewBillReport();
        $billReturns = $this->getMaterializedViewBillReturnsReport();


        $fiscal_years       = $this->fiscal_year->listFiscalYears();
        $selectusers = [];
        if ($this->authusertype() == 'branch admin') {
            $bills->where('customers.branch_id', '=', $this->authbranchid());
            $billReturns->where('customers.branch_id', '=', $this->authbranchid());
        }

        $froms = '';
        $tos = '';
        $user_id = '';
        $branchs    = '';
        $fiscal_year = '';

        if ($attributes) {
            if (array_key_exists('from', $attributes)) {
                $froms    = $attributes['from'];
            }

            if (array_key_exists('to', $attributes)) {
                $tos    = $attributes['to'];
            }

            if (array_key_exists('user_id', $attributes)) {
                $user_id = $attributes['user_id'];
            }

            if (array_key_exists('branch_id', $attributes)) {
                $branchs    = $attributes['branch_id'];
            }

            if (array_key_exists('fiscal_year', $attributes)) {
                $fiscal_year    = $attributes['fiscal_year'];
            }

            if ($froms) {
                if ($froms && $tos) {
                    $bills->whereDate('billing.created_at', '>=', $this->convert_date_to_english($froms));
                    $bills->whereDate('billing.created_at', '<=', $this->convert_date_to_english($tos));
                    $billReturns->whereDate('bill_returns.created_at', '>=', $this->convert_date_to_english($froms));
                    $billReturns->whereDate('bill_returns.created_at', '<=', $this->convert_date_to_english($tos));
                } else if ($tos) {
                    $bills->whereDate('billing.created_at', '<=', $this->convert_date_to_english($tos));
                    $billReturns->whereDate('bill_returns.created_at', '<=', $this->convert_date_to_english($tos));
                } else {
                    $bills->whereDate('billing.created_at', '>=', $this->convert_date_to_english($froms));
                    $bills->whereDate('billing.created_at', '<=', date('Y-m-d H:i:s'));
                    $billReturns->whereDate('bill_returns.created_at', '>=', $this->convert_date_to_english($froms));
                    $billReturns->whereDate('bill_returns.created_at', '<=', date('Y-m-d H:i:s'));
                }
            }

            if ($user_id != '') {
                $bills->where('users.id', $user_id);
                $billReturns->where('users.id', $user_id);
            }
            if ($branchs != '') {
                $bills->where('customers.branch_id', $branchs);
                $billReturns->where('customers.branch_id', $branchs);
                $selectusers = $this->user->where('branch_id', $branchs)->where('user_type', 'collector')->get();
            }
            if ($fiscal_year != '') {
                $bills->where('billing.fiscal_year_id', $fiscal_year);
                $billReturns->where('bill_returns.fiscal_year', $fiscal_year);
            }
        } else {
            $convertedDate = $this->current_date_to_nepali() . ' ' . date('H:i:s');
            $currentFiscalYear = $this->fiscalYear($convertedDate);
            $bills->where('billing.fiscal_year_id', $currentFiscalYear->id);
            $billReturns->where('bill_returns.fiscal_year', $currentFiscalYear->id);
        }
        $d =  $billReturns->union($bills);
        $billReturns = $billReturns->orderBy('created_at', 'DESC')->paginate(Config::get('constant.paginate'));

        $reports = [
            'froms' => $froms,
            'tos' => $tos,
            'reports' => $billReturns,
            'branches' => $branches,
            'user' => $user_id,
            'branchs' => $branchs,
            'my_users' => $my_users,
            'selectusers' => $selectusers,
            'fiscal_years' => $fiscal_years,
            'fiscal_year' => $fiscal_year,
        ];

        return $reports;
    }

    public function printMaterializedReport(array $attributes)
    {
        $branches           = $this->branch->getAllBranch();
        $my_users           = $this->user->get_my_user_by_branch();
        $bills              = $this->getMaterializedViewBillReport();
        $billReturns        = $this->getMaterializedViewBillReturnsReport();


        $fiscal_years       = $this->fiscal_year->listFiscalYears();
        $selectusers = [];
        if ($this->authusertype() == 'branch admin') {
            $bills->where('customers.branch_id', '=', $this->authbranchid());
            $billReturns->where('customers.branch_id', '=', $this->authbranchid());
        }

        $froms = '';
        $tos = '';
        $user_id = '';
        $branchs    = '';
        $fiscal_year = '';

        if ($attributes) {
            if (array_key_exists('from', $attributes)) {
                $froms    = $attributes['from'];
            }

            if (array_key_exists('to', $attributes)) {
                $tos    = $attributes['to'];
            }

            if (array_key_exists('user_id', $attributes)) {
                $user_id = $attributes['user_id'];
            }

            if (array_key_exists('branch_id', $attributes)) {
                $branchs    = $attributes['branch_id'];
            }

            if (array_key_exists('fiscal_year', $attributes)) {
                $fiscal_year    = $attributes['fiscal_year'];
            }

            if ($froms) {
                if ($froms && $tos) {
                    $bills->whereDate('billing.created_at', '>=', $this->convert_date_to_english($froms));
                    $bills->whereDate('billing.created_at', '<=', $this->convert_date_to_english($tos));
                    $billReturns->whereDate('bill_returns.created_at', '>=', $this->convert_date_to_english($froms));
                    $billReturns->whereDate('bill_returns.created_at', '<=', $this->convert_date_to_english($tos));
                } else if ($tos) {
                    $bills->whereDate('billing.created_at', '<=', $this->convert_date_to_english($tos));
                    $billReturns->whereDate('bill_returns.created_at', '<=', $this->convert_date_to_english($tos));
                } else {
                    $bills->whereDate('billing.created_at', '>=', $this->convert_date_to_english($froms));
                    $bills->whereDate('billing.created_at', '<=', date('Y-m-d H:i:s'));
                    $billReturns->whereDate('bill_returns.created_at', '>=', $this->convert_date_to_english($froms));
                    $billReturns->whereDate('bill_returns.created_at', '<=', date('Y-m-d H:i:s'));
                }
            }

            if ($user_id != '') {
                $bills->where('users.id', $user_id);
                $billReturns->where('users.id', $user_id);
            }
            if ($branchs != '') {
                $bills->where('customers.branch_id', $branchs);
                $billReturns->where('customers.branch_id', $branchs);
                $selectusers = $this->user->where('branch_id', $branchs)->where('user_type', 'collector')->get();
            }
            if ($fiscal_year != '') {
                $bills->where('billing.fiscal_year_id', $fiscal_year);
                $billReturns->where('bill_returns.fiscal_year', $fiscal_year);
            }
        } else {
            $convertedDate = $this->current_date_to_nepali() . ' ' . date('H:i:s');
            $currentFiscalYear = $this->fiscalYear($convertedDate);
            $bills->where('billing.fiscal_year_id', $currentFiscalYear->id);
            $billReturns->where('bill_returns.fiscal_year', $currentFiscalYear->id);
        }
        $billReturns->union($bills);
        $billReturns = $billReturns->orderBy('created_at', 'DESC')->paginate(Config::get('constant.paginate'));

        $reports = [
            'froms' => $froms,
            'tos' => $tos,
            'reports' => $billReturns,
            'branches' => $branches,
            'user' => $user_id,
            'branchs' => $branchs,
            'my_users' => $my_users,
            'selectusers' => $selectusers,
            'fiscal_years' => $fiscal_years,
            'fiscal_year' => $fiscal_year,
        ];

        return $reports;
    }
}
