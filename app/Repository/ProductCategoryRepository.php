<?php

namespace App\Repository;

use App\Interfaces\ProductCategoryInterface;
use App\Models\ProductCategory;
use App\Repository\BaseRepository;

class ProductCategoryRepository extends BaseRepository implements ProductCategoryInterface
{

    /**
     * UserRepository constructor.
     *
     * @param User $model
     */
    public function __construct(ProductCategory $model)
    {
        parent::__construct($model);
    }

    /**
     * @return Collection
     */
    public function getProductCategory()
    {
        return $this->model->select('id', 'name','parent_id', 'status')->where('parent_id',null)->get();
    }

    public function getActiveProductCategory()
    {
        return $this->model->select('id', 'name', 'status')->where('status','active')->get();
    }

    public function create(array $attributes, $id = null)
    {
        if ($id != null || !empty($id)) {
            $customerCategory = $this->model->find($id);
            return $customerCategory->update($attributes);    
        } 

        return $this->model->create($attributes);
    }

    public function createSubCategory($attributes, $id = null)
    {
        return $this->model->create($attributes);
    }
    public function listSubCategory($id)
    {
        return $this->model->select('id','name','parent_id')->where('parent_id',$id)->get();
    }

    public function getProductCategoryById($id)
    {
        return $this->model->find($id);
    }

}
