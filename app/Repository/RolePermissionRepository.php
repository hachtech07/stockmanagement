<?php

namespace App\Repository;

use App\Interfaces\RolePermissionInterface;
use App\Models\Role;
use App\Models\RoleHasPermission;
use App\Repository\BaseRepository;

class RolePermissionRepository extends BaseRepository implements RolePermissionInterface{

    /**
     * RolePermissionRepository constructor.
     *
     * @param RoleHasPermission $model
     */
    private $role;
    public function __construct(RoleHasPermission $model ,Role $role)
    {
        parent::__construct($model);
        $this->role = $role;
    }

    public function add($attributes){
        $role_id = $attributes['role_id'];
        $permission_Id = $attributes['permission_id'];
        $permissionData = [];

        foreach ($permission_Id as $attribute => $value) {
            $permissionData[] = [
                'permission_id' => $attribute,
                'role_id'      => $role_id
            ];
        }

        $this->model->where('role_id',$role_id)->delete();
        $this->model->insert($permissionData);
    }

    public function update(array $attributes , $id){

        $role = $this->findRoleById($id);
         return $role->update([
             'name' => $attributes['name'],
             'guard_name' => Role::GUARD
         ]);
    }

    public function findRoleById($id)
    {
        $role = $this->role->find($id);
        return $role;
    }
}