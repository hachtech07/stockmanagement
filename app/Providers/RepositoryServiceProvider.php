<?php

namespace App\Providers;

use App\Interfaces\BillingCategoryInterface;
use App\Interfaces\BillingInterface;
use App\Repository\BaseRepository;
use App\Repository\UserRepository;
use App\Interfaces\BranchInterface;
use App\Interfaces\CustomerInterface;
use App\Interfaces\DashboardInterface;
use App\Repository\BranchRepository;
use App\Interfaces\EloquentInterface;
use App\Interfaces\LogInterface;
use Illuminate\Support\ServiceProvider;
use App\Interfaces\ModelHasRoleInterface;
use App\Interfaces\ProductCategoryInterface;
use App\Interfaces\ProductInterface;
use App\Interfaces\ProductSubCategoryInterface;
use App\Interfaces\ProfileInterface;
use App\Interfaces\ReportInterface;
use App\Interfaces\RolePermissionInterface;
use App\Interfaces\SalesInterface;
use App\Repository\ModelHasRoleRepository;
use App\Interfaces\UserRepositoryInterface;
use App\Repository\BillingCategoryRepository;
use App\Repository\BillingRepository;
use App\Repository\CustomerRepository;
use App\Repository\DashboardRepository;
use App\Repository\LogRepository;
use App\Repository\ProductCategoryRepository;
use App\Repository\ProductRepository;
use App\Repository\ProductSubCategoryRepository;
use App\Repository\ProfileRepository;
use App\Repository\ReportRepository;
use App\Repository\RolePermissionRepository;
use App\Repository\SalesRepository;

class RepositoryServiceProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->bind(EloquentInterface::class, BaseRepository::class);
        $this->app->bind(UserRepositoryInterface::class, UserRepository::class);
        $this->app->bind(ModelHasRoleInterface::class, ModelHasRoleRepository::class);
        $this->app->bind(BranchInterface::class, BranchRepository::class);
        $this->app->bind(ProductCategoryInterface::class, ProductCategoryRepository::class);
        $this->app->bind(CustomerInterface::class, CustomerRepository::class);
        $this->app->bind(ProfileInterface::class,ProfileRepository::class);
        $this->app->bind(ReportInterface::class, ReportRepository::class);
        $this->app->bind(BillingInterface::class, BillingRepository::class);
        $this->app->bind(BillingCategoryInterface::class, BillingCategoryRepository::class);
        $this->app->bind(RolePermissionInterface::class ,RolePermissionRepository::class);
        $this->app->bind(DashboardInterface::class, DashboardRepository::class);
        $this->app->bind(LogInterface::class,LogRepository::class);
        $this->app->bind(ProductInterface::class,ProductRepository::class);
        $this->app->bind(SalesInterface::class,SalesRepository::class);
        $this->app->bind(ProductSubCategoryInterface::class,ProductSubCategoryRepository::class);
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }
}
