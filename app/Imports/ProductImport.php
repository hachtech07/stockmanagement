<?php

namespace App\Imports;
use App\Models\Product;
use Maatwebsite\Excel\Concerns\Importable;
use Maatwebsite\Excel\Concerns\ToModel;
use Maatwebsite\Excel\Concerns\WithBatchInserts;
use Maatwebsite\Excel\Concerns\WithChunkReading;
use Maatwebsite\Excel\Concerns\WithHeadingRow;
use Maatwebsite\Excel\Concerns\WithValidation;

class ProductImport implements ToModel, WithHeadingRow, WithValidation, WithBatchInserts, WithChunkReading
{
    use Importable;
    /**
     * @param array $row
     *
     * @return \Illuminate\Database\Eloquent\Model|null
     */
    public function model(array $row)
    {
        // $attribute=[];

        // if($row['stock_alert'] >  $row['unit']){
        //     $attribute['stock_alert'] = $row['unit'] - 1 ;
        // }
        return new Product([
            'product_name'              => $row['product_name'],
            'product_category_id'       => $row['product_category_id'],
            'product_sub_category_id'   => $row['product_sub_category_id'],
            'color'                     => $row['color'],
            'model'                     => $row['model'] ? $row['model']:'',
            'mrp'                       => $row['mrp'],
            'cp'                        => $row['cp'],
            'stock_alert'               => $row['stock_alert'],
            'entry_date'                => $row['entry_date'],
            'fiscal_year_id'            => $row['fiscal_year_id'],
            'batch_no'                  => $row['batch_no'],
            'created_by'                => $row['created_by'] ? $row['created_by']:1,
            'unit'                      => $row['unit'],
        ]);
    }

    public function rules(): array
    {
        return [
            '*.product_name'            => 'required',
            '*.product_category_id'     => 'required|integer',
            '*.product_sub_category_id' => 'required|integer',
            '*.color'                   => 'required',
            '*.model'                   => 'nullable',
            '*.mrp'                     => 'required|numeric',
            '*.cp'                      => 'required|numeric',
            '*.stock_alert'             => 'required|integer',
            '*.entry_date'              => 'nullable',
            '*.fiscal_year_id'          => 'required|integer',
            '*.batch_no'                => 'required',
            '*.created_by'              => 'required|integer',
            '*.unit'                    => 'required|integer'
        ];
    }

    public function customValidationMessages()
    {
        return [];
    }

    public function batchSize(): int
    {
        return 1000;
    }

    public function chunkSize(): int
    {
        return 1000;
    }
}
