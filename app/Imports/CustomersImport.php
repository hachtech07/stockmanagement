<?php

namespace App\Imports;

use App\Models\Customer;
use Maatwebsite\Excel\Concerns\Importable;
use Maatwebsite\Excel\Concerns\ToModel;
use Maatwebsite\Excel\Concerns\WithBatchInserts;
use Maatwebsite\Excel\Concerns\WithChunkReading;
use Maatwebsite\Excel\Concerns\WithHeadingRow;
use Maatwebsite\Excel\Concerns\WithValidation;

class CustomersImport implements ToModel, WithHeadingRow, WithValidation, WithBatchInserts, WithChunkReading
{
    use Importable;
    /**
     * @param array $row
     *
     * @return \Illuminate\Database\Eloquent\Model|null
     */
    public function model(array $row)
    {
        $attribute=[];
        if ($row['advance_amount'] != null || $row['due_amount'] != null) {
            if ($row['advance_amount'] != null) {
                $attribute['advance_amount'] = $row['advance_amount'];
                $attribute['due_amount']     = '-' . $row['advance_amount'];
            }

            if ($row['due_amount'] != null) {
                $attribute['advance_amount'] = '-' . $row['due_amount'];
                $attribute['due_amount']     = $row['due_amount'];
            }
            // $attributes['due_amount'] != null     ? '' : $attributes['due_amount'] = 0;
            // $attributes['advance_amount'] != null ? '' : $attributes['advance_amount'] = 0;
        } else {
            $attribute['due_amount'] = 0;
            $attribute['advance_amount'] = 0;
        }
        return new Customer([
            'customer_number'   => $row['customer_number'],
            'name'              => $row['name'],
            'address'           => $row['address'],
            'phone_number'      => !empty($row['phone_number']) ? $row['phone_number'] : '0',
            'municipality'      => $row['municipality'],
            'ward'              => $row['ward'],
            'house_number'      => !empty($row['house_number']) ? $row['house_number'] : '0',
            'branch_id'         => $row['branch_id'],
            'status'            => $row['status'],
            'customer_category_id' => $row['customer_category_id'],
            'due_amount'        => $attribute['due_amount'],
            'advance_amount'    => $attribute['advance_amount'],
            'last_bill_date'    => $row['last_bill_date'],
            'monthly_payable'   => $row['monthly_payable'],
            'card_number'       => $row['card_number'],
            'created_by'        => 1,
            'collector_id'      => $row['collector_id'],
            'vat_pan_number'    => $row['vat_pan_number']
        ]);
    }

    public function rules(): array
    {
        return [
            '*.customer_number'   => 'required',
            '*.card_number'       => 'required',
            '*.name'              => 'required',
            '*.address'           => 'required',
            // '*.phone_number'      => 'required|numeric|digits:10',
            '*.municipality'      => 'required',
            '*.ward'              => 'required|integer',
            // '*.house_number'      => 'required',
            '*.branch_id'         => 'required|integer',
            '*.customer_category_id' => 'required|integer',
            '*.monthly_payable'   => 'required|numeric',
            // '*.last_bill_date'    => 'required|date_format:Y-m-d',
            '*.status'            => 'required',
            '*.status'            => 'required',
            // '*.created_by'            => 'required',
            '*.collector_id'      => 'required'
        ];
    }

    public function customValidationMessages()
    {
        return [];
    }

    public function batchSize(): int
    {
        return 1000;
    }

    public function chunkSize(): int
    {
        return 1000;
    }
}
