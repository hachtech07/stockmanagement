<?php


namespace App\Exceptions;

use App\Helper\StatusCode;
use Exception;
use RuntimeException;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

class CustomException extends RuntimeException
{
    public function __construct($message = "No contain avaliable", $code = 500, \Throwable $previous = null)
    {
        parent::__construct($message, $code, $previous);
    }
}