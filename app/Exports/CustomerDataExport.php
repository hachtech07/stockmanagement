<?php

namespace App\Exports;

use App\Traits\AuthTrait;
use Illuminate\Http\Request;
use App\Traits\DateConveterTrait;
use Illuminate\Support\Facades\Auth;
use Maatwebsite\Excel\Events\AfterSheet;
use PhpOffice\PhpSpreadsheet\Shared\Date;
use Maatwebsite\Excel\Concerns\Exportable;
use Maatwebsite\Excel\Concerns\WithEvents;
use Maatwebsite\Excel\Concerns\WithStyles;
use Maatwebsite\Excel\Concerns\WithMapping;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Illuminate\Contracts\Support\Responsable;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;
use Maatwebsite\Excel\Concerns\WithColumnFormatting;
use Maatwebsite\Excel\Concerns\WithCalculatedFormulas;
use Maatwebsite\Excel\Concerns\WithPreCalculateFormulas;
use PhpOffice\PhpSpreadsheet\Style\NumberFormat;
use PhpOffice\PhpSpreadsheet\Worksheet\Worksheet;
use App\Models\Customer;

class CustomerDataExport implements FromCollection, Responsable, ShouldAutoSize, WithHeadings, WithMapping, WithEvents ,WithCalculatedFormulas ,WithPreCalculateFormulas ,WithColumnFormatting ,WithStyles
{
    use Exportable, DateConveterTrait , AuthTrait;
    private $fileName = "customer-report.xlsx";
    private $count = 0;
    private $request;
    private $numOfRows=1;
    private $totalRow='';
    /**
     * @return \Illuminate\Support\Collection
     */

    public function __construct(Request $request)
    {
        $this->request = $request;
    }

    public function collection()
    {
        $searchParam    = $this->request->get('search_param');
        $branch_id      = $this->request->get('branch_id');
        $category_id    = $this->request->get('category_id');
        $collector      = $this->request->get('collector_id');
        $status         = $this->request->get('status');
        $customers = Customer::selectRaw("customers.id,customers.customer_number ,customers.name as name ,customers.monthly_payable, customers.phone_number, customers.municipality, customers.ward, customers.house_number, branches.branch_title,  branches.branch_code, customer_categories.category_title, customers.due_amount, customers.advance_amount, customers.last_bill_date, customers.card_number, customers.status,users.name as collector_name")
            ->where(function ($query) use ($searchParam) {
                $query->where('customers.name', 'LIKE', '%' . $searchParam . '%')
                    ->orWhere('customer_number', 'LIKE', '%' . $searchParam . '%')
                    ->orWhere('phone_number', 'LIKE', '%' . $searchParam . '%')
                    ->orWhere('card_number', 'LIKE', '%' . $searchParam . '%');
            })
            ->leftjoin('branches', 'customers.branch_id', '=', 'branches.id')
            ->leftjoin('customer_categories', 'customers.customer_category_id', '=', 'customer_categories.id')
            ->leftjoin('users', 'users.id', 'customers.collector_id');

        if ($this->authusertype() == 'branch admin') {
            $customers->where('customers.branch_id', Auth::user()->branch_id);
        }

        if ($branch_id != null) {
            $customers->where('customers.branch_id', $branch_id);
        }

        if ($category_id != null) {
            $customers->where('customers.customer_category_id', $category_id);
        }
        if ($collector != null) {
            $customers->where('users.id', $collector);
        }
        if($status !=null){
            $customers->where('customers.status',$status);
        }


        $data = $customers->get();
        $this->numOfRows += $data->count();
        $this->totalRow = (int)$this->numOfRows + 1;
        return $data;
    }
    public function map($data): array
    {
        return [
            ++$this->count,
            $data->customer_number,
            $data->name,
            $data->phone_number,
            $data->municipality,
            $data->ward,
            $data->house_number,
            $data->branch_title,
            $data->collector_name,
            $data->category_title,
            $data->monthly_payable,
            $data->last_bill_date,
            $data->status
        ];
    }
    function columnFormats(): array {
        return [
            'K' => NumberFormat::FORMAT_NUMBER_COMMA_SEPARATED1
        ];
     }
     
    public function styles(Worksheet $sheet) {
        return [
            "K{$this->totalRow}"  => ['font' => ['bold' => 16]]

            
        ];
     }
    public function headings(): array
    {
        return [
            'SN',
            'Customer Number',
            'Customer Name',
            'Phone Number',
            'Municipality',
            'Ward',
            'House Number',
            'Branch Name',
            'Collector Name',
            'Category Title',
            'Monthly Payable',
            'Last Bill Date',
            'Status'
        ];
    }

    public function registerEvents(): array
    {
        return [
            AfterSheet::class => function (AfterSheet $event) {
                $event->sheet->getStyle('A1:M1')->applyFromArray([
                    'font' => [
                        'bold' => true
                    ]
                ]);
                $cellRange = 'A1:M1'; // All headers
                $event->sheet->getDelegate()->getStyle($cellRange)->getAlignment()
                ->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);

                $event->sheet->setCellValue("J{$this->totalRow}","Total")->getStyle("J{$this->totalRow}")->applyFromArray([
                    'font' => [
                        'bold' => true
                    ],
                ]);
                $event->sheet->setCellValue("K{$this->totalRow}", "=SUM(K2:K{$this->numOfRows})");
            }
        ];
    }
}
