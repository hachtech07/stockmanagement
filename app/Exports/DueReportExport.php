<?php

namespace App\Exports;

use App\Traits\AuthTrait;
use Illuminate\Http\Request;
use App\Traits\DateConveterTrait;
use Illuminate\Support\Facades\Auth;
use Maatwebsite\Excel\Events\AfterSheet;
use PhpOffice\PhpSpreadsheet\Shared\Date;
use Maatwebsite\Excel\Concerns\Exportable;
use Maatwebsite\Excel\Concerns\WithEvents;
use Maatwebsite\Excel\Concerns\WithStyles;
use Maatwebsite\Excel\Concerns\WithMapping;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Illuminate\Contracts\Support\Responsable;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;
use Maatwebsite\Excel\Concerns\WithColumnFormatting;
use Maatwebsite\Excel\Concerns\WithCalculatedFormulas;
use Maatwebsite\Excel\Concerns\WithPreCalculateFormulas;
use PhpOffice\PhpSpreadsheet\Style\NumberFormat;
use PhpOffice\PhpSpreadsheet\Worksheet\Worksheet;
use App\Models\Customer;

class DueReportExport implements FromCollection, Responsable, ShouldAutoSize, WithHeadings, WithMapping, WithEvents, WithCalculatedFormulas, WithPreCalculateFormulas, WithColumnFormatting, WithStyles
{
    use Exportable, DateConveterTrait, AuthTrait;
    private $count = 0;
    private $request;
    private $numOfRows = 1;
    private $totalRow = '';
    /**
     * @return \Illuminate\Support\Collection
     */

    public function __construct(Request $request)
    {
        $this->request = $request;
    }
    public function collection()
    {
        
                    $date = $this->convert_date_to_nepali(date(('Y-m-d')));
                    $date_explode = explode("-", $date);
                    $year = $date_explode[0];
                    $month = $date_explode[1];
                     $report = \DB::query()
                     ->selectRaw("customer_number,
                     name,
                     customer_id,
                     branch_id,
                     collector_id,
                     address,
                     monthly_payable,
                     last_bill_date,
                     IF(due >= 0,due,0) AS due,
                     advance_amount,
                     municipality,
                     ward,
                     branch,
                     collector_name,
                     IF((due_amount < 0),advance_amount- due_amount,due_amount) as due_amount,
                     last_bill_date")
                     ->from(\DB::raw("(SELECT
                     customer_number,
                     customers.branch_id,
                     customers.collector_id,
                     customers.name AS name,
                     customers.id AS customer_id,
                     customers.address,
                     customers.monthly_payable,
                     customers.due_amount AS due,
                     customers.advance_amount as advance_amount,
                     municipality,
                     ward,
                     branches.branch_title AS branch,
                     users.name AS collector_name,
                     (((2079 - YEAR(last_bill_date)) * 12) - MONTH(last_bill_date) + 2 ) as total_payable_month,
                     
                      (
                     IF(
                     (
                     (YEAR(last_bill_date) > '2079') OR(
                     YEAR(last_bill_date) = '2079' AND MONTH(last_bill_date) >= '2'
                     )
                     ),
                     0,
                     (
                     IF(
                     YEAR(last_bill_date) < '2079',
                     (
                     (((2079 - YEAR(last_bill_date)) * 12) - MONTH(last_bill_date) + 2 ) * monthly_payable
                     ),
                     (
                     ('2' - MONTH(`last_bill_date`)) * monthly_payable
                     )
                     )
                     )
                     ) + due_amount
                     ) AS due_amount,
                     last_bill_date
                     FROM
                     customers
                     LEFT JOIN branches ON branches.id = customers.branch_id
                     LEFT JOIN users ON users.id = customers.collector_id
                       )a"));

        if ($this->authusertype() == 'branch admin') {
            $report->where('branch_id', '=', Auth::user()->branch_id);
        }

        if ($this->request->to) {
            $report->whereDate('last_bill_date', '<=', $this->request->to);
        }

        if ($this->request->user_id != '') {
            $report->where('collector_id', $this->request->user_id);
        }
        if ($this->request->branch_id != '') {
            $report->where('branch_id', $this->request->branch_id);
        }

        if ($this->request->address != '') {
            $report->where('address', 'like', '%' . $this->request->address . '%');
        }

        $report = $report->orderBy('due_amount', 'ASC')->get();
        $this->numOfRows += $report->count();
        $this->totalRow = (int)$this->numOfRows + 1;
        return $report;
    }
    public function map($report): array
    {
        return [
            ++$this->count,
            $report->customer_number,
            $report->name,
            $report->address,
            $report->municipality,
            $report->ward,
            $report->branch,
            $report->collector_name,
            $report->monthly_payable,
            $report->due,
            $report->due_amount,
            $report->last_bill_date
        ];
    }

    function columnFormats(): array
    {
        return [
            'I' => NumberFormat::FORMAT_NUMBER_COMMA_SEPARATED1,
            'J' => NumberFormat::FORMAT_NUMBER_COMMA_SEPARATED1,
            'K' => NumberFormat::FORMAT_NUMBER_COMMA_SEPARATED1
        ];
    }

    public function styles(Worksheet $sheet)
    {
        return [
            "I{$this->totalRow}"  => ['font' => ['bold' => 16]],
            "J{$this->totalRow}"  => ['font' => ['bold' => 16]],
            "K{$this->totalRow}"  => ['font' => ['bold' => 16]]


        ];
    }
    public function headings(): array
    {
        return [
            'SN',
            'Customer Number',
            'Name',
            'Address',
            'Municipality',
            'Ward',
            'Branch Name',
            'Collector Name',
            'Rate',
            'Previous Due',
            'Due Amount',
            'Last Bill Date'
        ];
    }

    public function registerEvents(): array
    {
        return [
            AfterSheet::class => function (AfterSheet $event) {
                $event->sheet->getStyle('A1:L1')->applyFromArray([
                    'font' => [
                        'bold' => true
                    ],
                ]);
                $cellRange = 'A1:J1'; // All headers
                $event->sheet->getDelegate()->getStyle($cellRange)->getAlignment()
                    ->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);

                $event->sheet->setCellValue("H{$this->totalRow}", "Total")->getStyle("H{$this->totalRow}")->applyFromArray([
                    'font' => [
                        'bold' => true
                    ],
                ]);
                $event->sheet->setCellValue("I{$this->totalRow}", "=SUM(I2:I{$this->numOfRows})");
                $event->sheet->setCellValue("J{$this->totalRow}", "=SUM(J2:J{$this->numOfRows})");
                $event->sheet->setCellValue("K{$this->totalRow}", "=SUM(K2:K{$this->numOfRows})");
            }
        ];
    }
}
