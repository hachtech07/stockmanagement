<?php

namespace App\Exports;

use App\Models\Bill_return;
use App\Traits\AuthTrait;
use Illuminate\Http\Request;
use App\Traits\DateConveterTrait;
use Illuminate\Support\Facades\Auth;
use Maatwebsite\Excel\Events\AfterSheet;
use PhpOffice\PhpSpreadsheet\Shared\Date;
use Maatwebsite\Excel\Concerns\Exportable;
use Maatwebsite\Excel\Concerns\WithEvents;
use Maatwebsite\Excel\Concerns\WithStyles;
use Maatwebsite\Excel\Concerns\WithMapping;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Illuminate\Contracts\Support\Responsable;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;
use Maatwebsite\Excel\Concerns\WithColumnFormatting;
use Maatwebsite\Excel\Concerns\WithCalculatedFormulas;
use Maatwebsite\Excel\Concerns\WithPreCalculateFormulas;
use PhpOffice\PhpSpreadsheet\Style\NumberFormat;
use PhpOffice\PhpSpreadsheet\Worksheet\Worksheet;
use App\Models\Customer;
use App\Traits\FiscalYear;

class BillReturnReportExport implements FromCollection, Responsable, ShouldAutoSize, WithHeadings, WithMapping, WithEvents, WithCalculatedFormulas, WithPreCalculateFormulas, WithColumnFormatting, WithStyles
{
    use Exportable, DateConveterTrait, AuthTrait, FiscalYear;
    private $count = 0;
    private $request;
    private $numOfRows = 1;
    private $totalRow = '';
    /**
     * @return \Illuminate\Support\Collection
     */

    public function __construct(Request $request)
    {
        $this->request = $request;
    }
    public function collection()
    {

        $report =  Bill_return::selectRaw("customers.customer_number,customers.id as customer_id, customers.name as customer, customers.phone_number, bill_returns.credit_note, 
        sum(bill_returns.gross_amount) as gross_amount, 
        sum(bill_returns.net_amount) as net_amount, users.name as returned_by, bill_returns.billing_id, sum(bill_returns.discount_amount) as discount_amount, 
        sum(bill_returns.vat_amount) as vat_amount, usr.name as collector, bill_returns.created_at, bill_returns.tender_amount")
    ->leftJoin('customers', 'customers.id', 'bill_returns.customer_id')
    ->leftJoin('users', 'users.id', 'bill_returns.returned_by')
    ->leftJoin('users as usr', 'usr.id', 'bill_returns.user_id');

        if ($this->authusertype() == 'branch admin') {
            $report->where('customers.branch_id', '=', Auth::user()->branch_id);
        }


        if ($this->request->from) {
            if ($this->request->from && $this->request->to) {

                $report->whereDate('bill_returns.created_at', '>=', $this->convert_date_to_english($this->request->from));
                $report->whereDate('bill_returns.created_at', '<=', $this->convert_date_to_english($this->request->to));
            } else if ($this->request->to) {
                $report->whereDate('bill_returns.created_at', '<=', $this->convert_date_to_english($this->request->to));
            } else {
                $report->whereDate('bill_returns.created_at', '>=', $this->convert_date_to_english($this->request->from));
                $report->whereDate('bill_returns.created_at', '<=', date('Y-m-d H:i:s'));
            }
        }

        if ($this->request->user_id != '') {
            $report->where('bill_returns.user_id', $this->request->user_id);
        }
        if ($this->request->branch_id != '') {
            $report->where('customers.branch_id', $this->request->branch_id);
        }

        if ($this->request->address != '') {
            $report->where('customers.address', 'like', '%' . $this->request->address . '%');
        }

        if ($this->request->fiscal_year != '') {
            $report->where('bill_returns.fiscal_year', $this->request->get('fiscal_year'));
        } else {
            $convertedDate = $this->current_date_to_nepali() . ' ' . date('H:i:s');
            $currentFiscalYear = $this->fiscalYear($convertedDate);
            $report->where('bill_returns.fiscal_year', $currentFiscalYear->id);
        }

        $report = $report->groupBy('billing_id')->get();
        $this->numOfRows += $report->count();
        $this->totalRow = (int)$this->numOfRows + 1;
        return $report;
    }
    public function map($report): array
    {
        return [
            ++$this->count,
            $report->billing_id,
            $report->credit_note,
            $report->created_at,
            $report->customer_number,
            $report->customer,
            $report->collector,
            $report->returned_by,
            $report->tender_amount
        ];
    }

    function columnFormats(): array
    {
        return [
            'I' => NumberFormat::FORMAT_NUMBER_COMMA_SEPARATED1,
            // 'J' => NumberFormat::FORMAT_NUMBER_COMMA_SEPARATED1,
            // 'K' => NumberFormat::FORMAT_NUMBER_COMMA_SEPARATED1,
            // 'L' => NumberFormat::FORMAT_NUMBER_COMMA_SEPARATED1
        ];
    }

    public function styles(Worksheet $sheet)
    {
        return [
            "I{$this->totalRow}"  => ['font' => ['bold' => 16]],
            // "J{$this->totalRow}"  => ['font' => ['bold' => 16]],
            // "K{$this->totalRow}"  => ['font' => ['bold' => 16]],
            // "L{$this->totalRow}"  => ['font' => ['bold' => 16]]


        ];
    }
    public function headings(): array
    {
        return [
            'SN',
            'Bill No',
            'Credit Note',
            'Return Date',
            'Customer No',
            'Customer',
            'Collector',
            'Returned By',
            'Returned Amount'
        ];
    }

    public function registerEvents(): array
    {
        return [
            AfterSheet::class => function (AfterSheet $event) {
                $event->sheet->getStyle('A1:I1')->applyFromArray([
                    'font' => [
                        'bold' => true
                    ],
                ]);
                $cellRange = 'A1:I1'; // All headers
                $event->sheet->getDelegate()->getStyle($cellRange)->getAlignment()
                    ->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);

                $event->sheet->setCellValue("H{$this->totalRow}", "Total")->getStyle("H{$this->totalRow}")->applyFromArray([
                    'font' => [
                        'bold' => true
                    ],
                ]);
                $event->sheet->setCellValue("I{$this->totalRow}", "=SUM(I2:I{$this->numOfRows})");
                // $event->sheet->setCellValue("J{$this->totalRow}", "=SUM(J2:J{$this->numOfRows})");
                // $event->sheet->setCellValue("K{$this->totalRow}", "=SUM(K2:K{$this->numOfRows})");
                // $event->sheet->setCellValue("L{$this->totalRow}", "=SUM(L2:L{$this->numOfRows})");
            }
        ];
    }
}
