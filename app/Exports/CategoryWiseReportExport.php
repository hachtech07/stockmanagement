<?php

namespace App\Exports;

use App\Models\Billing;
use App\Traits\AuthTrait;
use Illuminate\Http\Request;
use App\Models\BillingDetail;
use App\Traits\DateConveterTrait;
use App\Traits\FiscalYear;
use Maatwebsite\Excel\Events\AfterSheet;
use Maatwebsite\Excel\Concerns\Exportable;
use Maatwebsite\Excel\Concerns\WithEvents;
use Maatwebsite\Excel\Concerns\WithStyles;
use Maatwebsite\Excel\Concerns\WithMapping;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Illuminate\Contracts\Support\Responsable;
use Illuminate\Support\Facades\DB;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;
use Maatwebsite\Excel\Concerns\WithColumnFormatting;
use Maatwebsite\Excel\Concerns\WithCalculatedFormulas;
use Maatwebsite\Excel\Concerns\WithPreCalculateFormulas;
use PhpOffice\PhpSpreadsheet\Style\NumberFormat;
use PhpOffice\PhpSpreadsheet\Worksheet\Worksheet;

class CategoryWiseReportExport implements FromCollection, Responsable, ShouldAutoSize, WithHeadings, WithMapping, WithEvents, WithCalculatedFormulas, WithPreCalculateFormulas, WithColumnFormatting, WithStyles
{
    use Exportable, DateConveterTrait, AuthTrait, FiscalYear;
    private $fileName = "collection-report.xlsx";
    private $count = 0;
    private $request;
    private $billingDetail;
    private $numOfRows = 1;
    private $totalRow = '';
    /**
     * @return \Illuminate\Support\Collection
     */

    public function __construct(Request $request)
    {
        $this->request = $request;
    }

    public function collection()
    {
        $convertedDate = $this->current_date_to_nepali() . ' ' . date('H:i:s');
        $currentFiscalYear = $this->fiscalYear($convertedDate);
        // $report = DB::query()
        //     ->selectRaw(
        //         "billingId,
        //                 userId,
        //                 categoryId,
        //                 branchId,
        //                 customer_number,
        //                 category,
        //                 branch,
        //                 fiscal_year,
        //                 last_bill_date,
        //                 SUM(paidTotal) billingSubTotal,
        //                 SUM(returnTotal) returnSubTotal,
        //                 (
        //                     SUM(paidTotal) - SUM(returnTotal)
        //                 ) sub_total,
        //                 customer_name,
        //                 collector"
        //     )
        //     ->from(DB::raw("( SELECT
        //                 billing.id AS billingId,
        //                 billing_details.billing_category_id AS categoryId,
        //                 0 AS returnTotal,
        //                 billing_details.created_at AS last_bill_date,
        //                 billing_details.sub_total AS paidTotal,
        //                 users.branch_id as branchId,
        //                 customers.name AS customer_name,
        //                 customers.customer_number as customer_number,
        //                 billing.fiscal_year_id as fiscal_year,
        //                 users.name AS collector,
        //                 billing.user_id as userId,
        //                 branches.branch_title AS branch,
        //                 billing_categories.title AS category
        //             FROM
        //                 billing_details
        //             LEFT JOIN billing ON billing.id = billing_details.billing_id
        //             LEFT JOIN customers ON billing.customer_id = customers.id
        //             LEFT JOIN users ON billing.user_id = users.id
        //             LEFT JOIN branches ON branches.id = users.branch_id
        //             LEFT JOIN billing_categories ON billing_categories.id = billing_details.billing_category_id
        //             LEFT JOIN fiscal_year ON fiscal_year.id = billing.fiscal_year_id
        //             UNION ALL
        //             SELECT
        //                 bill_returns.billing_id AS billingId,
        //                 bill_return_details.billing_category_id AS categoryId,
        //                 bill_return_details.sub_total AS returnTotal,
        //                 '' AS last_bill_date,
        //                 0 AS paidTotal,
        //                 users.branch_id as branchId,
        //                 customers.name AS customer_name,
        //                 customers.customer_number as customer_number,
        //                 bill_returns.fiscal_year as fiscal_year,
        //                 users.name AS collector,
        //                 bill_returns.user_id as userId,
        //                 branches.branch_title AS branch,
        //                 billing_categories.title AS category
        //             FROM
        //                 bill_return_details
        //             JOIN bill_returns ON bill_returns.id = bill_return_details.billing_return_id
        //             LEFT JOIN billing ON billing.id = bill_returns.billing_id
        //             LEFT JOIN customers ON billing.customer_id = customers.id
        //             LEFT JOIN users ON billing.user_id = users.id
        //             LEFT JOIN branches ON branches.id = users.branch_id
        //             LEFT JOIN billing_categories ON billing_categories.id = bill_return_details.billing_category_id
        //             LEFT JOIN fiscal_year ON fiscal_year.id = bill_returns.fiscal_year ) a"))
        //     ->where('fiscal_year',$currentFiscalYear->id)
        //     ->where('categoryId', '!=', 2)
        //     ->where('categoryId', '!=', 3);
        $report = BillingDetail::selectRaw(
            "customers.name as customer_name, customers.customer_number,
            users.name as collector, billing_categories.title as category,billing_details.sub_total,
            billing_details.created_at as last_bill_date, billing_details.billing_amount,branches.branch_title as branch"
        )
            ->join('billing', 'billing.id', 'billing_details.billing_id')
            ->join('customers', 'billing.customer_id', '=', 'customers.id')
            ->join('users', 'billing.user_id', '=', 'users.id')
            ->join(
                'billing_categories',
                'billing_categories.id',
                'billing_details.billing_category_id'
            )
            ->join(
                'branches',
                'branches.id',
                'users.branch_id'
            )
        ->where('billing_categories.id', '!=', 3)
        ->where('billing_categories.id', '!=', 2)
        ->where('net_amount', '>', '0.00');

        if ($this->authusertype() == 'branch admin') {
            $report->where('users.branch_id', '=', $this->authbranchid());
        }


        if ($this->request->from) {
            if ($this->request->from && $this->request->to) {

                $report->whereDate('billing_details.created_at', '>=', $this->convert_date_to_english($this->request->from));
                $report->whereDate('billing_details.created_at', '<=', $this->convert_date_to_english($this->request->to));
            } else if ($this->request->to) {
                $report->whereDate('billing_details.created_at', '<=', $this->convert_date_to_english($this->request->to));
            } else {
                $report->whereDate('billing_details.created_at', '>=', $this->convert_date_to_english($this->request->from));
                $report->whereDate('billing_details.created_at', '<=', date('Y-m-d H:i:s'));
            }
        }

        if ($this->request->category_id != '') {
            $report->where('billing_details.billing_category_id', $this->request->category_id);
        }

        if ($this->request->user_id != '') {
            $report->where('billing.user_id', $this->request->user_id);
        }

        if ($this->request->branch_id != '') {
            $report->where('users.branch_id', $this->request->branch_id);
        }

        if ($this->request->fiscal_year != '') {
            $report->where('fiscal_year', $this->request->get('fiscal_year'));
        }

        $report = $report->groupBy(DB::raw("billing_id,billing_category_id"))->orderBy('billing_id', 'DESC')->get();
        $this->numOfRows += $report->count();
        $this->totalRow = (int)$this->numOfRows + 1;
        return $report;
    }
    public function map($report): array
    {
        return [
            ++$this->count,
            $report->customer_number,
            $report->customer_name,
            $report->collector,
            $report->branch,
            $report->category,
            $report->sub_total ? $report->sub_total : '0.00',
            $report->last_bill_date
        ];
    }
    function columnFormats(): array
    {
        return [
            'G' => NumberFormat::FORMAT_NUMBER_COMMA_SEPARATED1,
            'H' => NumberFormat::FORMAT_NUMBER_COMMA_SEPARATED1,
            'I' => NumberFormat::FORMAT_NUMBER_COMMA_SEPARATED1,

        ];
    }

    public function styles(Worksheet $sheet)
    {
        return [
            "G{$this->totalRow}"  => ['font' => ['bold' => 16]],
            "H{$this->totalRow}"  => ['font' => ['bold' => 16]],
            "I{$this->totalRow}"  => ['font' => ['bold' => 16]],


        ];
    }
    public function headings(): array
    {
        return [
            'SN',
            'Customer Number',
            'Customer Name',
            'Collector Name',
            'Branch Name',
            'Billing Category',
            'Sub Total',
            'Last Bill Date'
        ];
    }

    public function registerEvents(): array
    {
        return [
            AfterSheet::class => function (AfterSheet $event) {
                $event->sheet->getStyle('A1:H1')->applyFromArray([
                    'font' => [
                        'bold' => true
                    ]
                ]);
                $cellRange = 'A1:G1'; // All headers
                $event->sheet->getDelegate()->getStyle($cellRange)->getAlignment()
                    ->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);

                $event->sheet->setCellValue("F{$this->totalRow}", "Total")->getStyle("F{$this->totalRow}")->applyFromArray([
                    'font' => [
                        'bold' => true
                    ],
                ]);
                $event->sheet->setCellValue("G{$this->totalRow}", "=SUM(G2:G{$this->numOfRows})");
            }
        ];
    }
}
