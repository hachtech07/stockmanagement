<?php

namespace App\Exports;

use App\Traits\AuthTrait;
use Illuminate\Http\Request;
use App\Traits\DateConveterTrait;
use Illuminate\Support\Facades\Auth;
use Maatwebsite\Excel\Events\AfterSheet;
use Maatwebsite\Excel\Concerns\Exportable;
use Maatwebsite\Excel\Concerns\WithEvents;
use Maatwebsite\Excel\Concerns\WithStyles;
use Maatwebsite\Excel\Concerns\WithMapping;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Illuminate\Contracts\Support\Responsable;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;
use PhpOffice\PhpSpreadsheet\Style\NumberFormat;
use PhpOffice\PhpSpreadsheet\Worksheet\Worksheet;
use Maatwebsite\Excel\Concerns\WithCalculatedFormulas;
use Maatwebsite\Excel\Concerns\WithPreCalculateFormulas;
use Maatwebsite\Excel\Concerns\WithColumnFormatting;
use App\Models\Billing;


class DiscountReportExport implements FromCollection, Responsable, ShouldAutoSize, WithHeadings, WithMapping, WithEvents ,WithCalculatedFormulas,WithPreCalculateFormulas, WithColumnFormatting,WithStyles
{
    use Exportable, DateConveterTrait, AuthTrait;
    private $fileName = "discount.xlsx";
    private $count = 0;
    private $request;
    private $numOfRows=1;
    private $totalRow='';
    /**
     * @return \Illuminate\Support\Collection
     */

    public function __construct(Request $request)
    {
        $this->request = $request;
    }
    public function collection()
    {
        $report = Billing::selectRaw("customers.name as customer_name,customers.id as customer_id,branches.branch_title as branch, users.name as collector_name, billing.date_np,billing.gross_amount as gross_amount, billing.discount_amount,billing.vat_amount,billing.net_amount, billing.tender_amount,billing.discount_percent, billing.created_at")
        ->join('customers', 'billing.customer_id', '=', 'customers.id')
        ->join('users', 'billing.user_id', '=', 'users.id')
        ->join('branches', 'branches.id', 'users.branch_id')
        ->where('billing.discount_amount', '>', '0.00');

        if ($this->authusertype() == 'branch admin') {
            $report->where('users.branch_id', '=', Auth::user()->branch_id);
        }



        if ($this->request->from) {
            if ($this->request->from && $this->request->to) {

                $report->whereDate('billing.created_at', '>=', $this->convert_date_to_english($this->request->from));
                $report->whereDate('billing.created_at', '<=', $this->convert_date_to_english($this->request->to));
            } else if ($this->request->to) {
                $report->whereDate('billing.created_at', '<=', $this->convert_date_to_english($this->request->to));
            } else {
                $report->whereDate('billing.created_at', '>=', $this->convert_date_to_english($this->request->from));
                $report->whereDate('billing.created_at', '<=', date('Y-m-d H:i:s'));
            }
        }
        if ($this->request->user_id != '') {
            $report->where('billing.user_id', $this->request->user_id);
        }
        if ($this->request->branch_id != '') {
            $report->where('users.branch_id', $this->request->branch_id);
        }
        $report = $report->get();
        $this->numOfRows += $report->count();
        $this->totalRow = (int)$this->numOfRows + 1;
        return $report;
    }
    public function map($report): array
    {
        return [
            ++$this->count,
            $report->customer_name,
            $report->collector_name,
            $report->branch,
            $report->created_at,
            $report->gross_amount,
            $report->discount_amount,
            $report->discount_percent,
            $report->vat_amount,
            $report->net_amount,
            $report->tender_amount,
        ];
    }

    function columnFormats(): array {
        return [
            'F' => NumberFormat::FORMAT_NUMBER_COMMA_SEPARATED1,
            'G' => NumberFormat::FORMAT_NUMBER_COMMA_SEPARATED1,
            'I' => NumberFormat::FORMAT_NUMBER_COMMA_SEPARATED1,
            'J' => NumberFormat::FORMAT_NUMBER_COMMA_SEPARATED1,
            'K' => NumberFormat::FORMAT_NUMBER_COMMA_SEPARATED1,
        ];
     }
     
    public function styles(Worksheet $sheet) {
        return [
            // Style the first row as bold text.
            // 1    => ['font' => ['bold' => true]],
            // Styling an entire column.
            "F{$this->totalRow}"  => ['font' => ['bold' => 16]],
            "G{$this->totalRow}"  => ['font' => ['bold' => 16]],
            "I{$this->totalRow}"  => ['font' => ['bold' => 16]],
            "J{$this->totalRow}"  => ['font' => ['bold' => 16]],
            "K{$this->totalRow}"  => ['font' => ['bold' => 16]]
            
        ];
        //Another way
        // $sheet->getStyle("I1:I5")->getFont()->setBold(true);
     }
    public function headings(): array
    {
        return [
            'SN',
            'Customer Name',
            'Collector Name',
            'Branch',
            'Created At',
            'Gross Amount',
            'Discount Amount',
            'Discount Percent',
            'VAT Amount',
            'Net Amount',
            'Tender Amount'
        ];
    }

    public function registerEvents(): array
    {
        // echo $this->numOfRows;die;
       
        return [
            AfterSheet::class => function (AfterSheet $event) {
                $event->sheet->getStyle('A1:K1')->applyFromArray([
                    'font' => [
                        'bold' => true
                    ],
                ]);
                $cellRange = 'A1:K1'; // All headers
                $event->sheet->getDelegate()->getStyle($cellRange)->getAlignment()
                ->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);
                $event->sheet->setCellValue("E{$this->totalRow}","Total")->getStyle("E{$this->totalRow}")->applyFromArray([
                    'font' => [
                        'bold' => true
                    ],
                ]);
                $event->sheet->setCellValue("F{$this->totalRow}", "=SUM(F2:F{$this->numOfRows})");
                $event->sheet->setCellValue("G{$this->totalRow}", "=SUM(G2:G{$this->numOfRows})");
                $event->sheet->setCellValue("I{$this->totalRow}", "=SUM(I2:I{$this->numOfRows})");
                $event->sheet->setCellValue("J{$this->totalRow}", "=SUM(J2:J{$this->numOfRows})");
                $event->sheet->setCellValue("K{$this->totalRow}", "=SUM(k2:K{$this->numOfRows})");
            }
        ];
    }
    
}
