<?php

namespace App\Exports;

use App\Traits\AuthTrait;
use App\Traits\DateConveterTrait;
use Illuminate\Contracts\Support\Responsable;
use Illuminate\Http\Request;
use Maatwebsite\Excel\Concerns\FromCollection;
use Illuminate\Support\Facades\Auth;
use Maatwebsite\Excel\Concerns\Exportable;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;
use Maatwebsite\Excel\Concerns\WithCalculatedFormulas;
use Maatwebsite\Excel\Concerns\WithColumnFormatting;
use Maatwebsite\Excel\Concerns\WithEvents;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\WithMapping;
use Maatwebsite\Excel\Concerns\WithPreCalculateFormulas;
use Maatwebsite\Excel\Concerns\WithStyles;
use Maatwebsite\Excel\Events\AfterSheet;
use PhpOffice\PhpSpreadsheet\Shared\Date;
use PhpOffice\PhpSpreadsheet\Style\NumberFormat;
use PhpOffice\PhpSpreadsheet\Worksheet\Worksheet;
use App\Models\Billing;
use App\Traits\FiscalYear;

class CollectionReportExport implements FromCollection, Responsable, ShouldAutoSize, WithHeadings, WithMapping, WithEvents, WithCalculatedFormulas, WithPreCalculateFormulas, WithColumnFormatting, WithStyles
{
    use Exportable, DateConveterTrait, AuthTrait, FiscalYear;
    private $fileName = "collection-report.xlsx";
    private $count = 0;
    private $numOfRows = 1;
    private $totalRow = '';
    /**
     * @return \Illuminate\Support\Collection
     */
    private $request;
    public function __construct(Request $request)
    {
        $this->request = $request;
    }

    public function collection()
    {
        // $report = Billing::select(
        //     "billing.id",
        //     "customers.name AS customer_name",
        //     "customers.id as customer_id",
        //     "customers.phone_number AS phone_number",
        //     "customers.customer_number AS customer_number",
        //     "billing.previous_due_amount",
        //     "billing.previous_advance_amount",
        //     "billing.from_year",
        //     "billing.from_month",
        //     "billing.to_year",
        //     "billing.date_np",
        //     "billing.to_month",
        //     \DB::raw("IF(
        //         (ISNULL(
        //             SUM(bill_returns.gross_amount)
        //         )=1),
                
        //         format(billing.gross_amount,2),
        //         format((billing.gross_amount - SUM(bill_returns.gross_amount)),2)
        //     ) AS gross_amount,
        //     IF(
        //         (ISNULL(
        //             SUM(bill_returns.tender_amount)
        //         )=1),
                
        //         format(billing.total_amount,2),
        //         format((billing.total_amount - SUM(bill_returns.tender_amount)),2)
        //     ) AS net_amount,
        //     IF(
        //         (ISNULL(
        //             SUM(bill_returns.vat_amount)
        //         )=1),
                
        //         format(billing.vat_amount,2),
        //         format((billing.vat_amount - SUM(bill_returns.vat_amount)),2)
        //     ) AS vat_amount,
        //     IF(
        //         (ISNULL(
        //             SUM(bill_returns.tender_amount)
        //         )=1),
                
        //         format(billing.tender_amount,2),
        //         format((billing.tender_amount - SUM(bill_returns.tender_amount)),2)
        //     ) AS tender_amount,
        //     IF(
        //         (ISNULL(
        //             SUM(bill_returns.advance_amount)
        //         )=1),
                
        //         format(billing.advance_amount,2),
        //         format((billing.advance_amount - SUM(bill_returns.advance_amount)),2)
        //     ) AS advance_amount,
        //     IF(
        //         (ISNULL(
        //             SUM(bill_returns.due_amount)
        //         )=1),
                
        //         format(billing.due_amount,2),
        //         format((billing.due_amount - SUM(bill_returns.due_amount)),2)
        //     ) AS due_amount,
        //     IF(
                
        //         (ISNULL(
        //             SUM(bill_returns.discount_amount)
        //         )=1),
                
        //         format(billing.discount_amount,2),
        //         (billing.discount_amount - SUM(bill_returns.discount_amount)
        //         )
        //     ) AS discount_amount"),
        //     "billing.created_at"
        // )
        //     ->leftJoin('customers', 'customers.id', 'billing.customer_id')
        //     ->leftJoin('users', 'users.id', 'billing.user_id')
        //     ->leftJoin('branches', 'branches.id', 'users.branch_id')
        //     ->leftJoin('bill_returns', 'bill_returns.billing_id', 'billing.id')
        // ->groupBy(\DB::raw("bill_returns.billing_id,billing.id"))
        // ->where('billing.net_amount', '>', '0.00');

$report = Billing::selectRaw("customers.name as customer_name,customers.phone_number as phone_number,customers.customer_number as customer_number, 
billing.from_year, billing.to_year, billing.date_np, billing.to_month, billing.from_month,billing.gross_amount, 
billing.discount_amount,billing.net_amount,billing.tender_amount, 
billing.due_amount,billing.advance_amount, billing.created_at, previous_due_amount, previous_advance_amount, billing.vat_amount, bill_returns.id as bill_return_id")
->join('customers', 'billing.customer_id', '=', 'customers.id')
->join('users', 'billing.user_id', '=', 'users.id')
->join('branches', 'branches.id', 'users.branch_id')
->leftJoin('bill_returns', 'bill_returns.billing_id', 'billing.id')
->where('billing.net_amount', ">", '0.00');

        if ($this->authusertype() == 'branch admin') {
            $report->where('users.branch_id', '=', Auth::user()->branch_id);
        }


        if ($this->request->from) {
            if ($this->request->from && $this->request->to) {

                $report->whereDate('billing.created_at', '>=', $this->convert_date_to_english($this->request->from));
                $report->whereDate('billing.created_at', '<=', $this->convert_date_to_english($this->request->to));
            } else if ($this->request->to) {
                $report->whereDate('billing.created_at', '<=', $this->convert_date_to_english($this->request->to));
            } else {
                $report->whereDate('billing.created_at', '>=', $this->convert_date_to_english($this->request->from));
                $report->whereDate('billing.created_at', '<=', date('Y-m-d H:i:s'));
            }
        }
        if ($this->request->user_id != '') {
            $report->where('billing.user_id', $this->request->get('user_id'));
        }
        if ($this->request->branch_id != '') {
            $report->where('users.branch_id', $this->request->branch_id);
        }
        if ($this->request->fiscal_year != '') {
            $report->where('billing.fiscal_year_id', $this->request->get('fiscal_year'));
        } else {
            $convertedDate = $this->current_date_to_nepali() . ' ' . date('H:i:s');
            $currentFiscalYear = $this->fiscalYear($convertedDate);
            $report->where('billing.fiscal_year_id', $currentFiscalYear->id);
        }


        $report = $report->get();
        $this->numOfRows += $report->count();
        $this->totalRow = (int)$this->numOfRows + 1;
        return $report;
    }
    public function map($report): array
    {
        return [
            ++$this->count,
            $report->customer_number,
            $report->customer_name,
            $report->phone_number,
            $report->from_year . '-' . $report->from_month,
            $report->to_year . '-' . $report->to_month,
            $report->gross_amount,
            $report->discount_amount,
            $report->vat_amount,
            $report->net_amount,
            $report->previous_due_amount > 0 ? $report->previous_due_amount : '0.00',
            $report->previous_advance_amount > 0 ? $report->previous_advance_amount : '0.00',
            $report->tender_amount,
            $report->due_amount > 0 ?  $report->due_amount :'0.00',
            $report->advance_amount > 0 ? $report->advance_amount :'0.00',
            $report->created_at,
        ];
    }

    function columnFormats(): array
    {
        return [
            'G' => NumberFormat::FORMAT_NUMBER_COMMA_SEPARATED1,
            'H' => NumberFormat::FORMAT_NUMBER_COMMA_SEPARATED1,
            'I' => NumberFormat::FORMAT_NUMBER_COMMA_SEPARATED1,
            'J' => NumberFormat::FORMAT_NUMBER_COMMA_SEPARATED1,
            'K' => NumberFormat::FORMAT_NUMBER_COMMA_SEPARATED1,
            'L' => NumberFormat::FORMAT_NUMBER_COMMA_SEPARATED1,
            'M' => NumberFormat::FORMAT_NUMBER_COMMA_SEPARATED1,
            'N' => NumberFormat::FORMAT_NUMBER_COMMA_SEPARATED1,
            'O' => NumberFormat::FORMAT_NUMBER_COMMA_SEPARATED1,
        ];
    }

    public function styles(Worksheet $sheet)
    {
        return [
            "G{$this->totalRow}"  => ['font' => ['bold' => 16]],
            "H{$this->totalRow}"  => ['font' => ['bold' => 16]],
            "I{$this->totalRow}"  => ['font' => ['bold' => 16]],
            "J{$this->totalRow}"  => ['font' => ['bold' => 16]],
            "K{$this->totalRow}"  => ['font' => ['bold' => 16]],
            "L{$this->totalRow}"  => ['font' => ['bold' => 16]],
            "M{$this->totalRow}"  => ['font' => ['bold' => 16]],
            "N{$this->totalRow}"  => ['font' => ['bold' => 16]],
            "O{$this->totalRow}"  => ['font' => ['bold' => 16]]


        ];
    }
    public function headings(): array
    {
        return [
            'SN',
            'Customer Number',
            'Customer Name',
            'Customer Phone Number',
            'Bill From',
            'Bill To',
            'Gross Amount',
            'Discount Amount',
            'VAT Amount',
            'Net Amount',
            'PRE. Due Amount',
            'PRE. Advance Amount',
            'Tender Amount',
            'Current Due',
            'Current ADV amount',
            'Created At',
        ];
    }

    public function registerEvents(): array
    {
        return [
            AfterSheet::class => function (AfterSheet $event) {
                $event->sheet->getStyle('A1:P1')->applyFromArray([
                    'font' => [
                        'bold' => true
                    ]
                ]);
                $cellRange = 'A1:P1'; // All headers
                $event->sheet->getDelegate()->getStyle($cellRange)->getAlignment()
                    ->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);
                $event->sheet->setCellValue("F{$this->totalRow}", "Total")->getStyle("F{$this->totalRow}")->applyFromArray([
                    'font' => [
                        'bold' => true
                    ],
                ]);

                $event->sheet->setCellValue("G{$this->totalRow}", "=SUM(G2:G{$this->numOfRows})");
                $event->sheet->setCellValue("H{$this->totalRow}", "=SUM(H2:H{$this->numOfRows})");
                $event->sheet->setCellValue("I{$this->totalRow}", "=SUM(I2:I{$this->numOfRows})");
                $event->sheet->setCellValue("J{$this->totalRow}", "=SUM(J2:J{$this->numOfRows})");
                $event->sheet->setCellValue("K{$this->totalRow}", "=SUM(K2:K{$this->numOfRows})");
                $event->sheet->setCellValue("L{$this->totalRow}", "=SUM(L2:L{$this->numOfRows})");
                $event->sheet->setCellValue("M{$this->totalRow}", "=SUM(M2:M{$this->numOfRows})");
                $event->sheet->setCellValue("N{$this->totalRow}", "=SUM(N2:N{$this->numOfRows})");
                $event->sheet->setCellValue("O{$this->totalRow}", "=SUM(O2:O{$this->numOfRows})");
            }
        ];
    }
}
