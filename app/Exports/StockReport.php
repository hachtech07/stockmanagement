<?php

namespace App\Exports;

use App\Models\Billing;
use App\Traits\AuthTrait;
use Illuminate\Http\Request;
use App\Models\BillingDetail;
use App\Models\Product;
use App\Traits\DateConveterTrait;
use App\Traits\FiscalYear;
use Maatwebsite\Excel\Events\AfterSheet;
use Maatwebsite\Excel\Concerns\Exportable;
use Maatwebsite\Excel\Concerns\WithEvents;
use Maatwebsite\Excel\Concerns\WithStyles;
use Maatwebsite\Excel\Concerns\WithMapping;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Illuminate\Contracts\Support\Responsable;
use Illuminate\Support\Facades\DB;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;
use Maatwebsite\Excel\Concerns\WithColumnFormatting;
use Maatwebsite\Excel\Concerns\WithCalculatedFormulas;
use Maatwebsite\Excel\Concerns\WithPreCalculateFormulas;
use PhpOffice\PhpSpreadsheet\Style\NumberFormat;
use PhpOffice\PhpSpreadsheet\Worksheet\Worksheet;

class StockReport implements FromCollection, Responsable, ShouldAutoSize, WithHeadings, WithMapping, WithEvents, WithCalculatedFormulas, WithPreCalculateFormulas, WithColumnFormatting, WithStyles
{
    use Exportable, DateConveterTrait, AuthTrait, FiscalYear;
    private $fileName = "collection-report.xlsx";
    private $count = 0;
    private $request;
    private $billingDetail;
    private $numOfRows = 1;
    private $totalRow = '';
    /**
     * @return \Illuminate\Support\Collection
     */

    public function __construct(Request $request)
    {
        $this->request = $request;
    }

    public function collection()
    {
        $report = Product::select('products.id','products.product_name','product_categories.name as category_name','sub_category.subcategory_name as sub_category','products.color','products.model','products.mrp','products.cp','products.entry_date','products.created_at','fiscal_year.fiscal_years','products.batch_no','products.unit','products.stock_alert','users.first_name')
        ->leftjoin('product_categories','product_categories.id','products.product_category_id')
        ->leftjoin('fiscal_year','fiscal_year.id','products.fiscal_year_id')
        ->leftjoin('sub_category','sub_category.id','products.product_sub_category_id')
        ->leftjoin('users','users.id','products.created_by');

        if ($this->authusertype() == 'branch admin') {
            $report->where('users.branch_id', '=', $this->authbranchid());
        }


        if ($this->request->from) {
            if ($this->request->from && $this->request->to) {

                $report->whereDate('products.created_at', '>=', $this->convert_date_to_english($this->request->from));
                $report->whereDate('products.created_at', '<=', $this->convert_date_to_english($this->request->to));
            } else if ($this->request->to) {
                $report->whereDate('products.created_at', '<=', $this->convert_date_to_english($this->request->to));
            } else {
                $report->whereDate('products.created_at', '>=', $this->convert_date_to_english($this->request->from));
                $report->whereDate('products.created_at', '<=', date('Y-m-d H:i:s'));
            }
        }

        if ($this->request->batch != '') {
            $report->where('products.batch_no', $this->request->batch);
        }

        if ($this->request->category_id != '') {
            $report->where('products.product_category_id', $this->request->category_id);
        }

        if ($this->request->fiscal_year != '') {
            $report->where('fiscal_year_id', $this->request->get('fiscal_year'));
        }

        if ($this->request->product_sub_category_id != '') {
            $report->where('product_sub_category_id', $this->request->product_sub_category_id);
        }
        if ($this->request->product_name != '') {
            $report->where('products.product_name', 'LIKE', '%' . $this->request->get('product_name') . '%');

        }

        $report = $report->groupBy(DB::raw("products.id"))->orderBy('products.created_at', 'DESC')->get();
        $this->numOfRows += $report->count();
        $this->totalRow = (int)$this->numOfRows + 1;
        return $report;
    }
    public function map($report): array
    {
        return [
            ++$this->count,
            $report->product_name,
            $report->category_name,
            $report->sub_category,
            $report->batch_no,
            $report->color,
            $report->model,
            $report->unit,
            $report->cp,
            $report->mrp,
            $report->cp * $report->unit,
            $report->fiscal_years,
            $report->created_at,

            
        ];
    }
    function columnFormats(): array
    {
        return [
            'H' => NumberFormat::FORMAT_NUMBER_COMMA_SEPARATED1,
            'I' => NumberFormat::FORMAT_NUMBER_COMMA_SEPARATED1,
            'J' => NumberFormat::FORMAT_NUMBER_COMMA_SEPARATED1,
            'K' => NumberFormat::FORMAT_NUMBER_COMMA_SEPARATED1,


        ];
    }

    public function styles(Worksheet $sheet)
    {
        return [
            "G{$this->totalRow}"  => ['font' => ['bold' => 16]],
            "H{$this->totalRow}"  => ['font' => ['bold' => 16]],
            "I{$this->totalRow}"  => ['font' => ['bold' => 16]],
            "J{$this->totalRow}"  => ['font' => ['bold' => 16]],
            "K{$this->totalRow}"  => ['font' => ['bold' => 16]],
            "L{$this->totalRow}"  => ['font' => ['bold' => 16]],
            "M{$this->totalRow}"  => ['font' => ['bold' => 16]],



        ];
    }
    public function headings(): array
    {
        return [
            'SN',
            'Product name',
            'Product category',
            'Product subcategory',
            'Batch no.',
            'Color',
            'Model',
            'Unit',
            'Cost price',
            'Market retailing price',
            'Total',
            'Fiscal Year',
            'Created At'
        ];
    }

    public function registerEvents(): array
    {
        return [
            AfterSheet::class => function (AfterSheet $event) {
                $event->sheet->getStyle('A1:M1')->applyFromArray([
                    'font' => [
                        'bold' => true
                    ]
                ]);
                $cellRange = 'A1:K1'; // All headers
                $event->sheet->getDelegate()->getStyle($cellRange)->getAlignment()
                    ->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);

                $event->sheet->setCellValue("G{$this->totalRow}", "Total")->getStyle("G{$this->totalRow}")->applyFromArray([
                    'font' => [
                        'bold' => true
                    ],
                ]);
                $event->sheet->setCellValue("H{$this->totalRow}", "=SUM(H2:H{$this->numOfRows})");
                $event->sheet->setCellValue("I{$this->totalRow}", "=SUM(I2:I{$this->numOfRows})");
                $event->sheet->setCellValue("J{$this->totalRow}", "=SUM(J2:J{$this->numOfRows})");
                $event->sheet->setCellValue("K{$this->totalRow}", "=SUM(K2:K{$this->numOfRows})");

            }
        ];
    }
}
